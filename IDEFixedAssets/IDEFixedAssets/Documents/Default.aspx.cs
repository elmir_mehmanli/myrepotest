﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Documents_Default : System.Web.UI.Page
{
    UserData u_data = new UserData();
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserData"] == null) Config.Rd("/exit");
        u_data = (UserData)Session["UserData"];
        if (!IsPostBack)
        {
            db.DeletePendingDocument((short)u_data.LogonUserID);
            FillPageData(u_data);

            /*Items*/
            if (Session["drlGridDocumentPageSize"] == null) Session["drlGridDocumentPageSize"] = "10";
            drlGridDocumentPageSize.SelectedValue = Convert.ToString(Session["drlGridDocumentPageSize"]);

            grdDocuments.VirtualItemCount = db.GetDocumentCount();
            grdDocuments.PageIndex = Session["GridDocumentPageIndex"] == null ? 0 : Convert.ToInt32(Session["GridDocumentPageIndex"]);
            FillDocuments(Session["GridDocumentPageIndex"] == null ? 0 : Convert.ToInt32(Session["GridDocumentPageIndex"]));
            drlGridDocumentPageSize.Visible = grdDocuments.Rows.Count > 0;
            /**/

        }
    }



    void FillDocuments(int PageIndex)
    {
        /*Fill Item Grid*/
        grdDocuments.PageSize = Convert.ToInt32(Convert.ToString(Session["drlGridDocumentPageSize"]));
        grdDocuments.DataSource = db.GetDocumentList(PageIndex, Convert.ToInt32(Session["drlGridDocumentPageSize"]));
        grdDocuments.DataBind();
        int index = 0;
        foreach (GridViewRow row in grdDocuments.Rows)
        {
            LinkButton lnkDocumentsDataEdit = row.FindControl("lnkDocumentsDataEdit") as LinkButton;
            lnkDocumentsDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkDocumentsView = row.FindControl("lnkDocumentsView") as LinkButton;
            lnkDocumentsView.Text = Config.getXmlValue(u_data.lang, "lnkGridItemEditLabel");

            LinkButton lnkDocumentsHistory = row.FindControl("lnkDocumentsHistory") as LinkButton;
            lnkDocumentsHistory.Text = Config.getXmlValue(u_data.lang, "lnkGridItemHistoryLabel");

            LinkButton lnkDocumentsDelete = row.FindControl("lnkDocumentsDelete") as LinkButton;
            lnkDocumentsDelete.Text = Config.getXmlValue(u_data.lang, "dataBtnDeleteLabel");

            Literal ltrDocumentsGridDocumentType = row.FindControl("ltrDocumentsGridDocumentType") as Literal;          
            byte documentType = Convert.ToByte(grdDocuments.DataKeys[index].Values["DOCUMENT_TYPE"]);
            if (documentType == 0) ltrDocumentsGridDocumentType.Text = Config.getXmlValue(u_data.lang, "documentType1");
            if (documentType == 1) ltrDocumentsGridDocumentType.Text = Config.getXmlValue(u_data.lang, "documentType2");
            if (documentType == 2) ltrDocumentsGridDocumentType.Text = Config.getXmlValue(u_data.lang, "documentType3");
            index++;
        }
    }


    private void FillPageData(UserData u_data)
    {
        ltrPageHeaderDocumentsLabel.Text = Config.getXmlValue(u_data.lang, "pageHeaderDocumentsLabel");
        ltrAddNewDocumentsLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddNewDocumentsLabel");

        ltrAddOrUpdateDocumentMainDataPanelHeader.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentMainDataPanelHeader");

        ltrAddOrUpdateDocNumLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocNumLabel");
        txtAddOrUpdateDocNum.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocNumLabel"));
        ltrAddOrUpdateDocumentTypeLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentTypeLabel");
        ltrAddOrUpdateDocumentDateLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentDateLabel");
        txtAddOrUpdateDocumentDate.Text = Config.HostingTime.ToString("dd.MM.yyyy");
        ltrAddOrUpdateWarouseLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateWarouseLabel");
        ltrAddOrUpdateDocumentNoteLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentNoteLabel");
        txtAddOrUpdateDocumentNote.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentNoteLabel"));

        drlAddOrUpdateDocumentType.Items.Clear();
        drlAddOrUpdateDocumentType.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentTypeLabel"), ""));
        drlAddOrUpdateDocumentType.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "documentType1"), "0"));
        drlAddOrUpdateDocumentType.Items.Insert(2, new ListItem(Config.getXmlValue(u_data.lang, "documentType2"), "1"));
        drlAddOrUpdateDocumentType.Items.Insert(3, new ListItem(Config.getXmlValue(u_data.lang, "documentType3"), "2"));


        drlAddOrUpdateWarouse.Items.Clear();
        drlAddOrUpdateWarouse.DataTextField = "DESCRIPTION";
        drlAddOrUpdateWarouse.DataValueField = "RECORD_ID";
        drlAddOrUpdateWarouse.DataSource = db.GetWarehouse("ACTIVE");
        drlAddOrUpdateWarouse.DataBind();
        drlAddOrUpdateWarouse.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateWarouseLabel"), ""));

        ltrAddOrUpdateItemlineTab.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateItemlineTabLabel");
        ltrAddOrUpdateItemLineModal.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateItemLineModalLabel");

        txtAddOrUpdateDocument_ItemUnitCoeficient.Attributes.Add("readonly", "readonly");
        ltrAddOrUpdateDocument_ItemLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_ItemLabel");

        drlAddOrUpdateDocument_Item.Items.Clear();
        /* drlAddOrUpdateDocument_Item.DataTextField = "NAME";
         drlAddOrUpdateDocument_Item.DataValueField = "RECORD_ID";
         drlAddOrUpdateDocument_Item.DataSource = db.GetItemList();
          drlAddOrUpdateDocument_Item.DataBind();*/
        //Server.HtmlEncode();
        DataTable dtItemList = db.GetItemList();
        for (int i = 0; i < dtItemList.Rows.Count; i++)
        {
            drlAddOrUpdateDocument_Item.Items.Add(new ListItem(dtItemList.Rows[i]["NAME"].ToString(), dtItemList.Rows[i]["RECORD_ID"].ToString()));
        }
        drlAddOrUpdateDocument_Item.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_ItemLabel"), "0"));

        ltrAddOrUpdateDocument_ItemUnitLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_ItemUnitLabel");
        ltrAddOrUpdateDocument_ItemUnitCoeficientLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_ItemUnitCoeficientLabel");
        txtAddOrUpdateDocument_ItemUnitCoeficient.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_ItemUnitCoeficientLabel"));

        ltrAddOrUpdateDocument_QuantityLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_QuantityLabel");
        txtAddOrUpdateDocument_Quantity.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_QuantityLabel"));
        ltrAddOrUpdateDocument_PriceLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_PriceLabel");
        txtAddOrUpdateDocument_Price.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_PriceLabel"));
        ltrAddOrUpdateDocument_ItemLineNoteLabel.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentNoteLabel");
        txtAddOrUpdateDocument_ItemLineNote.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentNoteLabel"));

    }
   
    
    //Add new Document
    protected void lnkAddNewDocuments_Click(object sender, EventArgs e)
    {
        hdnOperationType.Value = "INSERT";
        int docNumOut;
        int getDocumentId = db.GetDocumentId(u_data.LogonUserID.ToString().PadLeft(6, '_'), Config.HostingTime, (short)u_data.LogonUserID, out docNumOut);
        if (getDocumentId < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        string _docNum = docNumOut.ToString().PadLeft(6, '0');
        hdnDocumentRecordId.Value = getDocumentId.ToString();
        btnInsertUpdateDocument.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnInsertUpdateDocumentCancel.Text = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        txtAddOrUpdateDocNum.Text = _docNum;

        FillLineByDocId(getDocumentId);

        MultiViewMain.ActiveViewIndex = 1;
        
        PageComponetEnable(); 
    }


    void FillLineByDocId(int documentId)
    {
        grdItemLine.DataSource = db.GetItemLineByDocId(documentId);
        grdItemLine.DataBind();
    }
   
    //AddLine
    protected void lnkAddOrUpdateItemLineModal_Click(object sender, EventArgs e)
    {
        ltrAddOrUpdateDocument_ItemLineMessage.Text = "";
        ltrAddOrUpdateDocument_ItemLineModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_ItemLineModalHeaderLabel");
        btnAddOrUpdateDocument_ItemLine.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnAddOrUpdateDocument_ItemLineCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        if (drlAddOrUpdateDocumentType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentTypeSelectLabel"));
            return;
        }
        if (drlAddOrUpdateWarouse.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentWarouseLabel"));
            return;
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOrUpdateDocumentItemLineModal();", true);
    }
   
    protected void drlAddOrUpdateDocument_Item_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrAddOrUpdateDocument_ItemLineMessage.Text = "";
        int itemRecId = Convert.ToInt32(drlAddOrUpdateDocument_Item.SelectedValue);

        drlAddOrUpdateDocument_ItemUnit.Items.Clear();
        drlAddOrUpdateDocument_ItemUnit.DataTextField = "UNIT_NAME";
        drlAddOrUpdateDocument_ItemUnit.DataValueField = "RECORD_ID";
        drlAddOrUpdateDocument_ItemUnit.DataSource = db.GetItemUnitById(itemRecId);
        drlAddOrUpdateDocument_ItemUnit.DataBind();

        if (drlAddOrUpdateDocument_ItemUnit.Items.Count > 0)
        {
            int unitRecId = Convert.ToInt32(drlAddOrUpdateDocument_ItemUnit.SelectedValue);
            decimal coeficient = db.GetUnitCoeficient(unitRecId);
            txtAddOrUpdateDocument_ItemUnitCoeficient.Text = coeficient < 0 ? "0" : coeficient.ToString();
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOrUpdateDocumentItemLineModal();", true);

    }
    protected void drlAddOrUpdateDocument_ItemUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrAddOrUpdateDocument_ItemLineMessage.Text = "";
        int unitRecId = Convert.ToInt32(drlAddOrUpdateDocument_ItemUnit.SelectedValue);
        decimal coeficient = db.GetUnitCoeficient(unitRecId);
        txtAddOrUpdateDocument_ItemUnitCoeficient.Text = coeficient < 0 ? "0" : coeficient.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOrUpdateDocumentItemLineModal();", true);
    }

    Config _configFile = new Config();
    //accept line
    protected void btnAddOrUpdateDocument_ItemLine_Click(object sender, EventArgs e)
    {
        ltrAddOrUpdateDocument_ItemLineMessage.Text = "";
        if (drlAddOrUpdateDocument_Item.SelectedValue == "0")
        {
            ltrAddOrUpdateDocument_ItemLineMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_ItemLabel"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOrUpdateDocumentItemLineModal();", true);
            return;
        }
        if (drlAddOrUpdateDocument_ItemUnit.Items.Count == 0)
        {
            ltrAddOrUpdateDocument_ItemLineMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocument_ItemLineMessageM1"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOrUpdateDocumentItemLineModal();", true);
            return;
        }

        if (_configFile.ConvertToDecimal(txtAddOrUpdateDocument_Quantity.Text) <= 0)
        {
            ltrAddOrUpdateDocument_ItemLineMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddOrUpdateDocument_QuantityMessage"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOrUpdateDocumentItemLineModal();", true);
            return;
        }

        if (_configFile.ConvertToDecimal(txtAddOrUpdateDocument_Price.Text) <= 0)
        {
            ltrAddOrUpdateDocument_ItemLineMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddOrUpdateDocument_PriceMessage"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOrUpdateDocumentItemLineModal();", true);
            return;
        }
        if (_configFile.ConvertToDecimal(txtAddOrUpdateDocument_ItemUnitCoeficient.Text) < 0)
        {
            ltrAddOrUpdateDocument_ItemLineMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddOrUpdateDocument_ItemUnitCoeficientMessage"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOrUpdateDocumentItemLineModal();", true);
            return;
        }

        int pItemDocumentRecId = Convert.ToInt32(hdnDocumentRecordId.Value);
        byte pDocumentType = Convert.ToByte(drlAddOrUpdateDocumentType.SelectedValue);
        int pItemRecId = Convert.ToInt32(drlAddOrUpdateDocument_Item.SelectedValue);
        short pWarehouseRecId = Convert.ToInt16(drlAddOrUpdateWarouse.SelectedValue);
        byte pInOut = 1;
        if (pDocumentType == 0 || pDocumentType == 2) pInOut = 1;
        if (pDocumentType == 1) pInOut = 0;
       
        decimal pQuantity = _configFile.ConvertToDecimal(txtAddOrUpdateDocument_Quantity.Text);
        decimal pPrice = _configFile.ConvertToDecimal(txtAddOrUpdateDocument_Price.Text);
        decimal coeficient = _configFile.ConvertToDecimal(txtAddOrUpdateDocument_ItemUnitCoeficient.Text);
        decimal pAmount = (coeficient == 0 ? 1 : coeficient) * pPrice * pQuantity;

     
       
        string pNote = txtAddOrUpdateDocument_ItemLineNote.Text;
        int pItemUnitRecId = Convert.ToInt32(drlAddOrUpdateDocument_ItemUnit.SelectedValue);

        string _result = db.AddItemLine(pItemDocumentRecId, pDocumentType, pItemRecId, pWarehouseRecId, pInOut, pQuantity, pPrice, pAmount, pNote, pItemUnitRecId, (short)u_data.LogonUserID);
        if (!_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        drlAddOrUpdateDocument_Item.SelectedValue = "0";
        drlAddOrUpdateDocument_ItemUnit.Items.Clear();
        txtAddOrUpdateDocument_ItemUnitCoeficient.Text = "";
        txtAddOrUpdateDocument_Quantity.Text = "";
        txtAddOrUpdateDocument_Price.Text = "";
        txtAddOrUpdateDocument_ItemLineNote.Text = "";
        FillLineByDocId(pItemDocumentRecId);
    }
    protected void grdItemLine_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdItemLine_ItemCodeLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdItemLine_ItemNameLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdItemLine_ItemQuantityLabel");
            e.Row.Cells[3].Text = Config.getXmlValue(u_data.lang, "grdItemLine_ItemUnitNameLabel");
            e.Row.Cells[4].Text = Config.getXmlValue(u_data.lang, "grdItemLine_ItemPriceLabel");
            e.Row.Cells[5].Text = Config.getXmlValue(u_data.lang, "grdItemLine_ItemAmountLabel");
        }
    }

    //delete itemline
    protected void lnkItemLineDeleteData_Click(object sender, EventArgs e)
    {
        LinkButton lnkItemLineDeleteData = (LinkButton)sender;
        hdnFieldDeleteItemLineID.Value = lnkItemLineDeleteData.CommandArgument;

        ltrDeleteItemLineModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrDeleteItemLineModalHeaderMessage");
        ltrDeleteItemLineModalBody.Text = Config.getXmlValue(u_data.lang, "ltrDeleteItemLineModalBodyMessage");

        btnDeleteItemLine.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnDeleteItemLineCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteDocumentItemLineModal();", true);   
    }
    protected void btnDeleteItemLine_Click(object sender, EventArgs e)
    {
       int itemLineRecId = Convert.ToInt32(hdnFieldDeleteItemLineID.Value);
       string _result = db.DeleteItemLine(itemLineRecId);

       if (!_result.Equals("OK"))
       {
           ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
           ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
       }
       int pItemDocumentRecId = Convert.ToInt32(hdnDocumentRecordId.Value);
       FillLineByDocId(pItemDocumentRecId);
    }
   
    // Add or Update Document
    protected void btnInsertUpdateDocument_Click(object sender, EventArgs e)
    {
        ltrAlertMsg.Text = "";
        if (!Config.IsNumeric(txtAddOrUpdateDocNum.Text) || txtAddOrUpdateDocNum.Text.Trim().Length != 6)
        {
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddOrUpdateDocNumEmptyMessage"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        if (drlAddOrUpdateDocumentType.SelectedValue == "")
        {
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentTypeSelectLabel"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        DateTime dtAddOrUpdateDocumentDate;
        bool dateIsSuccess = false;
        try
        {
            dtAddOrUpdateDocumentDate = Config.ToDate(txtAddOrUpdateDocumentDate.Text, out dateIsSuccess);
            if (!dateIsSuccess)
            {
                ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddOrUpdateDocumentDateEmptyMessage"));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                return;
            }
        }
        catch
        {
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddOrUpdateDocumentDateEmptyMessage"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        
        if (drlAddOrUpdateWarouse.SelectedValue == "")
        {
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "ltrAddOrUpdateDocumentWarouseLabel"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        int documentRecordId = Convert.ToInt32(hdnDocumentRecordId.Value);

         DateTime? pModifiedDate = null; if (hdnOperationType.Value == "UPDATE") pModifiedDate = Config.HostingTime; 
         short? pModifiedUser = null; if (hdnOperationType.Value == "UPDATE") pModifiedUser = (short)u_data.LogonUserID;

         string _result = db.UpdateItemDocument(documentRecordId, 
                                                txtAddOrUpdateDocNum.Text.Trim(), 
                                                Convert.ToByte(drlAddOrUpdateDocumentType.SelectedValue), 
                                                dtAddOrUpdateDocumentDate, 
                                                txtAddOrUpdateDocumentNote.Text,
                                                Convert.ToInt16(drlAddOrUpdateWarouse.SelectedValue), 
                                                pModifiedDate, 
                                                pModifiedUser);
         if (_result.Equals("OK"))
         {
             MultiViewMain.ActiveViewIndex = 0;
         }
         else if (_result.Equals("BUSY"))
         {
             ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "documentNumberBusyMessage"));
             ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
             return;
         }
         else
         {
             ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
             ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
             return;
         }

         if (hdnOperationType.Value == "INSERT") Session["GridDocumentPageIndex"] = 0;

         Config.Rd("/documents");
        

    }
    protected void grdDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdDocumentsDocumentNumberLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdDocumentsDocumentDateLabel");
            e.Row.Cells[3].Text = Config.getXmlValue(u_data.lang, "grdDocumentsDocumentTypeLabel");
            e.Row.Cells[4].Text = Config.getXmlValue(u_data.lang, "grdDocumentsAmountLabel");
            e.Row.Cells[5].Text = Config.getXmlValue(u_data.lang, "grdDocumentsWarehouseLabel");
        }
    }
    protected void grdDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["GridDocumentPageIndex"] = e.NewPageIndex;
        grdDocuments.PageIndex = Convert.ToInt32(Session["GridDocumentPageIndex"]);
        FillDocuments(Convert.ToInt32(Session["GridDocumentPageIndex"])); 
    }
   
    protected void drlGridDocumentPageSize_SelectedIndexChanged(object sender, EventArgs e)
    {
        grdDocuments.PageIndex = 0;
        Session["drlGridDocumentPageSize"] = drlGridDocumentPageSize.SelectedValue;
        Session["GridDocumentPageIndex"] = 0;
        FillDocuments(0); // when pagesize changed - first page load
    }
   
    //edit document & line
    protected void lnkDocumentsDataEdit_Click(object sender, EventArgs e)
    {
        PageComponetEnable();
        LinkButton lnkDocumentsDataEdit = (LinkButton)sender;
        hdnDocumentRecordId.Value = lnkDocumentsDataEdit.CommandArgument;
        hdnOperationType.Value = "UPDATE";
        btnInsertUpdateDocument.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnInsertUpdateDocumentCancel.Text = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        MultiViewMain.ActiveViewIndex = 1;
        DataRow drDocument = db.GetDocumentByRecordId(Convert.ToInt32(hdnDocumentRecordId.Value));
        if (drDocument == null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }

        FillLineByDocId(Convert.ToInt32(hdnDocumentRecordId.Value));

        txtAddOrUpdateDocNum.Text = Convert.ToString(drDocument["DOCUMENT_NUMBER"]);
        drlAddOrUpdateDocumentType.SelectedValue = Convert.ToString(drDocument["DOCUMENT_TYPE"]);
        txtAddOrUpdateDocumentDate.Text = Convert.ToDateTime(drDocument["DOCUMENT_DATE"]).ToString("dd.MM.yyyy");
        drlAddOrUpdateWarouse.SelectedValue = Convert.ToString(drDocument["WAREHOUSE_RECID"]);
        txtAddOrUpdateDocumentNote.Text = Convert.ToString(drDocument["NOTE"]);

    }

    //Cancel
    protected void btnInsertUpdateDocumentCancel_Click(object sender, EventArgs e)
    {
        db.DeletePendingDocument((short)u_data.LogonUserID);
        Config.Rd("~/documents");
    }
    protected void lnkDocumentsView_Click(object sender, EventArgs e)
    {
        LinkButton lnkDocumentsView = (LinkButton)sender;
        hdnDocumentRecordId.Value = lnkDocumentsView.CommandArgument;
        hdnOperationType.Value = "UPDATE";    
        MultiViewMain.ActiveViewIndex = 1;
        DataRow drDocument = db.GetDocumentByRecordId(Convert.ToInt32(hdnDocumentRecordId.Value));
        if (drDocument == null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }

        FillLineByDocId(Convert.ToInt32(hdnDocumentRecordId.Value));

        txtAddOrUpdateDocNum.Text = Convert.ToString(drDocument["DOCUMENT_NUMBER"]);
        drlAddOrUpdateDocumentType.SelectedValue = Convert.ToString(drDocument["DOCUMENT_TYPE"]);
        txtAddOrUpdateDocumentDate.Text = Convert.ToDateTime(drDocument["DOCUMENT_DATE"]).ToString("dd.MM.yyyy");
        drlAddOrUpdateWarouse.SelectedValue = Convert.ToString(drDocument["WAREHOUSE_RECID"]);
        txtAddOrUpdateDocumentNote.Text = Convert.ToString(drDocument["NOTE"]);
        btnInsertUpdateDocumentCancel.Text = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");

        PageComponetDisable();
    }


    void PageComponetDisable()
    {
        btnInsertUpdateDocument.Visible = false;
        txtAddOrUpdateDocNum.Attributes.Add("readonly", "");
        drlAddOrUpdateDocumentType.Attributes.Add("disabled", "disabled");
        txtAddOrUpdateDocumentDate.Attributes.Add("disabled", "disabled");
        drlAddOrUpdateWarouse.Attributes.Add("disabled", "disabled");
        txtAddOrUpdateDocumentNote.Attributes.Add("readonly", "");
        DivAddOrUpdateItemLineModal.Visible = false;
        grdItemLine.Columns[6].Visible = false;  
    }

    void PageComponetEnable()
    {
        btnInsertUpdateDocument.Visible = true;
        txtAddOrUpdateDocNum.Attributes.Remove("readonly");
        drlAddOrUpdateDocumentType.Attributes.Remove("disabled");
        txtAddOrUpdateDocumentDate.Attributes.Remove("disabled");
        drlAddOrUpdateWarouse.Attributes.Remove("disabled");
        txtAddOrUpdateDocumentNote.Attributes.Remove("readonly");
        DivAddOrUpdateItemLineModal.Visible = true;
        grdItemLine.Columns[6].Visible = true;
    }
    protected void lnkDocumentsHistory_Click(object sender, EventArgs e)
    {
        LinkButton lnkDocumentsHistory = (LinkButton)sender;
        int documentRecordId = Convert.ToInt32(lnkDocumentsHistory.CommandArgument);
        DataRow drDocument = db.GetDocumentByRecordId(documentRecordId);
        ltrViewDocument_HistoryModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrViewDocument_HistoryModalHeaderLabel");

        ltrViewDocument_HistoryCreateUserLabel.Text = Config.getXmlValue(u_data.lang, "ltrViewAsstets_HistoryCreateUserLabel");
        ltrViewDocument_HistoryCreateUser.Text = "<i>" + Convert.ToString(drDocument["CREATEDBY_USER"]) + "</i>";
        ltrViewDocument_HistoryCreateDateLabel.Text = Config.getXmlValue(u_data.lang, "ltrViewDocument_HistoryCreateDateLabel");
        ltrViewDocument_HistoryCreateDate.Text = Convert.ToString(drDocument["CREATED_DATE"]).Trim().Length != 0 ? "<i>" + Convert.ToDateTime(drDocument["CREATED_DATE"]).ToString("dd.MM.yyyy HH:mm") + "</i>" : "<i>" + Convert.ToString(drDocument["CREATED_DATE"]) + "</i>";

        ltrViewDocument_HistoryModifiedUserLabel.Text = Config.getXmlValue(u_data.lang, "ltrViewDocument_HistoryModifiedUserLabel");
        ltrViewDocument_HistoryModifiedUser.Text = "<i>" + Convert.ToString(drDocument["MODIFIEDBY_USER"]) + "</i>";
        ltrViewDocument_HistoryModifiedDateLabel.Text = Config.getXmlValue(u_data.lang, "ltrViewDocument_HistoryModifiedDateLabel");
        ltrViewDocument_HistoryModifiedDate.Text = Convert.ToString(drDocument["MODIFIED_DATE"]).Trim().Length != 0 ? "<i>" + Convert.ToDateTime(drDocument["MODIFIED_DATE"]).ToString("dd.MM.yyyy HH:mm") + "</i>" : Convert.ToString(drDocument["MODIFIED_DATE"]);
        btnViewDocument_HistoryCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCloseButtonLabel");

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDocumentHistoryModal();", true);
    }
    
    //delete documents
    protected void lnkDocumentsDelete_Click(object sender, EventArgs e)
    {
        LinkButton lnkDocumentsDelete = (LinkButton)sender;
        hdnFieldDocumentRecordID.Value = lnkDocumentsDelete.CommandArgument;
        ltrDeleteDocumentModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrDeleteDocumentModalHeaderLabel");
        ltrDeleteDocumentModalBody.Text = Config.getXmlValue(u_data.lang, "ltrDeleteDocumentModalBodyMessage");


        btnDeleteDocument.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnDeleteDocumentCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteDocument_DeleteItemModal();", true);
    }
   
    protected void btnDeleteDocument_Click(object sender, EventArgs e)
    {
        int deletedDocumentRecordId = Convert.ToInt32(hdnFieldDocumentRecordID.Value);

        string _result = db.DeleteDocument(deletedDocumentRecordId);

        if (_result.Equals("OK"))
        {
            Config.Rd("~/documents");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
    }
}