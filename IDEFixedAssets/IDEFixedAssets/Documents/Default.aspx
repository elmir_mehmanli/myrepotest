﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Documents_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet" />
    <link href="../lib/spectrum/spectrum.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script>
        function checkDocumentsGrid(ele) {
            var checkboxes = document.getElementsByTagName('input');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        if (checkboxes[i].id.indexOf('chkDocumnetsGridId') != -1) {
                            checkboxes[i].checked = true;
                        }
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        if (checkboxes[i].id.indexOf('chkDocumnetsGridId') != -1) {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
        }
    </script>

    <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

            .pagination-ys table > tbody > tr :hover {
                display: inline;
                background-color: rgb(233, 236, 239);
            }

            .pagination-ys table > tbody > tr > td > a,
            .pagination-ys table > tbody > tr > td > span {
                position: relative;
                float: left;
                padding: 8px 12px;
                line-height: 1.42857143;
                text-decoration: none;
                color: #5b93d3;
                background-color: #ffffff;
                border: 1px solid #dddddd;
                margin-left: -1px;
            }

            .pagination-ys table > tbody > tr > td > span {
                position: relative;
                float: left;
                padding: 8px 12px;
                line-height: 1.42857143;
                text-decoration: none;
                margin-left: -1px;
                z-index: 2;
                color: #aea79f;
                background-color: #f5f5f5;
                border-color: #dddddd;
                cursor: default;
            }

            .pagination-ys table > tbody > tr > td:first-child > a,
            .pagination-ys table > tbody > tr > td:first-child > span {
                margin-left: 0;
                border-bottom-left-radius: 4px;
                border-top-left-radius: 4px;
            }

            .pagination-ys table > tbody > tr > td:last-child > a,
            .pagination-ys table > tbody > tr > td:last-child > span {
                border-bottom-right-radius: 4px;
                border-top-right-radius: 4px;
            }

            .pagination-ys table > tbody > tr > td > a:hover,
            .pagination-ys table > tbody > tr > td > span:hover,
            .pagination-ys table > tbody > tr > td > a:focus,
            .pagination-ys table > tbody > tr > td > span:focus {
                color: #5b93d3;
                background-color: #eeeeee;
                border-color: #dddddd;
            }
    </style>

    <style>
        .dropdown-menu {
            min-width: 120%;
            margin: 0.125rem 0 0;
            font-size: 0.875rem;
            list-style: none;
            transform: translate3d(0px, 0px, 0px)!important;
        }

        .list-group-item {
            position: relative;
            display: block;
            padding: 5px 10px;
            background-color: #fff;
            border: 0px;
        }


            .list-group-item :hover {
                background-color: #dadada;
                padding: 4px 7px;
            }


        .popover-body {
            padding: 0px !important;
        }

        .tableCustom th {
            color: #ffffff !important;
        }
    </style>

    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">IDE Fixed Assets</a>
            <span class="breadcrumb-item active">
                <asp:Literal ID="ltrPageHeaderDocumentsLabel" runat="server"></asp:Literal>
            </span>
        </nav>

        <div class="sl-pagebody">
            <!-- SMALL MODAL -->
            <div id="alertmodal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                        <div class="modal-footer justify-content-right" style="padding: 5px">
                            <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
                <!-- modal-dialog -->
            </div>
            <!-- modal -->

            <div class="card pd-20 pd-sm-15">
                <asp:MultiView ID="MultiViewMain" runat="server" ActiveViewIndex="0">
                    <asp:View ID="ViewListDocuments" runat="server">
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:LinkButton ID="lnkAddNewDocuments" runat="server" class="btn btn-outline-info btn-block" OnClick="lnkAddNewDocuments_Click">
                                    <i class="icon ion-plus-circled"></i>
                                    <asp:Literal ID="ltrAddNewDocumentsLabel" runat="server"></asp:Literal>
                                </asp:LinkButton>
                                <asp:HiddenField ID="hdnOperationType" runat="server" />
                                <asp:HiddenField ID="hdnDocumentRecordId" runat="server" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div>
                            <div style="float: right; margin-bottom: 7px">
                                <asp:DropDownList ID="drlGridDocumentPageSize" runat="server" AutoPostBack="True" CssClass="form-control" Width="100px" OnSelectedIndexChanged="drlGridDocumentPageSize_SelectedIndexChanged">
                                    <asp:ListItem Selected="True">10</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div style="clear: both"></div>
                        </div>


                        <div class="table-responsive">
                            <asp:GridView class="table tableCustom display responsive nowrap dataTable no-footer dtr-inline" ID="grdDocuments" runat="server" DataKeyNames="RECORD_ID,DOCUMENT_TYPE"
                                OnRowDataBound="grdDocuments_RowDataBound" OnPageIndexChanging="grdDocuments_PageIndexChanging" AutoGenerateColumns="False" GridLines="None" AllowPaging="True" AllowCustomPaging="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <label class="ckbox">
                                                <input type="checkbox" id="chkDocumentsHeader" runat="server" onchange="checkDocumentsGrid(this)" />
                                                <span></span>
                                            </label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <label class="ckbox">
                                                <asp:CheckBox ID="chkDocumnetsGridId" runat="server"></asp:CheckBox>
                                                <span></span>
                                            </label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DOCUMENT_NUMBER" />
                                    <asp:BoundField DataField="DOCUMENT_DATE1" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltrDocumentsGridDocumentType" runat="server"></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AMOUNT" />
                                    <asp:BoundField DataField="WAREHOUSE" />
                                    <asp:BoundField DataField="NOTE" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="dropdown">
                                                <asp:LinkButton ID="lnkActionDocumentsData" runat="server" class="btn btn-link" data-toggle="dropdown"><i class="icon ion-more"></i></asp:LinkButton>
                                                <ul class="dropdown-menu">
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkDocumentsDataEdit" OnClick="lnkDocumentsDataEdit_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkDocumentsView" OnClick="lnkDocumentsView_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkDocumentsHistory" OnClick="lnkDocumentsHistory_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkDocumentsDelete" OnClick="lnkDocumentsDelete_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                                    </li>
                                                </ul>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="bg-dark" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>


                        <!-- Begin View History modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="viewDocumentHistoryModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                    &nbsp&nbsp
                                                 <asp:Literal ID="ltrViewDocument_HistoryModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row" style="width: 450px">
                                                        <div class="col-lg-8">
                                                            <asp:Literal ID="ltrViewDocument_HistoryCreateUserLabel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:Literal ID="ltrViewDocument_HistoryCreateUser" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="width: 450px">
                                                        <div class="col-lg-8">
                                                            <asp:Literal ID="ltrViewDocument_HistoryCreateDateLabel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:Literal ID="ltrViewDocument_HistoryCreateDate" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="width: 450px">
                                                        <div class="col-lg-8">
                                                            <asp:Literal ID="ltrViewDocument_HistoryModifiedUserLabel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:Literal ID="ltrViewDocument_HistoryModifiedUser" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="width: 450px">
                                                        <div class="col-lg-8">
                                                            <asp:Literal ID="ltrViewDocument_HistoryModifiedDateLabel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:Literal ID="ltrViewDocument_HistoryModifiedDate" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button runat="server" id="btnViewDocument_HistoryCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- End View Historymodal-dialog -->



                        <!-- Begin Delete Item modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel17" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="deleteDocumentModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-trash-a"></i>
                                                    <asp:Literal ID="ltrDeleteDocumentModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    &nbsp&nbsp
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <asp:HiddenField ID="hdnFieldDocumentRecordID" runat="server" />
                                            <div class="card pd-20 pd-sm-30">
                                                <div class="row mg-t-20">
                                                    <h6>
                                                        <asp:Literal ID="ltrDeleteDocumentModalBody" runat="server"></asp:Literal></h6>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="deleteDocument_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnDeleteDocument" OnClick ="btnDeleteDocument_Click" class="btn btn-info pd-x-20" runat="server"
                                                    OnClientClick="this.style.display = 'none';
                                                    document.getElementById('deleteDocument_loading').style.display = '';
                                                    document.getElementById('ContentPlaceHolder1_btnDeleteDocumentCancel').style.display = 'none';
                                                    document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnDeleteDocumentCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnDeleteDocument" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End Delete Item modal-dialog -->



                    </asp:View>

                    <asp:View ID="ViewDocumentDetails" runat="server">
                        <div class="d-flex align-items-center justify-content-end ht-md-60 bd pd-x-20 mg-t-10" style="border-bottom-width: 0px;">
                            <div class="d-md-flex pd-y-20 pd-md-y-0">
                                <img id="imgInsertUpdateDocument_loading" style="display: none" src="../img/loader.gif" />
                                <asp:Button ID="btnInsertUpdateDocument" class="btn btn-info" runat="server"
                                    OnClick="btnInsertUpdateDocument_Click"
                                    OnClientClick="this.style.display = 'none';
                                                     document.getElementById('imgInsertUpdateDocument_loading').style.display = '';" />
                                <asp:Button ID="btnInsertUpdateDocumentCancel" class="btn btn-secondary mg-md-l-10 mg-t-10 mg-md-t-0" runat="server" OnClick="btnInsertUpdateDocumentCancel_Click" />
                            </div>
                        </div>

                        <div class="card bd">
                            <div class="card-header bd-b">
                                <asp:Literal ID="ltrAddOrUpdateDocumentMainDataPanelHeader" runat="server"></asp:Literal>
                            </div>
                            <div class="card-body bg-white-200">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <asp:Literal ID="ltrAddOrUpdateDocNumLabel" runat="server"></asp:Literal><span class="tx-danger"> *</span>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtAddOrUpdateDocNum" ReadOnly MaxLength="6" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <asp:Literal ID="ltrAddOrUpdateDocumentTypeLabel" runat="server"></asp:Literal><span class="tx-danger"> *</span>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drlAddOrUpdateDocumentType" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <asp:Literal ID="ltrAddOrUpdateDocumentDateLabel" runat="server"></asp:Literal><span class="tx-danger"> *</span>
                                        <div class="input-group" runat="server" id="DivAddOrUpdateDocumentDate">
                                            <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                            <asp:TextBox ID="txtAddOrUpdateDocumentDate" type="text" class="form-control fc-datepicker" placeholder="dd.mm.yyyy" runat="server" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <asp:Literal ID="ltrAddOrUpdateWarouseLabel" runat="server"></asp:Literal><span class="tx-danger"> *</span>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drlAddOrUpdateWarouse" runat="server" class="selectpicker form-control" data-live-search="true" data-hide-disabled="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                </div>
                                <div class="row  mg-t-5">
                                    <div class="col-lg-12">
                                        <asp:Literal ID="ltrAddOrUpdateDocumentNoteLabel" runat="server"></asp:Literal>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtAddOrUpdateDocumentNote" TextMode="MultiLine" Rows="2" MaxLength="201" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card bd">
                            <div class="card-header bd-b">
                                <ul class="nav nav-tabs card-header-tabs">
                                    <li class="nav-item">
                                        <asp:LinkButton Enabled="false" ID="lnkAddOrUpdateItemlineTab" CssClass="nav-link active" runat="server">
                                            <asp:Literal ID="ltrAddOrUpdateItemlineTab" runat="server"></asp:Literal>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body color-gray-lighter">
                                <div class="row mg-t-20">
                                    <div class="col-lg-2" runat="server" id="DivAddOrUpdateItemLineModal">
                                        <asp:LinkButton ID="lnkAddOrUpdateItemLineModal" OnClick="lnkAddOrUpdateItemLineModal_Click" runat="server" class="btn btn-outline-info btn-block">
                                            <i class="icon ion-plus-circled"></i>
                                            <asp:Literal ID="ltrAddOrUpdateItemLineModal" runat="server"></asp:Literal>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <br />
                                <br />

                                <div class="table-responsive">
                                    <asp:GridView ID="grdItemLine" class="table table-white table-hover" runat="server" AutoGenerateColumns="False"
                                        DataKeyNames="RECORD_ID" GridLines="None" EnableModelValidation="True" OnRowDataBound="grdItemLine_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="ITEM_CODE" />
                                            <asp:BoundField DataField="ITEM_NAME" />
                                            <asp:BoundField DataField="QUANTITY" />
                                            <asp:BoundField DataField="UNIT_NAME" />
                                            <asp:BoundField DataField="PRICE" />
                                            <asp:BoundField DataField="AMOUNT" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkItemLineDeleteData" OnClick="lnkItemLineDeleteData_Click" CommandArgument='<%# Eval("RECORD_ID") %>' runat="server">
                                                    <i class="icon ion-trash-a"></i></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>

                            </div>
                        </div>



                        <!-- Begin AddOrUpdate Itemline modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="addOrUpdateDocument_ItemLineModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                    &nbsp&nbsp
                                              <asp:Literal ID="ltrAddOrUpdateDocument_ItemLineModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrAddOrUpdateDocument_ItemLineMessage" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <asp:Literal ID="ltrAddOrUpdateDocument_ItemLabel" runat="server"></asp:Literal><span class="tx-danger"> *</span>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drlAddOrUpdateDocument_Item" htmlencode="false" AutoPostBack="true" OnSelectedIndexChanged="drlAddOrUpdateDocument_Item_SelectedIndexChanged" runat="server" class="selectpicker form-control" data-live-search="true" data-hide-disabled="true"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <asp:Literal ID="ltrAddOrUpdateDocument_ItemUnitLabel" runat="server"></asp:Literal><span class="tx-danger"> *</span>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drlAddOrUpdateDocument_ItemUnit" AutoPostBack="true" OnSelectedIndexChanged="drlAddOrUpdateDocument_ItemUnit_SelectedIndexChanged" runat="server" class="form-control"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg-4">
                                                            <asp:Literal ID="ltrAddOrUpdateDocument_ItemUnitCoeficientLabel" runat="server"></asp:Literal><span class="tx-danger"> </span>
                                                            <div class="form-group">
                                                                <asp:TextBox ID="txtAddOrUpdateDocument_ItemUnitCoeficient" MaxLength="7" runat="server" class="form-control" type="text"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <asp:Literal ID="ltrAddOrUpdateDocument_QuantityLabel" runat="server"></asp:Literal><span class="tx-danger"> *</span>
                                                            <div class="form-group">
                                                                <asp:TextBox ID="txtAddOrUpdateDocument_Quantity" MaxLength="7" runat="server" class="form-control" type="text"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <asp:Literal ID="ltrAddOrUpdateDocument_PriceLabel" runat="server"></asp:Literal><span class="tx-danger"> *</span>
                                                            <div class="form-group">
                                                                <asp:TextBox ID="txtAddOrUpdateDocument_Price" MaxLength="9" runat="server" class="form-control" type="text"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg-12">
                                                            <asp:Literal ID="ltrAddOrUpdateDocument_ItemLineNoteLabel" runat="server"></asp:Literal>
                                                            <div class="form-group">
                                                                <asp:TextBox ID="txtAddOrUpdateDocument_ItemLineNote" TextMode="MultiLine" Rows="2" MaxLength="201" runat="server" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="AddOrUpdateDocument_ItemLine_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnAddOrUpdateDocument_ItemLine" class="btn btn-info pd-x-20" runat="server"
                                                    OnClick="btnAddOrUpdateDocument_ItemLine_Click"
                                                    OnClientClick="this.style.display = 'none';
                                               document.getElementById('AddOrUpdateDocument_ItemLine_loading').style.display = '';
                                               document.getElementById('ContentPlaceHolder1_btnAddOrUpdateDocument_ItemLineCancel').style.display = 'none';
                                               document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnAddOrUpdateDocument_ItemLineCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnAddOrUpdateDocument_ItemLine" />
                                <asp:PostBackTrigger ControlID="drlAddOrUpdateDocument_Item" />
                                <asp:PostBackTrigger ControlID="drlAddOrUpdateDocument_ItemUnit" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End AddOrUpdate Itemline  modal-dialog -->


                        <!-- Begin Delete ItemLine modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="deleteItemLineModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-trash-a"></i>
                                                    <asp:Literal ID="ltrDeleteItemLineModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    &nbsp&nbsp
                                                 <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <asp:HiddenField ID="hdnFieldDeleteItemLineID" runat="server" />
                                            <div class="card pd-20 pd-sm-30">
                                                <div class="row mg-t-20">
                                                    <h6>
                                                        <asp:Literal ID="ltrDeleteItemLineModalBody" runat="server"></asp:Literal></h6>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="deleteItemLine_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnDeleteItemLine" OnClick="btnDeleteItemLine_Click" class="btn btn-info pd-x-20" runat="server"
                                                    OnClientClick="this.style.display = 'none';
                                                     document.getElementById('deleteItemLine_loading').style.display = '';
                                                     document.getElementById('ContentPlaceHolder1_btnDeleteItemLineCancel').style.display = 'none';
                                                     document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnDeleteItemLineCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnDeleteItemLine" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End Delete ItemLine modal-dialog -->



                    </asp:View>

                </asp:MultiView>
            </div>

        </div>


        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }


            function openAddOrUpdateDocumentItemLineModal() {
                $('#addOrUpdateDocument_ItemLineModal').modal({ show: true });
            }

            function openDeleteDocumentItemLineModal() {
                $('#deleteItemLineModal').modal({ show: true }); 
            }

            function openDocumentHistoryModal() {
                $('#viewDocumentHistoryModal').modal({ show: true });
            }

            function openDeleteDocument_DeleteItemModal() {
                $('#deleteDocumentModal').modal({ show: true });
            }

        </script>


        <script src="../lib/jquery/jquery.js"></script>

        <style>
            .ui-datepicker {
                z-index: 1151 !important; /* has to be larger than 1050 */
            }
        </style>

        <script type="text/javascript">
            $(function () {
                'use strict';
                // Datepicker
                $('.fc-datepicker').datepicker({
                    dateFormat: 'dd.mm.yy',
                    showOtherMonths: true,
                    selectOtherMonths: true
                });
            });
        </script>

    </div>

</asp:Content>

