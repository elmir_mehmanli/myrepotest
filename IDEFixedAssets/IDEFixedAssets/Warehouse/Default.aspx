﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Warehouse_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .dropdown-menu {
            min-width: 120%;
            margin: 0.125rem 0 0;
            font-size: 0.875rem;
            list-style: none;
            transform: translate3d(0px, 0px, 0px)!important;
        }

        .list-group-item {
            position: relative;
            display: block;
            padding: 5px 10px;
            background-color: #fff;
            border: 0px;
        }


            .list-group-item :hover {
                background-color: #dadada;
                padding: 4px 7px;
            }
    </style>


    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">IDE Fixed Assets</a>
            <span class="breadcrumb-item active">
                <asp:Literal ID="ltrPageHeaderWarehouseLabel" runat="server"></asp:Literal>
            </span>
        </nav>

        <div class="sl-pagebody">

            <!-- SMALL MODAL -->
            <div id="alertmodal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                        <div class="modal-footer justify-content-right" style="padding: 5px">
                            <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
                <!-- modal-dialog -->
            </div>
            <!-- modal -->


            <div class="card pd-sm-10">

                <div class="row">
                    <div class="col-lg-2">
                        <asp:LinkButton ID="btnAddNew" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                            data-toggle="modal" data-target="#addNewWarehouseModal">
                            <i class="fa fa-database"></i>
                            <asp:Literal ID="ltrAddNewWarehouse" runat="server"></asp:Literal>
                        </asp:LinkButton>
                    </div>
                    <div class="col-lg-2">
                        <asp:DropDownList ID="drlWarehouseStatus"  class="form-control" AutoPostBack ="true" runat="server" OnSelectedIndexChanged="drlWarehouseStatus_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                    <asp:GridView ID="grdWarehouse" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" 
                        DataKeyNames="RECORD_ID,STATUS" GridLines="None" EnableModelValidation="True" OnRowDataBound="grdWarehouse_RowDataBound"
                        >
                        <Columns>
                            <asp:BoundField DataField="CODE" />
                            <asp:BoundField DataField="DESCRIPTION" />
                            <asp:BoundField DataField="STATUS" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <div class="dropdown">
                                        <asp:LinkButton ID="lnkActionWarehouseData"
                                            runat="server"
                                            class="btn btn-link" data-toggle="dropdown">
                                                <i class="icon ion-more"></i></asp:LinkButton>
                                        <ul class="dropdown-menu">
                                            <li class="list-group-item">
                                                <asp:LinkButton ID="lnkWarehouseDataEdit" OnClick ="lnkWarehouseDataEdit_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                            </li>
                                            <li class="list-group-item">
                                                <asp:LinkButton ID="lnkWarehouseChangeStatus" OnClick ="lnkWarehouseChangeStatus_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                            </li>
                                        </ul>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>


                <!-- Begin New WareHouse modal-dialog -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="addNewWarehouseModal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-database"></i>
                                            &nbsp&nbsp
                                                    <asp:Literal ID="ltrAddNewWarehouseModalHeader" runat="server"></asp:Literal>
                                        </h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrAddWarehouseMessage" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtWarehouseCode" Width ="300px" MaxLength="25" runat="server" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtWarehouseDescription" runat="server" MaxLength="51" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="add_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnWarehouseAdd" class="btn btn-info pd-x-20" runat="server"
                                            OnClick="btnWarehouseAdd_Click"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loading').style.display = '';
                                                           document.getElementById('ContentPlaceHolder1_btnWarehouseAddCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button runat="server" id="btnWarehouseAddCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnWarehouseAdd" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- End New Warehouse modal-dialog -->




                <!-- Begin Edit Warehouse modal-dialog -->
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="editWarehouseModal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                            &nbsp&nbsp
                                                <asp:Literal ID="ltrEditWarehouseModalHeader" runat="server"></asp:Literal>
                                        </h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrEditWarehouseMessage" runat="server"></asp:Literal>
                                        <asp:HiddenField ID="hdnFieldEditWarehouseID" runat="server" />

                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtEditWarehouseCode" Width ="300px" MaxLength="25" runat="server" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtEditWarehouseDescription" runat="server" MaxLength="51" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="edit_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnWarehouseEdit" class="btn btn-info pd-x-20" runat="server"
                                            OnClick ="btnWarehouseEdit_Click"
                                            OnClientClick="this.style.display = 'none';
                                                       document.getElementById('edit_loading').style.display = '';
                                                       document.getElementById('btnWarehouseEditCancel').style.display = 'none';
                                                       document.getElementById('alert_msg').style.display = 'none';" />
                                        <button runat="server" id="btnWarehouseEditCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnWarehouseEdit" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- End Edit Warehouse modal-dialog -->



                <!-- Begin Change Status Warehouse modal-dialog -->
                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="changeStatusWarehouseModal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                        
                                    <asp:Literal ID="ltrChangeStatusWarehouseModalHeader" runat="server"></asp:Literal>
                                        </h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> &nbsp&nbsp
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <asp:Literal ID="ltrChangeStatusWarehouseMessage" runat="server"></asp:Literal>
                                    <asp:HiddenField ID="hdnFieldChangeStatusWarehouseID" runat="server" />
                                    <asp:HiddenField ID="hdnFieldChangeStatusWarehouseStatus" runat="server" />
                                    <div class="card pd-20 pd-sm-30">
                                        <div class="row mg-t-20">
                                            <h6>
                                                <asp:Literal ID="ltrChangeStatusWarehouseModalBody" runat="server"></asp:Literal></h6>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="warehouseChangeStatus_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnWarehouseChangeStatus" class="btn btn-info pd-x-20" runat="server"
                                            OnClick ="btnWarehouseChangeStatus_Click"
                                            OnClientClick="this.style.display = 'none';
                                           document.getElementById('warehouseChangeStatus_loading').style.display = '';
                                           document.getElementById('ContentPlaceHolder1_btnWarehouseChangeStatusCancel').style.display = 'none';
                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button runat="server" id="btnWarehouseChangeStatusCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnWarehouseChangeStatus" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- End Block Warehouse modal-dialog -->
            </div>
        </div>
        <style>
            .card {
                border: 1px solid #dadada;
            }
        </style>

        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openAddWarehouseModal() {
                $('#addNewWarehouseModal').modal({ show: true });
            }

            function openEditWarehouseModal() {
                $('#editWarehouseModal').modal({ show: true });
            }


            function changeStatusWarehouseModal() {
                $('#changeStatusWarehouseModal').modal({ show: true });
            }
        </script>
    </div>
</asp:Content>

