﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Warehouse_Default : System.Web.UI.Page
{
    UserData u_data = new UserData();
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserData"] == null) Config.Rd("/exit");
        u_data = (UserData)Session["UserData"];

       

        if (!IsPostBack)
        {
            drlWarehouseStatus.Items.Clear();
            drlWarehouseStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlStatusActive"), "ACTIVE"));
            drlWarehouseStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "drlStatusDeActive"), "DEACTIVE"));

            /*if not admin*/
            if (!u_data.LoginUserType.Equals("admin")) Config.Rd("/index");

             /*Page Labes*/
            FillPageData(u_data);

            /*Fill Warehouse*/
            FillWarehouse(drlWarehouseStatus.SelectedValue);
        }
    }

    private void FillPageData(UserData u_data)
    {
        ltrPageHeaderWarehouseLabel.Text = Config.getXmlValue(u_data.lang, "pageHeaderWarehouseLabel");
        ltrAddNewWarehouse.Text = Config.getXmlValue(u_data.lang, "addNewWarehouseLabel");

        /*Add Warehouse Inputs and Labels*/
        ltrAddNewWarehouseModalHeader.Text = Config.getXmlValue(u_data.lang, "addNewWarehouseModalHeaderLabel");
        txtWarehouseCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "warehouseCodeLabel"));
        txtWarehouseDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "warehouseDescriptionLabel"));
        btnWarehouseAdd.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnWarehouseAddCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");

        /*Edit Warehouse Inputs and Labels*/
        ltrEditWarehouseModalHeader.Text = Config.getXmlValue(u_data.lang, "editObjectsModalHeaderLabel");
        btnWarehouseEdit.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnWarehouseEditCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        txtEditWarehouseCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "warehouseCodeLabel"));
        txtEditWarehouseDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "warehouseDescriptionLabel"));


        /*Change Status Warehouse Labels */
        ltrChangeStatusWarehouseModalHeader.Text = Config.getXmlValue(u_data.lang, "changeStatusWarehouseModalHeaderLabel");
        btnWarehouseChangeStatus.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnWarehouseChangeStatusCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");
        ltrChangeStatusWarehouseModalBody.Text = Config.getXmlValue(u_data.lang, "changeStatusWarehouseModalBodyLabel");

       
    }

    void FillWarehouse(string status)
    {
        grdWarehouse.DataSource = db.GetWarehouse(status);
        grdWarehouse.DataBind();
        grdWarehouse.UseAccessibleHeader = true;
        if (grdWarehouse.Rows.Count > 0)
        {
            grdWarehouse.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        foreach (GridViewRow row in grdWarehouse.Rows)
        {
            LinkButton lnkWarehouseDataEdit = row.FindControl("lnkWarehouseDataEdit") as LinkButton;
            lnkWarehouseDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkWarehouseChangeStatus = row.FindControl("lnkWarehouseChangeStatus") as LinkButton;
            lnkWarehouseChangeStatus.Text = Config.getXmlValue(u_data.lang, row.Cells[2].Text == "ACTIVE" ? "dataBtnDeaktivLabel" : "dataBtnAktivLabel");

            lnkWarehouseDataEdit.Visible = row.Cells[2].Text == "ACTIVE";

          
        }
    }
    
    protected void btnWarehouseAdd_Click(object sender, EventArgs e)
    {
            ltrAddWarehouseMessage.Text = "";
            if (txtWarehouseCode.Text.Trim().Length < 1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddWarehouseModal();", true);
                ltrAddWarehouseMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addWarehouseCodeEmptyMessage"));
                return;
            }
            if (txtWarehouseDescription.Text.Trim().Length < 1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddWarehouseModal();", true);
                ltrAddWarehouseMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addWarehouseDescriptionEmptyMessage"));
                return;
            }

            string result = db.AddWarehouse(txtWarehouseCode.Text.Trim(), txtWarehouseDescription.Text.Trim(), (short)u_data.LogonUserID);

            if (result.Equals("OK"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addWarehouseSuccessMessage"));
            }
            else if (result.Equals("EXISTS_WAREHOUSE"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddWarehouseModal();", true);
                ltrAddWarehouseMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addWarehouseExistsMessage"));
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            }
            FillWarehouse(drlWarehouseStatus.SelectedValue);
    }
    protected void grdWarehouse_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "warehouseCodeGridLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "warehouseDescriptionGridLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "warehouseStatusGridLabel");
        }

    }
    protected void drlWarehouseStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillWarehouse(drlWarehouseStatus.SelectedValue);
    }
    protected void lnkWarehouseDataEdit_Click(object sender, EventArgs e)
    {
        ltrEditWarehouseMessage.Text = "";
        LinkButton lnkWarehouseDataEdit = (LinkButton)sender;
        hdnFieldEditWarehouseID.Value = lnkWarehouseDataEdit.CommandArgument;
        FillWarehouseDataById(Convert.ToInt16(hdnFieldEditWarehouseID.Value));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditWarehouseModal();", true);
    }

    private void FillWarehouseDataById(short editWarehouseID)
    {
        DataRow drEditWarehouse = db.GetWarehouseById(editWarehouseID);
        if (drEditWarehouse != null)
        {
            txtEditWarehouseCode.Text = Convert.ToString(drEditWarehouse["CODE"]);
            txtEditWarehouseDescription.Text = Convert.ToString(drEditWarehouse["DESCRIPTION"]);
        }
    }
    protected void btnWarehouseEdit_Click(object sender, EventArgs e)
    {
        ltrEditWarehouseMessage.Text = "";
        if (txtEditWarehouseCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditWarehouseModal();", true);
            ltrEditWarehouseMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addWarehouseCodeEmptyMessage"));
            return;
        }
        if (txtEditWarehouseDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditWarehouseModal();", true);
            ltrEditWarehouseMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addWarehouseDescriptionEmptyMessage"));
            return;
        }

        string result = db.UpdateWarehouse(Convert.ToInt16(hdnFieldEditWarehouseID.Value), txtEditWarehouseCode.Text.Trim(),txtEditWarehouseDescription.Text.Trim(), 
            (short)u_data.LogonUserID);

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "editWarehouseSuccessMessage"));
        }
        else if (result.Equals("BUSY_WAREHOUSE"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditWarehouseModal();", true);
            ltrEditWarehouseMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "editWarehouseBusyMessage"));
            return;
        }
        else if (result.Equals("EXISTS_WAREHOUSE"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditWarehouseModal();", true);
            ltrEditWarehouseMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addWarehouseExistsMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillWarehouse(drlWarehouseStatus.SelectedValue);
    }
   
    
    protected void lnkWarehouseChangeStatus_Click(object sender, EventArgs e)
    {
        ltrChangeStatusWarehouseMessage.Text = "";
        LinkButton lnkWarehouseChangeStatus = (LinkButton)sender;
        hdnFieldChangeStatusWarehouseID.Value = lnkWarehouseChangeStatus.CommandArgument;
        int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
        string status = Convert.ToString(grdWarehouse.DataKeys[rowIndex].Values["STATUS"]);
        hdnFieldChangeStatusWarehouseStatus.Value = status;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "changeStatusWarehouseModal();", true);
    }
    protected void btnWarehouseChangeStatus_Click(object sender, EventArgs e)
    {
        ltrChangeStatusWarehouseMessage.Text = "";
        string result = db.ChangeStatusWarehouse(Convert.ToInt16(hdnFieldChangeStatusWarehouseID.Value),
            hdnFieldChangeStatusWarehouseStatus.Value == "ACTIVE" ? "DEACTIVE" : "ACTIVE", 
            (short)u_data.LogonUserID);

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "changeStatusWarehouseSuccessMessage"));
        }
        else if (result.Equals("BUSY_WAREHOUSE"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changeStatusWarehouseBusyMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillWarehouse(drlWarehouseStatus.SelectedValue);
    }
}