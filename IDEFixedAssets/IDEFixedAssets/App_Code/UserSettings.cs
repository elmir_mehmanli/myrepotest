﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public partial class DbProcess : DALC
{
    public string dbLogin(string pLogin, string pPassword, ref string pUserInfo)
    {
        string aa = Config.Sha1(pPassword);
        SqlCommand cmd = new SqlCommand("spLogin", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pLogin", pLogin);
        cmd.Parameters.AddWithValue("@pPassword", Config.Sha1(pPassword));
        cmd.Parameters.AddWithValue("@pMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        SqlParameter paramUserInfo = new SqlParameter("@pUserInfo", SqlDbType.NVarChar);
        paramUserInfo.Direction = ParameterDirection.Output;
        paramUserInfo.Size = 200;
        cmd.Parameters.Add(paramUserInfo);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            pUserInfo = Convert.ToString(cmd.Parameters["@pUserInfo"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetMenu(string lang,string user_type)
    {
        DataTable dt = new DataTable("IDE_MENU");
        string cmdText = @"select * from IDE_MENU where LANG = @lang ";
        if (user_type.Equals("user"))
        {
            cmdText += "and MENU_GRANT = @menu_grant";
        }
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@lang", lang);
        
        if (user_type.Equals("user"))
        {
            da.SelectCommand.Parameters.AddWithValue("@menu_grant", user_type);
        }

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }



    public string ChangePassword(short pUserID, string pPass, short pModifiedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spChangePassword", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pUserID", pUserID);
        cmd.Parameters.AddWithValue("@pPassword", Config.Sha1(pPass));
        cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetUserList()
    {
        DataTable dt = new DataTable("IDE_USERS");
        SqlDataAdapter da = new SqlDataAdapter("select * FROM V_IDE_USERS order by CREATED_DATE desc", SqlConn);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    
    
    public string AddUser(string     pName,
                            string   pSurname,
                            string   pLogin,
                            string   pPassword,
                            string   pContactNumber,
                            Int16    pCreatedBy_UserRecId,
                            string   pGender,
                            string   pPosition,
                            string   pPicture,
                            string   pType)
    {
        SqlCommand cmd = new SqlCommand("spAddUser", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pName", pName);
        cmd.Parameters.AddWithValue("@pSurname", pSurname);
        cmd.Parameters.AddWithValue("@pLogin", pLogin);
        cmd.Parameters.AddWithValue("@pPassword", Config.Sha1(pPassword));
        cmd.Parameters.AddWithValue("@pContactNumber", pContactNumber);
        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pCreated_Date", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pGender", pGender);
        cmd.Parameters.AddWithValue("@pPosition", pPosition);
        cmd.Parameters.AddWithValue("@pPicture", pPicture);
        cmd.Parameters.AddWithValue("@pType", pType);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public DataRow GetUserById(short userId)
    {
        DataTable dt = new DataTable("IDE_USERS_BY_ID");
        SqlDataAdapter da = new SqlDataAdapter("select * FROM V_IDE_USERS where RECORD_ID = @id", SqlConn);
        try
        {
            da.SelectCommand.Parameters.AddWithValue("@id", userId);
            da.Fill(dt);
        }
        catch
        {
            return null;
        }
        if (dt.Rows.Count == 0) return null;
        return dt.Rows[0];
    }


    public string UpdateUser(short pID,
                            string pName, 
                            string pSurname,
                            string pContactNumber,
                            short pModifiedBy_UserRecId,
                            string pGender,
                            string pPosition,
                            string pPicture,
                            string pType)
    {
        SqlCommand cmd = new SqlCommand("spUpdateUser", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pID", pID);
        cmd.Parameters.AddWithValue("@pName", pName);
        cmd.Parameters.AddWithValue("@pSurname", pSurname);
        cmd.Parameters.AddWithValue("@pContactNumber", pContactNumber);
        cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pGender", pGender);
        cmd.Parameters.AddWithValue("@pPosition", pPosition);
        cmd.Parameters.AddWithValue("@pPicture", pPicture);
        cmd.Parameters.AddWithValue("@pType", pType);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public string BlockUser(short pUserID, string pStatus, short pModifiedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spBlockUser", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pUserID", pUserID);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);
        cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

}