﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{


    public int GetItemCount(string status)
    {
        string cmdText = @"select count(1) from (select row_number() OVER (ORDER BY tab.RECORD_ID DESC) n , tab.* from
            (SELECT   t1.*, t2.UNIT_RECID, t3.CODE AS UNIT_CODE, (t4.CODE + ' - ' + t4.DESCRIPTION) AS MARK_DESCRIPTION, (t5.CODE + ' - ' + t5.DESCRIPTION) 
            AS GROUP_DESCRIPTION, dbo.getWarehouseRemain(t1.RECORD_ID) AS WAREHOUSE_REMAIN
            FROM            IDE_ITEM t1 LEFT JOIN
                                     IDE_ITEMUNIT t2 ON t1.RECORD_ID = t2.ITEM_RECID AND t2.IS_MAIN_UNIT = 1 LEFT JOIN
                                     IDE_UNIT t3 ON t2.UNIT_RECID = t3.RECORD_ID LEFT JOIN
                                     IDE_MARK t4 ON t1.MARK_RECID = t4.RECORD_ID LEFT JOIN
                                     IDE_GROUP t5 ON t1.GROUP_RECID = t5.RECORD_ID) tab where tab.STATUS = @STATUS) tab1";
        SqlCommand cmd = new SqlCommand(cmdText, SqlConn);
        cmd.Parameters.AddWithValue("@STATUS", status);
        cmd.Connection.Open();
        int value = -1;
        try
        {
            value = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }
        catch
        {
            value = -2;
            throw new Exception();
        }
        finally
        {
            cmd.Connection.Close();
        }
        return value;
    }


   


    public int GetItemUnitCount(int itemRecordId)
    {
        string cmdText = @"select count(1) from IDE_ITEMUNIT where ITEM_RECID = @itemRecordId";
        SqlCommand cmd = new SqlCommand(cmdText, SqlConn);
        cmd.Parameters.AddWithValue("@itemRecordId", itemRecordId);
        cmd.Connection.Open();
        int value = -1;
        try
        {
            value = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }
        catch
        {
            value = -2;
            throw new Exception();
        }
        finally
        {
            cmd.Connection.Close();
        }
        return value;
    }


    public DataTable GetItemList(string status,int PageIndex, int PageSize)
    {
        SqlDataAdapter da = new SqlDataAdapter(@"select tab1.* from (select row_number() OVER (ORDER BY tab.RECORD_ID DESC) n , tab.* from
            (SELECT   t1.*, t2.UNIT_RECID, t3.CODE AS UNIT_CODE, (t4.CODE + ' - ' + t4.DESCRIPTION) AS MARK_DESCRIPTION, (t5.CODE + ' - ' + t5.DESCRIPTION) 
            AS GROUP_DESCRIPTION, dbo.getWarehouseRemain(t1.RECORD_ID) AS WAREHOUSE_REMAIN
            FROM            IDE_ITEM t1 LEFT JOIN
                                     IDE_ITEMUNIT t2 ON t1.RECORD_ID = t2.ITEM_RECID AND t2.IS_MAIN_UNIT = 1 LEFT JOIN
                                     IDE_UNIT t3 ON t2.UNIT_RECID = t3.RECORD_ID LEFT JOIN
                                     IDE_MARK t4 ON t1.MARK_RECID = t4.RECORD_ID LEFT JOIN
                                     IDE_GROUP t5 ON t1.GROUP_RECID = t5.RECORD_ID) tab where tab.STATUS = @STATUS) tab1 
            where n between ((@PageIndex * @PageSize) + 1) and  ((@PageIndex + 1) * (@PageSize))", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@STATUS", status);
        da.SelectCommand.Parameters.AddWithValue("@PageIndex", PageIndex);
        da.SelectCommand.Parameters.AddWithValue("@PageSize", PageSize);
        DataTable dt = new DataTable("Items");
        da.Fill(dt);
        return dt;
    }

    public DataTable GetItemList()
    {
        SqlDataAdapter da = new SqlDataAdapter(@"select RECORD_ID , ( CODE +  ' # ' +  DESCRIPTION) as NAME from IDE_ITEM where status = 'ACTIVE' order by RECORD_ID", SqlConn);
        DataTable dt = new DataTable("Items");
        da.Fill(dt);
        return dt;
    }


    public DataRow GetItemByRecordId(int itemRecordId)
    {
        SqlDataAdapter da = new SqlDataAdapter(@"select 
            t1.CODE ,t1.DESCRIPTION,t1.DESCRIPTION2,t1.NOTE , t1.STATUS ,
            (select u.NAME + N' ' + u.SURNAME as fName  from IDE_USERS u where u.RECORD_ID = t1.CREATEDBY_USERRECID)  CREATEDBY_USER,
            t1.CREATED_DATE,
            (select u.NAME + N' ' + u.SURNAME as fName  from IDE_USERS u where u.RECORD_ID = t1.MODIFIEDBY_USERRECID)  MODIFIEDBY_USER,
            t1.MODIFIED_DATE,
            t1.MARK_RECID,
            (select IM.DESCRIPTION from IDE_MARK IM where IM.RECORD_ID = t1.MARK_RECID) MARK_DESC,
            t1.GROUP_RECID,
            (select IG.DESCRIPTION from IDE_GROUP IG where IG.RECORD_ID = t1.GROUP_RECID) GROUP_DESC,
            t1.SPECODE1_RECID,
            t1.SPECODE2_RECID,
            t1.SPECODE3_RECID,
            t1.SPECODE4_RECID,
            t1.SPECODE5_RECID,
            (select ISC.DESCRIPTION from IDE_SPECODE ISC where ISC.RECORD_ID = t1.SPECODE1_RECID and SPECODE_TYPE = 0) SPECODE1_DESC,
            (select ISC.DESCRIPTION from IDE_SPECODE ISC where ISC.RECORD_ID = t1.SPECODE2_RECID and SPECODE_TYPE = 1) SPECODE2_DESC,
            (select ISC.DESCRIPTION from IDE_SPECODE ISC where ISC.RECORD_ID = t1.SPECODE3_RECID and SPECODE_TYPE = 2) SPECODE3_DESC,
            (select ISC.DESCRIPTION from IDE_SPECODE ISC where ISC.RECORD_ID = t1.SPECODE4_RECID and SPECODE_TYPE = 3) SPECODE4_DESC,
            (select ISC.DESCRIPTION from IDE_SPECODE ISC where ISC.RECORD_ID = t1.SPECODE5_RECID and SPECODE_TYPE = 4) SPECODE5_DESC
             from IDE_ITEM t1 where RECORD_ID = @RECORD_ID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@RECORD_ID", itemRecordId);       
        DataTable dt = new DataTable("Items");
        da.Fill(dt);
        try
        {
            return dt.Rows[0];
        }
        catch
        {
            return null;
        }
    }



    public int GetItemId(string pCODE,
                        short pCreatedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spAddItem", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCODE", pCODE);
        cmd.Parameters.AddWithValue("@pCreated_Date", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);
       

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.Int);
        paramResult.Direction = ParameterDirection.Output;
        cmd.Parameters.Add(paramResult);

        int pResult = 0;
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToInt32(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = -2;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public void DeletePendingItem(short pCreatedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spDeletePendingItem", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;    
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";

        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = "ERR";
        }
        finally
        {
            cmd.Connection.Close();
        }  
    }



    public string DeleteItem(int pRecorId)
    {
        SqlCommand cmd = new SqlCommand("spDeleteItem", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pRecorId", pRecorId);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";

        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = "ERR";
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public string AddOrUpdateItem(int pItemRecId,
                                string pCode,
                                string pDescription,
                                string pDescription2,
                                string pNote,
                                string pStatus,
                                short ? pModifiedUserId,
                                DateTime ? pModifiedDate,
                                int ? pMarkId,
                                int ? pGroupId,
                                int ? pSpecode1,
                                int ? pSpecode2,
                                int ? pSpecode3,
                                int ? pSpecode4,
                                int ? pSpecode5)
    {
        SqlCommand cmd = new SqlCommand("spUpdateItem", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pItemRecId", pItemRecId);
        cmd.Parameters.AddWithValue("@pCode", pCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pDescription2", pDescription2 == null ? (object)DBNull.Value : pDescription2);
        cmd.Parameters.AddWithValue("@pNote", pNote == null ? (object)DBNull.Value : pNote);
        cmd.Parameters.AddWithValue("@pStatus", pStatus == null ? (object)DBNull.Value : pStatus);
        cmd.Parameters.AddWithValue("@pModifiedUserId", pModifiedUserId == null ? (object)DBNull.Value : pModifiedUserId);
        cmd.Parameters.AddWithValue("@pModifiedDate", pModifiedDate == null ? (object)DBNull.Value : pModifiedDate);
        cmd.Parameters.AddWithValue("@pMarkId", pMarkId == null ? (object)DBNull.Value : pMarkId);
        cmd.Parameters.AddWithValue("@pGroupId", pGroupId == null ? (object)DBNull.Value : pGroupId);
        cmd.Parameters.AddWithValue("@pSpecode1", pSpecode1 == null ? (object)DBNull.Value : pSpecode1);
        cmd.Parameters.AddWithValue("@pSpecode2", pSpecode2 == null ? (object)DBNull.Value : pSpecode2);
        cmd.Parameters.AddWithValue("@pSpecode3", pSpecode3 == null ? (object)DBNull.Value : pSpecode3);
        cmd.Parameters.AddWithValue("@pSpecode4", pSpecode4 == null ? (object)DBNull.Value : pSpecode4);
        cmd.Parameters.AddWithValue("@pSpecode5", pSpecode5 == null ? (object)DBNull.Value : pSpecode5);



        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = "ERR";
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public DataTable GetWareHouseRemains(int itemRecId)
    {
        SqlDataAdapter da = new SqlDataAdapter(@"select t_2.DESCRIPTION WAREHOUSE_NAME, t2.QUANTITY from (select t1.WAREHOUSE_RECID , sum(t1.QUANTITY) QUANTITY from 
        (select  WAREHOUSE_RECID,
            case IN_OUT when 1 then  QUANTITY else (-1) * QUANTITY end as QUANTITY 
             from IDE_ITEMLINE t1 
            where ITEM_RECID = @itemRecId) t1  group by  t1.WAREHOUSE_RECID) t2 , IDE_WAREHOUSE t_2 where t2.WAREHOUSE_RECID = t_2.RECORD_ID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@itemRecId", itemRecId);
        DataTable dt = new DataTable("Remains");
        da.Fill(dt);
        return dt;
    }
  

}
