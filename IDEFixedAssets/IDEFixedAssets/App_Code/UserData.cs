﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class UserData
{
    public int LogonUserID { get; set; }
    public string LoginUserName { get; set; }
    public string LoginUserImage { get; set; }
    public string LoginUserType { get; set; }
    public string lang { get; set; }
}