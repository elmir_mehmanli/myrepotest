﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public partial class DbProcess : DALC
{
    public string AddItemBarcode(int pItemRecordId,
                          string pCode,
                          string pDescription,
                          short pCodeType)
    {
        SqlCommand cmd = new SqlCommand("spAddBarcode", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pItemRecordId", pItemRecordId);
        cmd.Parameters.AddWithValue("@pCode", pCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pCodeType", pCodeType);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public string EditItemBarcode(int pId,
                         string pCode,
                         string pDescription,
                         short pCodeType)
    {
        SqlCommand cmd = new SqlCommand("spEditBarcode", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pId", pId);
        cmd.Parameters.AddWithValue("@pCode", pCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pCodeType", pCodeType);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public string DeleteItemBarcode(int pId)
    {
        SqlCommand cmd = new SqlCommand("spDeleteItemBarcode", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pRecorId", pId);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public DataTable GetBarcodeListByItemId(int item_record_id)
    {
        DataTable dt = new DataTable("IDE_BARCODE");

        string cmdText = @"select IB.RECORD_ID , IB.ITEM_RECID , IB.CODE , IB.DESCRIPTION,
                        IB.CODE_TYPE ,
                        case IB.CODE_TYPE when 0 then 'Barcode'
                                          when 1 then 'QrCode'
				                          when 2 then 'RFID Code' end as CODE_TYPE_DESCRIPTION
                         from IDE_BARCODE IB where ITEM_RECID = @ITEM_RECID order by  IB.RECORD_ID";


        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ITEM_RECID", item_record_id);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataRow GetBarcodeById(int id)
    {
        DataTable dt = new DataTable("IDE_BARCODE");
        string cmdText = @"select IB.RECORD_ID , IB.ITEM_RECID , IB.CODE , IB.DESCRIPTION,
                        IB.CODE_TYPE ,
                        case IB.CODE_TYPE when 0 then 'Barcode'
                                          when 1 then 'QrCode'
				                          when 2 then 'RFID Code' end as CODE_TYPE_DESCRIPTION
                         from IDE_BARCODE IB where IB.RECORD_ID = @ID order by  IB.RECORD_ID";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ID", id);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt.Rows[0];
    }
}