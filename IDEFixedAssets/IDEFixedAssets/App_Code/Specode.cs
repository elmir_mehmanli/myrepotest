﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{
    public DataTable GetSpecodeListByType(byte speType)
    {
        DataTable dt = new DataTable("IDE_SPECODE");
        string cmdText = @"select * from IDE_SPECODE where SPECODE_TYPE = @speType order by RECORD_ID";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@speType", speType);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetSpecodeTemplateList()
    {
        DataTable dt = new DataTable("IDE_SPECODE_TEMPLATE");
        string cmdText = @"select * from IDE_SPECODE_TEMPLATE order by RECORD_ID";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }



    public DataTable GetSpecodeList(string status)
    {
        DataTable dt = new DataTable("IDE_SPECODE");
        string cmdText = @"select s.* , (select t.DESCRIPTION from IDE_SPECODE_TEMPLATE t where t.RECORD_ID = s.SPECODE_TYPE) SPECODE_DESCRIPTION  from IDE_SPECODE s where status = @status";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@status", status);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataRow GetSpecodeById(int id)
    {
        DataTable dt = new DataTable("IDE_SPECODE");
        string cmdText = @"select s.* , (select t.DESCRIPTION from IDE_SPECODE_TEMPLATE t where t.RECORD_ID = s.SPECODE_TYPE) SPECODE_DESCRIPTION  from IDE_SPECODE s where RECORD_ID = @id";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@id", id);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt.Rows[0];
    }



    public string AddSpecode(string pSpeCode,
                     string pDescription,
                     string pSpecodeType)
    {
        SqlCommand cmd = new SqlCommand("spAddSpecode", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pSpecode", pSpeCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pSpecodeType", pSpecodeType);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public string UpdateSpecode(int pId,
                    string pSpeCode,
                    string pDescription,
                    string pSpecodeType)
    {
        SqlCommand cmd = new SqlCommand("spUpdateSpecode", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pId", pId);
        cmd.Parameters.AddWithValue("@pSpecode", pSpeCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pSpecodeType", pSpecodeType);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public string ChangeStatusSpecode(int pId,
                    string pStatus)
    {
        SqlCommand cmd = new SqlCommand("spChangeStatusSpecode", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pId", pId);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

}