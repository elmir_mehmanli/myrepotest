﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public partial class DbProcess : DALC
{
    public string AddWarehouse(string pCode,
                           string pDescription,
                           short  pCreatedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spAddWarehouse", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCode", pCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pCreated_Date", Config.HostingTime);
       
        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetWarehouse(string status)
    {
        DataTable dt = new DataTable("IDE_WAREHOUSE");
        string cmdText = @"select * from IDE_WAREHOUSE where STATUS = @status order by CREATED_DATE desc";
        
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@status", status);

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataRow GetWarehouseById(short warehouseId)
    {
        DataTable dt = new DataTable("IDE_WAREHOUSE_BY_ID");
        SqlDataAdapter da = new SqlDataAdapter("select * FROM IDE_WAREHOUSE where RECORD_ID = @id", SqlConn);
        try
        {
            da.SelectCommand.Parameters.AddWithValue("@id", warehouseId);
            da.Fill(dt);
        }
        catch
        {
            return null;
        }
        if (dt.Rows.Count == 0) return null;
        return dt.Rows[0];
    }


    public string UpdateWarehouse(short Id,
                           string pCode,
                           string pDescription,
                           short pModifiedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spUpdateWarehouse", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@pId", Id);
        cmd.Parameters.AddWithValue("@pCode", pCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public string ChangeStatusWarehouse(short pId,
                           string pStatus,
                           short pModifiedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spChangeStatusWarehouse", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@pId", pId);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);
        cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


}