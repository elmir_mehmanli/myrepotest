﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{
    
    public DataTable GetPriorityList()
    {
        DataTable dt = new DataTable("IDE_PRIORITY");
        string cmdText = @"select RECORD_ID from IDE_PRIORITY where record_id not in (select PRIORITY from IDE_ADDRESS)";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetAddressList()
    {
        DataTable dt = new DataTable("IDE_ADDRESS");
        string cmdText = @"select a.*,
               isnull((select max(a2.Title)  from IDE_ADDRESS a2 where a2.OWNER_RECID = a.RECORD_ID),'') as PARENT_TITLE,
	           isnull((select max(a2.RECORD_ID)  from IDE_ADDRESS a2 where a2.OWNER_RECID = a.RECORD_ID),'') as PARENT_ID
             from IDE_ADDRESS a  order by PRIORITY";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataRow GetAddressById(int addressID)
    {
        DataTable dt = new DataTable("IDE_ADDRESS_BY_ID");
        SqlDataAdapter da = new SqlDataAdapter(@"select a.*,
               isnull((select max(a2.Title)  from IDE_ADDRESS a2 where a2.OWNER_RECID = a.RECORD_ID),'') as PARENT_TITLE,
	           isnull((select max(a2.RECORD_ID)  from IDE_ADDRESS a2 where a2.OWNER_RECID = a.RECORD_ID),'') as PARENT_ID
             from IDE_ADDRESS a where a.RECORD_ID = @id order by PRIORITY", SqlConn);
        try
        {
            da.SelectCommand.Parameters.AddWithValue("@id", addressID);
            da.Fill(dt);
        }
        catch
        {
            return null;
        }
        if (dt.Rows.Count == 0) return null;
        return dt.Rows[0];
    }


    public string AddAddress(int pOwnerRecId,
                        string pTitle,
                        string pDescription,
                        int pPriority,
                        short pCreatedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spAddAddress", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pOwnerRecId", pOwnerRecId);
        cmd.Parameters.AddWithValue("@pTitle", pTitle);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pPriority", pPriority);
        cmd.Parameters.AddWithValue("@pCreated_Date", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public string UpdateAddress(int pId,
                                int pOwnerRecId,
                                string pTitle,
                                string pDescription,
                                int pPriority,
                                short pModifiedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spUpdateAddress", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pId", pId);
        cmd.Parameters.AddWithValue("@pOwnerRecId", pOwnerRecId);
        cmd.Parameters.AddWithValue("@pTitle", pTitle);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pPriority", pPriority);
        cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public string DeleteAddress(int pId,
                                short pModified_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spDeleteAddress", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pId", pId);
        cmd.Parameters.AddWithValue("@pModified_UserRecId", pModified_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime); 

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public DataTable GetAddressItemList(int addressId)
    {
        DataTable dt = new DataTable("IDE_ADDRESS_ITEMS");
        string cmdText = @"select a.*, isnull((select max(a2.DESCRIPTION) from IDE_ADDRESS_ITEMS a2 where a2.RECORD_ID = a.OWNER_RECID),'') as PARENT_DESCRIPTION
           from IDE_ADDRESS_ITEMS a  where a.ADDRESS_RECID = @address_recid order by Create_Date desc";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@address_recid", addressId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetAddressItemTopCategoryList(int addressId)
    {
        DataTable dt = new DataTable("IDE_ADDRESS_ITEMS");
        string cmdText = @"select * from IDE_ADDRESS_ITEMS where address_recid in (
           select record_id from IDE_ADDRESS where OWNER_RECID in (select RECORD_ID from IDE_ADDRESS I where record_id = @record_id))";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@record_id", addressId);

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataRow GetAddressItemsById(int addressID)
    {
        DataTable dt = new DataTable("IDE_ADDRESSITEMS_BY_ID");
        SqlDataAdapter da = new SqlDataAdapter(@"select a.*, isnull((select max(a2.DESCRIPTION) from IDE_ADDRESS_ITEMS a2 where a2.RECORD_ID = a.OWNER_RECID),'') as PARENT_DESCRIPTION
           from IDE_ADDRESS_ITEMS a  where a.RECORD_ID = @ID", SqlConn);
        try
        {
            da.SelectCommand.Parameters.AddWithValue("@ID", addressID);
            da.Fill(dt);
        }
        catch
        {
            return null;
        }
        if (dt.Rows.Count == 0) return null;
        return dt.Rows[0];
    }


    public string AddAddressItems(int pAddressRecID,
                                    int pOwnerRecId, 
                                    string pDescription,
                                    string  pCode,
                                    string pRFIDCode,
                                    string pBarCode,
                                    string pQRCode,
                                    short pCreatedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spAddAddressItems", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pAddressRecID", pAddressRecID);
        cmd.Parameters.AddWithValue("@pOwnerRecId", pOwnerRecId);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pCode", pCode == null ? (object)DBNull.Value : pCode);
        cmd.Parameters.AddWithValue("@pRFIDCode", pRFIDCode == null ? (object)DBNull.Value : pRFIDCode);
        cmd.Parameters.AddWithValue("@pBarCode", pBarCode == null ? (object)DBNull.Value : pBarCode);
        cmd.Parameters.AddWithValue("@pQRCode",  pQRCode == null ? (object)DBNull.Value : pQRCode);
        cmd.Parameters.AddWithValue("@pCreated_Date", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public string UpdateAddressItems(int pRecID,
                                     int pOwnerRecId,
                                   string pDescription,
                                   string pCode,
                                   string pRFIDCode,
                                   string pBarCode,
                                   string pQRCode,
                                   short pModifiedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spUpdateAddressItems", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pRecID", pRecID);
        cmd.Parameters.AddWithValue("@pOwnerRecId", pOwnerRecId);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pCode", pCode == null ? (object)DBNull.Value : pCode);
        cmd.Parameters.AddWithValue("@pRFIDCode", pRFIDCode == null ? (object)DBNull.Value : pRFIDCode);
        cmd.Parameters.AddWithValue("@pBarCode", pBarCode == null ? (object)DBNull.Value : pBarCode);
        cmd.Parameters.AddWithValue("@pQRCode", pQRCode == null ? (object)DBNull.Value : pQRCode);
        cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public string DeleteAddressItems(int pRecID,
                                     short pModifiedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spDeleteAddressItems", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pRecId", pRecID);
        cmd.Parameters.AddWithValue("@pModified_UserRecId", pModifiedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pModified_Date", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

}