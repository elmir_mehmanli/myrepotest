﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public partial class DbProcess : DALC
{
    public string AddMark(string pCode,
                       string pDescription,
                       string pStatus)
    {
        SqlCommand cmd = new SqlCommand("spAddMark", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCode", pCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetMarkList(string status)
    {
        DataTable dt = new DataTable("IDE_MARK");
        string cmdText = @"select * from IDE_MARK where status = @status";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@status", status);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataRow GetMarkById(int id)
    {
        DataTable dt = new DataTable("IDE_MARK");
        string cmdText = @"select * from IDE_MARK where RECORD_ID = @id";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@id", id);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt.Rows[0];
    }


    public string UpdateMark(int pId, string pCode,
                      string pDescription,
                      string pStatus)
    {
        SqlCommand cmd = new SqlCommand("spUpdateMark", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pId", pId);
        cmd.Parameters.AddWithValue("@pCode", pCode);
        cmd.Parameters.AddWithValue("@pDescription", pDescription);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public string ChangeStatusMark(int pId, 
                     string pStatus)
    {
        SqlCommand cmd = new SqlCommand("spChangeStatusMark", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pId", pId);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }
}