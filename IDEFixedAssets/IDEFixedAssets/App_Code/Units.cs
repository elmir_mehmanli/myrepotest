﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public partial class DbProcess : DALC
{
    public DataTable GetUnitTeplate()
    {
        DataTable dt = new DataTable("V_IDE_UNIT");
        string cmdText = @"select * from V_IDE_UNIT order by RECORD_ID";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);      
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public bool isMainUnit(int item_record_id)
    {
        string cmdText = @"select count(1) as itemcount from IDE_ITEMUNIT where ITEM_RECID = @ITEM_RECID";
        SqlCommand cmd = new SqlCommand(cmdText, SqlConn);
        cmd.Parameters.AddWithValue("@ITEM_RECID", item_record_id);
        cmd.Connection.Open();
        int value = -1;
        try
        {
            value = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }
        catch
        {
            value = -2;
            throw new Exception();
        }
        finally
        {
            cmd.Connection.Close();
        }
        return (value == 0);
    }


    public string AddItemUnit(int pItemRecordId,
                           int pUnitRecordId,
                           float pCoefficient,
                           bool pIsMainUnit)
    {
        SqlCommand cmd = new SqlCommand("spAddUnit", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pItemRecordId", pItemRecordId);
        cmd.Parameters.AddWithValue("@pUnitRecordId", pUnitRecordId);
        cmd.Parameters.AddWithValue("@pCoefficient", pCoefficient);
        cmd.Parameters.AddWithValue("@pIsMainUnit", pIsMainUnit);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch(Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public DataTable GetUnitByItemId(int item_record_id)
    {
        DataTable dt = new DataTable("IDE_ITEMUNIT");

        string cmdText = @"select t1.*, t2.CODE as UNIT_CODE,
            (select  t_2.CODE  from IDE_ITEMUNIT t_1 inner join IDE_UNIT t_2 on t_1.UNIT_RECID = t_2.RECORD_ID where t_1.ITEM_RECID = t1.ITEM_RECID and IS_MAIN_UNIT = 1) as MAIN_UNIT_CODE,
            (select  t_1.COEFFICIENT  from IDE_ITEMUNIT t_1 inner join IDE_UNIT t_2 on t_1.UNIT_RECID = t_2.RECORD_ID where t_1.ITEM_RECID = t1.ITEM_RECID and IS_MAIN_UNIT = 1) as MAIN_UNIT_COEFFICINET
             from IDE_ITEMUNIT t1 inner join IDE_UNIT t2 on t1.UNIT_RECID = t2.RECORD_ID where t1.ITEM_RECID = @ITEM_RECID order by t1.RECORD_ID";

       /* string cmdText = @"select II.*,
        (select IU.CODE from IDE_UNIT IU where IU.RECORD_ID = II.UNIT_RECID) as UNIT_CODE
         from IDE_ITEMUNIT II where II.ITEM_RECID = @ITEM_RECID";*/

        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ITEM_RECID", item_record_id);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public string DeleteItemUnit(int pRecorId,
                           bool pIsMainUnit)
    {
        SqlCommand cmd = new SqlCommand("spDeleteItemUnit", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pRecorId", pRecorId);
        cmd.Parameters.AddWithValue("@pIsMainUnit", pIsMainUnit);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public DataRow getMainUnitCode(int item_recid)
    {
        string cmdText = @"select II.COEFFICIENT,  (select IU.CODE from IDE_UNIT IU where IU.RECORD_ID = II.UNIT_RECID) as UNIT_CODE     from IDE_ITEMUNIT II where II.ITEM_RECID = @item_recid and II.IS_MAIN_UNIT = 1";
        SqlDataAdapter da = new SqlDataAdapter(cmdText, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@item_recid", item_recid);
        DataTable dt = new DataTable("IDE_ITEMUNIT");
       
        try
        {
            da.Fill(dt);
        }
        catch
        {
            return null;
        }
       
        return dt.Rows[0];
    }

    public string UpdateItemUnitCoefficient(int pRecorId,
                                            decimal pCoefficient)
    {
        SqlCommand cmd = new SqlCommand("spUpdateUnitCoefficient", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pId", pRecorId);
        cmd.Parameters.AddWithValue("@pCoefficient", pCoefficient);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

}