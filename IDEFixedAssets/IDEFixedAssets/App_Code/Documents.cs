﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public partial class DbProcess : DALC
{
    public int GetDocumentId(string pDocNum,
                             DateTime pDocDate,
                             short pCreatedBy_UserRecId,out int pDocnumOut)
    {
        SqlCommand cmd = new SqlCommand("spAddDocument", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pDocNum", pDocNum);
        cmd.Parameters.AddWithValue("@pDocDate", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);
        cmd.Parameters.AddWithValue("@pCreated_Date", Config.HostingTime);


        SqlParameter paramResult1 = new SqlParameter("@pResult", SqlDbType.Int);
        paramResult1.Direction = ParameterDirection.Output;
        cmd.Parameters.Add(paramResult1);

        SqlParameter paramResult2 = new SqlParameter("@pDocNumOut", SqlDbType.Int);
        paramResult2.Direction = ParameterDirection.Output;
        cmd.Parameters.Add(paramResult2);

        int pResult = 0;
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToInt32(cmd.Parameters["@pResult"].Value);
            pDocnumOut = Convert.ToInt32(cmd.Parameters["@pDocNumOut"].Value);
        }
        catch
        {
            pResult = -2;
            pDocnumOut = -1;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public void DeletePendingDocument(short pCreatedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spDeletePendingDocument", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";

        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = "ERR";
        }
        finally
        {
            cmd.Connection.Close();
        }
    }



    public string DeleteItemLine(int itemLineId)
    {
        SqlCommand cmd = new SqlCommand("spDeleteItemLine", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pItemLineId", itemLineId);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";

        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = "ERR";
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetItemUnitById(int id)
    {
        SqlDataAdapter da = new SqlDataAdapter(@"select t1.RECORD_ID , (select t2.CODE from IDE_UNIT t2 where t2.RECORD_ID = t1.UNIT_RECID) as UNIT_NAME from IDE_ITEMUNIT t1  where t1.ITEM_RECID = @id  order by t1.IS_MAIN_UNIT desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("id", id);
        DataTable dt = new DataTable("Unit");
        da.Fill(dt);
        return dt;
    }

    Config _configFile = new Config();
    public decimal GetUnitCoeficient(int unit_recId)
    {
        string cmdText = @"select COEFFICIENT from IDE_ITEMUNIT where RECORD_ID =@id";
        SqlCommand cmd = new SqlCommand(cmdText, SqlConn);
        cmd.Parameters.AddWithValue("@id", unit_recId);
        cmd.Connection.Open();
        decimal value = -1;
        try
        {
            value = _configFile.ConvertToDecimal(cmd.ExecuteScalar().ToString());
        }
        catch
        {
            value = -2;
            throw new Exception();
        }
        finally
        {
            cmd.Connection.Close();
        }
        return value;
    }



    public string AddItemLine(int pItemDocumentRecId,
                            byte pDocumentType,
                            int pItemRecId,
                            short pWarehouseRecId,
                            byte pInOut,
                            decimal pQuantity,
                            decimal pPrice,
                            decimal pAmount,
                            string pNote,
                            int pItemUnitRecId,
                            short pCreatedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spAddItemLine", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pItemDocumentRecId", pItemDocumentRecId);
        cmd.Parameters.AddWithValue("@pDocumentType", pDocumentType);
        cmd.Parameters.AddWithValue("@pItemRecId", pItemRecId);
        cmd.Parameters.AddWithValue("@pWarehouseRecId", pWarehouseRecId);
        cmd.Parameters.AddWithValue("@pInOut", pInOut);
        cmd.Parameters.AddWithValue("@pQuantity", pQuantity);
        cmd.Parameters.AddWithValue("@pPrice", pPrice);
        cmd.Parameters.AddWithValue("@pAmount", pAmount);
        cmd.Parameters.AddWithValue("@pNote", pNote);
        cmd.Parameters.AddWithValue("@pItemUnitRecId", pItemUnitRecId);
        cmd.Parameters.AddWithValue("@pCreated_Date", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";

        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = "ERR";
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public string UpdateItemDocument(int pDocumentRecId, string pDocumentNumber, byte pDocumentType, DateTime pDocumentDate, string pNote, short pWarehouseRecId, DateTime? pModifiedBy_Date, short? pModifiedBy_UserRecId)
    {
        SqlCommand cmd = new SqlCommand("spUpdateItemDocument", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pDocumentRecId", pDocumentRecId);
        cmd.Parameters.AddWithValue("@pDocumentNumber", pDocumentNumber);
        cmd.Parameters.AddWithValue("@pDocumentType", pDocumentType);
        cmd.Parameters.AddWithValue("@pDocumentDate", pDocumentDate);
        cmd.Parameters.AddWithValue("@pNote", pNote);
        cmd.Parameters.AddWithValue("@pWarehouseRecId", pWarehouseRecId);
        cmd.Parameters.AddWithValue("@pModifiedBy_Date", pModifiedBy_Date == null ? (object)DBNull.Value : pModifiedBy_Date);
        cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId == null ? (object)DBNull.Value : pModifiedBy_UserRecId);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";

        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = "ERR";
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetItemLineByDocId(int documentId)
    {
        SqlDataAdapter da = new SqlDataAdapter(@"select 
            t2.CODE as ITEM_CODE,
            t2.DESCRIPTION as ITEM_NAME,
            t4.CODE as UNIT_NAME,
            t1.* from IDE_ITEMLINE t1 inner join IDE_ITEM  t2 on t1.ITEM_RECID = t2.RECORD_ID
                                      inner join IDE_ITEMUNIT t3 on t1.ITEMUNIT_RECID = t3.RECORD_ID
						              inner join IDE_UNIT t4 on t3.UNIT_RECID = t4.RECORD_ID
						              where t1.ITEMDOCUMENT_RECID = @documentId", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@documentId", documentId);
        DataTable dt = new DataTable("item_line");
        da.Fill(dt);
        return dt;
    }

    public DataTable GetDocumentList(int PageIndex, int PageSize)
    {
        SqlDataAdapter da = new SqlDataAdapter(@"select tab1.* from (select 
        row_number() OVER (ORDER BY t.RECORD_ID DESC) n, 
        Convert(varchar,t.DOCUMENT_DATE,104) DOCUMENT_DATE1,
        (select (t_1.CODE + N' / ' + t_1.DESCRIPTION)  from IDE_WAREHOUSE t_1 where t_1.RECORD_ID = t.WAREHOUSE_RECID) as WAREHOUSE,
        t.* from IDE_ITEMDOCUMENT t where t.STATUS = 'ACTIVE') tab1
        where n between ((@PageIndex * @PageSize) + 1) and  ((@PageIndex + 1) * (@PageSize))", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@PageIndex", PageIndex);
        da.SelectCommand.Parameters.AddWithValue("@PageSize", PageSize);
        DataTable dt = new DataTable("Documents");
        da.Fill(dt);
        return dt;
    }


    public int GetDocumentCount()
    {
        string cmdText = @"select count(1) from IDE_ITEMDOCUMENT t where t.STATUS = 'ACTIVE'";
        SqlCommand cmd = new SqlCommand(cmdText, SqlConn);
        cmd.Connection.Open();
        int value = -1;
        try
        {
            value = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }
        catch
        {
            value = -2;
            throw new Exception();
        }
        finally
        {
            cmd.Connection.Close();
        }
        return value;
    }


    public DataRow GetDocumentByRecordId(int documentRecordId)
    {
        SqlDataAdapter da = new SqlDataAdapter(@"select 
        (select I.NAME + ' ' + I.SURNAME  from IDE_USERS I where I.RECORD_ID = t.CREATEDBY_USERRECID) as CREATEDBY_USER,
        (select I.NAME + ' ' + I.SURNAME  from IDE_USERS I where I.RECORD_ID = t.MODIFIEDBY_USERRECID) as MODIFIEDBY_USER,
        t.* from IDE_ITEMDOCUMENT t where t.RECORD_ID = @RECORD_ID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@RECORD_ID", documentRecordId);
        DataTable dt = new DataTable("Documents");
        da.Fill(dt);
        try
        {
            return dt.Rows[0];
        }
        catch
        {
            return null;
        }
    }



    public string DeleteDocument(int pRecorId)
    {
        SqlCommand cmd = new SqlCommand("spDeleteDocument", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pRecorId", pRecorId);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";

        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch
        {
            pResult = "ERR";
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

}