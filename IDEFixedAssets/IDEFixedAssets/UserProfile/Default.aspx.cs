﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserProfile_Default : System.Web.UI.Page
{
    UserData u_data = new UserData();
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
      if (Session["UserData"] == null) Config.Rd("/exit");
      u_data = (UserData)Session["UserData"];

      /*Page Labes Begin*/
      FillPageData(u_data);
      /*Page Labes End*/
    }

    void FillPageData(UserData u_data)
    {
        ltrPageHeaderUserProfileLabel_n.Text = ltrPageHeaderUserProfileLabel.Text = Config.getXmlValue(u_data.lang, "pageHeaderUserProfileLabel");
        txtUserPassword.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "loginPasswordLabel"));
        txtUserPasswordRepeat.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "loginPasswordLabelRepeat"));
        btnChangePassword.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");       
    }
    protected void btnChangePassword_Click(object sender, EventArgs e)
    {

        if (txtUserPassword.Text.Trim().Length < 6 || !Config.isEnglish(txtUserPassword.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changePasswordErrorMessage1"));
            return;
        }

        if (txtUserPassword.Text != txtUserPasswordRepeat.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changePasswordErrorMessage2"));
            return;
        }


        string result = db.ChangePassword((short)u_data.LogonUserID, txtUserPassword.Text.Trim(),(short)u_data.LogonUserID);
        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "changePasswordSuccessMessage"));
        }
    }
}