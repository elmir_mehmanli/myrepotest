﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="UserProfile_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <script src="../lib/jquery/jquery.js"></script>
     <script>
         $(document).ready(function () {
             $("#txtUserPasswordRepeatShow a").on('click', function (event) {
                 event.preventDefault();
                 if ($('#txtUserPasswordRepeatShow input').attr("type") == "text") {
                     $('#txtUserPasswordRepeatShow input').attr('type', 'password');
                     $('#txtUserPasswordRepeatShow i').addClass("fa-eye-slash");
                     $('#txtUserPasswordRepeatShow i').removeClass("fa-eye");
                 } else if ($('#txtUserPasswordRepeatShow input').attr("type") == "password") {
                     $('#txtUserPasswordRepeatShow input').attr('type', 'text');
                     $('#txtUserPasswordRepeatShow i').removeClass("fa-eye-slash");
                     $('#txtUserPasswordRepeatShow i').addClass("fa-eye");
                 }
             });
         });

         $(document).ready(function () {
             $("#txtUserPasswordShow a").on('click', function (event) {
                 event.preventDefault();
                 if ($('#txtUserPasswordShow input').attr("type") == "text") {
                     $('#txtUserPasswordShow input').attr('type', 'password');
                     $('#txtUserPasswordShow i').addClass("fa-eye-slash");
                     $('#txtUserPasswordShow i').removeClass("fa-eye");
                 } else if ($('#txtUserPasswordShow input').attr("type") == "password") {
                     $('#txtUserPasswordShow input').attr('type', 'text');
                     $('#txtUserPasswordShow i').removeClass("fa-eye-slash");
                     $('#txtUserPasswordShow i').addClass("fa-eye");
                 }
             });
         });

	   </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">IDE Fixed Assets</a>
            <span class="breadcrumb-item active">
                <asp:Literal ID="ltrPageHeaderUserProfileLabel" runat="server"></asp:Literal>
            </span>
        </nav>

        <div class="sl-pagebody">
            <!-- SMALL MODAL -->
            <div id="alertmodal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                        <div class="modal-footer justify-content-right" style="padding: 5px">
                            <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
                <!-- modal-dialog -->
            </div>
            <!-- modal -->

            <div class="card">
                <h5 class="card-header">
                    <asp:Literal ID="ltrPageHeaderUserProfileLabel_n" runat="server"></asp:Literal>

                </h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 input-group" id ="txtUserPasswordShow">
                            <asp:TextBox ID="txtUserPassword" runat="server" MaxLength="50" class="form-control" placeholder="Şifrə" TextMode="Password"></asp:TextBox>
                            <div class="input-group-addon">
                              <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                             </div>
                        </div>
                    </div>


                    <br />
                    <div class="row">
                        <div class="col-lg-6 input-group" id="txtUserPasswordRepeatShow">
                            <asp:TextBox ID="txtUserPasswordRepeat" runat="server" MaxLength="50" class="form-control" placeholder="Şifrə təkrar" TextMode="Password"></asp:TextBox>
                            <div class="input-group-addon">
                              <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                             </div>
                        </div>

                         <!--<div class="col-lg-6 input-group" id="show_hide_password">
                          <input class="form-control" type="password"/>
                          <div class="input-group-addon">
                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                          </div>
                         </div>-->

                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg">
                            <img id="add_loading" style="display: none" src="../img/loader.gif" />
                            <asp:Button ID="btnChangePassword" class="btn btn-secondary" runat="server" OnClick ="btnChangePassword_Click"
                                OnClientClick="this.style.display = 'none';
                                        document.getElementById('add_loading').style.display = '';" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .card {
                border: 1px solid #dadada;
            }
        </style>

        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }
        </script>


       

    </div>
</asp:Content>

