﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IDE Fixed Assets-Login</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <!-- vendor css -->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet" />
    <!-- Starlight CSS -->
    <link rel="stylesheet" href="css/starlight.css" />
    <link rel="icon" href="img/ide.ico"/>
</head>
<body>
    <form id="form1" runat="server">
        <!-- SMALL MODAL -->
        <div id="alertmodal" class="modal fade" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content bd-0 tx-14">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"></h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                    <div class="modal-footer justify-content-right" style="padding: 5px">
                        <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
            <!-- modal-dialog -->
        </div>
       
        
         <!-- modal -->
        <div class="d-flex align-items-center justify-content-center bg-sl-primary ht-100v">
            <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-20 bg-white">
                <div class="signin-logo tx-center tx-24 tx-bold tx-inverse">
                    IDE 
                    <span class="tx-info tx-normal">Soft</span>
                </div>
                <div class="tx-center mg-b-40">IDE Fixed Assets</div>
                <div class="tx-center mg-b-40"></div>
                <div class="form-group">
                    <asp:TextBox ID="txtLogin" class="form-control" 
                         MaxLength="50" runat="server"></asp:TextBox>
                </div>
                <!-- form-group -->
                <div class="form-group input-group" style ="margin-bottom:0" id ="txtPasswordShow">
                    <asp:TextBox ID="txtPassword" class="form-control" TextMode="Password" MaxLength="50" placeholder="Şifrə" runat="server"></asp:TextBox>
                     <div class="input-group-addon">
                         <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                      </div>
                </div>
                 <asp:LinkButton ID="lnkForgotPass" style ="margin-top:0" runat="server" class="tx-info tx-12 d-block mg-t-10" OnClick ="lnkForgotPass_Click">
                       <asp:Literal ID="ltrForgotPass" runat="server"></asp:Literal>
                 </asp:LinkButton><br />

                 <!-- form-group -->
                <div class="form-group">
                    <asp:DropDownList ID="drlLang" class="form-control"  runat="server" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="az">Azerbaijan</asp:ListItem>
                        <asp:ListItem Value="ru">Russian</asp:ListItem>
                        <asp:ListItem Value="en">English</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <!-- form-group -->
                <div style="text-align: right">
                    <img id="add_loading" style="display: none" src="../img/loader.gif" />
                    <asp:Button ID="btnLogin" class="btn btn-info btn-block" runat="server" Text="Giriş"
                      OnClick ="btnLogin_Click"  OnClientClick="this.style.display = 'none';
                                        document.getElementById('add_loading').style.display = '';
                                        document.getElementById('alert_msg').style.display = 'none';" />
                </div>
            </div>
            <!-- login-wrapper -->
        </div>
        <!-- d-flex -->


        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }
        </script>

        <script src="../lib/jquery/jquery.js"></script>
        <script src="../lib/popper.js/popper.js"></script>
        <script src="../lib/bootstrap/bootstrap.js"></script>

         <script>
             $(document).ready(function () {
                 $("#txtPasswordShow a").on('click', function (event) {
                     event.preventDefault();
                     if ($('#txtPasswordShow input').attr("type") == "text") {
                         $('#txtPasswordShow input').attr('type', 'password');
                         $('#txtPasswordShow i').addClass("fa-eye-slash");
                         $('#txtPasswordShow i').removeClass("fa-eye");
                     } else if ($('#txtPasswordShow input').attr("type") == "password") {
                         $('#txtPasswordShow input').attr('type', 'text');
                         $('#txtPasswordShow i').removeClass("fa-eye-slash");
                         $('#txtPasswordShow i').addClass("fa-eye");
                     }
                 });
             });
	</script>


    </form>
</body>
</html>
