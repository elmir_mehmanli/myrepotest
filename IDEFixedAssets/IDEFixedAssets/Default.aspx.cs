﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["lang"] == null)
        {
            Session["lang"] = "az";
        }
        else
        {
            // when language file is complated
           // Session["lang"] = drlLang.SelectedValue;
            Session["lang"] = "az";
        }
        FillPageData();
    }

    void FillPageData()
    {
        txtLogin.Attributes.Add("placeholder", Config.getXmlValue(Convert.ToString(Session["lang"]), "loginNameLabel"));
        txtPassword.Attributes.Add("placeholder", Config.getXmlValue(Convert.ToString(Session["lang"]), "loginPasswordLabel"));
        ltrForgotPass.Text = Config.getXmlValue(Convert.ToString(Session["lang"]), "forgotPassword");
        btnLogin.Text = Config.getXmlValue(Convert.ToString(Session["lang"]), "btnLogin");
    }

    
    protected void lnkForgotPass_Click(object sender, EventArgs e)
    {
        ltrAlertMsg.Text = Alert.WarningMessage(Config.getXmlValue(Convert.ToString(Session["lang"]), "forgotPasswordInfo"));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        return;
    }

    DbProcess db = new DbProcess();

    protected void btnLogin_Click(object sender, EventArgs e)
    {

      /*  string ll = Config.Sha1("elmir123");
        ltrAlertMsg.Text = Alert.DangerMessage(ll);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        return;*/

        UserData u_data = new UserData();

        if (txtLogin.Text.Length < 3)
        {
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(Convert.ToString(Session["lang"]), "loginNameValidation"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            txtLogin.Focus();
            return;
        }

        if (txtPassword.Text.Length < 5)
        {
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(Convert.ToString(Session["lang"]), "passwordValidation"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            txtPassword.Focus();
            return;
        }

        string pUserInfo = "";
        string login_result = db.dbLogin(txtLogin.Text, txtPassword.Text, ref pUserInfo);

        if (login_result != "OK")
        {
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(Convert.ToString(Session["lang"]), "loginIsError"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        string[] uInfo = pUserInfo.Split('#');
        try
        {
            if (uInfo.Length != 4)
            {
                ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(Convert.ToString(Session["lang"]), "anyErrorString"));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                return;
            }
            u_data.LogonUserID = Convert.ToInt32(uInfo[0].Trim());
            u_data.LoginUserName = Convert.ToString(uInfo[1].Trim());
            u_data.LoginUserImage = Convert.ToString(uInfo[2].Trim());
            u_data.LoginUserType = Convert.ToString(uInfo[3].Trim());
            u_data.lang =Convert.ToString(Session["lang"]);

            Session["UserData"] = u_data;
        }
        catch
        {
            Session.Clear();
            Session.Abandon();
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(Convert.ToString(Session["lang"]), "anyErrorString"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        Config.Rd("/index");

    }
}