﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Address_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .dropdown-menu {
            min-width: 120%;
            margin: 0.125rem 0 0;
            font-size: 0.875rem;
            list-style: none;
            transform: translate3d(0px, 0px, 0px)!important;
        }

        .list-group-item {
            position: relative;
            display: block;
            padding: 5px 10px;
            background-color: #fff;
            border: 0px;
        }


            .list-group-item :hover {
                background-color: #dadada;
                padding: 4px 7px;
            }
    </style>


    <!-- Starlight CSS -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">IDE Fixed Assets</a>
            <span class="breadcrumb-item active">
                <asp:Literal ID="ltrPageHeaderAddressLabel" runat="server"></asp:Literal>
            </span>
        </nav>

        <div class="sl-pagebody">

            <!-- SMALL MODAL -->
            <div id="alertmodal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                        <div class="modal-footer justify-content-right" style="padding: 5px">
                            <button type="button" class="btn btn-info pd-x-20"  data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
                <!-- modal-dialog -->
            </div>
            <!-- modal -->



            <div class="pd-10 bd" style="background-color: #d8dce3;">
                <ul class="nav nav-pills flex-column flex-md-row" role="tablist">
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkAddress" runat="server" class="nav-link active" role="tab" OnClick="lnkAddress_Click"></asp:LinkButton>
                    </li>
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkAddressItems" runat="server" class="nav-link" role="tab" OnClick="lnkAddressItems_Click"></asp:LinkButton>
                    </li>
                </ul>
            </div>
            <div class="card pd-20 pd-sm-15">
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">

                        <!-- Begin New Address modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="addNewAddressModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                    &nbsp&nbsp
                                                <asp:Literal ID="ltrAddNewAddressModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrAddNewAddressMessage" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtAddNewAddressTitle" Width="300px" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtAddNewAddressDescription" Width="300px" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlAddNewAddressPriority"
                                                                class="selectpicker form-control"
                                                                data-live-search="true" data-hide-disabled="true"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlAddNewAddressList"
                                                                class="selectpicker form-control"
                                                                data-live-search="true" data-hide-disabled="true"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="addNewAddress_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnAddNewAddress" class="btn btn-info pd-x-20" runat="server"
                                                    OnClick="btnAddNewAddress_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                       document.getElementById('addNewAddress_loading').style.display = '';
                                                       document.getElementById('ContentPlaceHolder1_btnAddNewAddressCancel').style.display = 'none';
                                                       document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnAddNewAddressCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnAddNewAddress" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End New Address modal-dialog -->



                        <!-- Begin Edit Address modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="editAddressModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                    &nbsp&nbsp
                                                 <asp:Literal ID="ltrEditAddressModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrEditAddressMessage" runat="server"></asp:Literal>
                                                <asp:HiddenField ID="hdnFieldEditAddressID" runat="server" />
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtEditAddressTitle" Width="300px" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtEditAddressDescription" Width="300px" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlEditAddressPriority"
                                                                class="selectpicker form-control"
                                                                data-live-search="true" data-hide-disabled="true"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlEditAddressList"
                                                                class="selectpicker form-control"
                                                                data-live-search="true" data-hide-disabled="true"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="editAddress_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnEditAddress" class="btn btn-info pd-x-20" runat="server"
                                                    OnClick="btnEditAddress_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                       document.getElementById('editAddress_loading').style.display = '';
                                                       document.getElementById('ContentPlaceHolder1_btnEditAddressCancel').style.display = 'none';
                                                       document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnEditAddressCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnEditAddress" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End Edit Address modal-dialog -->



                        <!-- Begin Delete Address modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="deleteAddressModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-trash-a"></i>
                                                    <asp:Literal ID="ltrDeleteAddressModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    &nbsp&nbsp
                                            <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <asp:Literal ID="ltrDeleteAddressMessage" runat="server"></asp:Literal>
                                            <asp:HiddenField ID="hdnFieldDeleteAddressID" runat="server" />
                                            <div class="card pd-20 pd-sm-30">
                                                <div class="row mg-t-20">
                                                    <h6>
                                                        <asp:Literal ID="ltrDeleteAddressModalBody" runat="server"></asp:Literal></h6>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="deleteAddress_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnAddressDelete" class="btn btn-info pd-x-20" runat="server"
                                                    OnClick="btnAddressDelete_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                       document.getElementById('deleteAddress_loading').style.display = '';
                                                       document.getElementById('ContentPlaceHolder1_btnAddressDeleteCancel').style.display = 'none';
                                                       document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnAddressDeleteCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnAddressDelete" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End Delete Address modal-dialog -->




                        <div class="row">
                            <div class="col-lg-2">
                                <asp:LinkButton ID="lnkNewAddressModal" runat="server" class="btn btn-outline-info btn-block" OnClick="lnkNewAddressModal_Click">
                                    <i class="icon ion-plus-circled"></i>
                                    <asp:Literal ID="ltrNewAddressLabel" runat="server"></asp:Literal>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="table-responsive">
                            <asp:GridView class="table table-hover" ID="grdAddress" runat="server"
                                AutoGenerateColumns="False" GridLines="None" OnRowDataBound="grdAddress_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="TITLE" />
                                    <asp:BoundField DataField="DESCRIPTION" />
                                    <asp:BoundField DataField="PRIORITY" />
                                    <asp:BoundField DataField="PARENT_TITLE" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="dropdown">
                                                <asp:LinkButton ID="lnkActionAddressData"
                                                    runat="server"
                                                    class="btn btn-link" data-toggle="dropdown">
                                                <i class="icon ion-more"></i></asp:LinkButton>
                                                <ul class="dropdown-menu">
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkAddressDataEdit" OnClick="lnkAddressDataEdit_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkAddressDelete" OnClick="lnkAddressDelete_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                                    </li>
                                                </ul>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="bg-info" />
                            </asp:GridView>
                        </div>
                    </asp:View>
                    <asp:View ID="View2" runat="server">


                        <!-- Begin New AddressItems modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="addNewAddressItemsModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                    &nbsp&nbsp
                                                <asp:Literal ID="ltrAddNewAddressItemsModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrAddNewAddressItemsMessage" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            <label class="ckbox">
                                                                <asp:CheckBox ID="chkaAddNewAddressItemsDescriptionSelect"
                                                                    AutoPostBack="true" OnCheckedChanged="chkaAddNewAddressItemsDescriptionSelect_CheckedChanged"
                                                                    runat="server" />
                                                                <span>
                                                                    <asp:Literal ID="ltrCheckBoxWarehouse" runat="server"></asp:Literal>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            <div id="divAddNewAddressItemsDescription" runat="server">
                                                                <asp:TextBox ID="txtAddNewAddressItemsDescription" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                            </div>
                                                            <div id="divWarehouseListAdd" runat="server">
                                                                <asp:DropDownList ID="drlWarehouseListAdd"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true"
                                                                    runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtAddNewAddressItemsRfIdCode" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtAddNewAddressItemsBarCode" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtAddNewAddressItemsQRCode" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlAddNewAddressItemsList"
                                                                class="selectpicker form-control"
                                                                data-live-search="true" data-hide-disabled="true"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="addNewAddressItems_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnAddNewAddressItems" class="btn btn-info pd-x-20" runat="server"
                                                    OnClick="btnAddNewAddressItems_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                       document.getElementById('addNewAddressItems_loading').style.display = '';
                                                       document.getElementById('ContentPlaceHolder1_btnAddNewAddressItemsCancel').style.display = 'none';
                                                       document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnAddNewAddressItemsCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnAddNewAddressItems" />
                                <asp:PostBackTrigger ControlID="chkaAddNewAddressItemsDescriptionSelect" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End New AddressItems modal-dialog -->



                        <!-- Begin Edit AddressItems modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="editNewAddressItemsModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                    &nbsp&nbsp
                                                <asp:Literal ID="ltrEditAddressItemsModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrEditAddressItemsMessage" runat="server"></asp:Literal>
                                                <asp:HiddenField ID="hdnFieldEditAddressItemsID" runat="server" />
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            <label class="ckbox">
                                                                <asp:CheckBox ID="chkaEditAddressItemsDescriptionSelect"
                                                                    AutoPostBack="true" OnCheckedChanged="chkaEditAddressItemsDescriptionSelect_CheckedChanged"
                                                                    runat="server" />
                                                                <span>
                                                                    <asp:Literal ID="ltrEditCheckBoxWarehouse" runat="server"></asp:Literal>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            <div id="divEditAddressItemsDescription" runat="server">
                                                                <asp:TextBox ID="txtEditAddressItemsDescription" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                            </div>
                                                            <div id="divWarehouseListEdit" runat="server">
                                                                <asp:DropDownList ID="drlWarehouseListEdit"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true"
                                                                    runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtEditAddressItemsRfIdCode" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtEditAddressItemsBarCode" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtEditAddressItemsQRCode" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlEditAddressItemsList"
                                                                class="selectpicker form-control"
                                                                data-live-search="true" data-hide-disabled="true"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="editAddressItems_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnEditAddressItems" class="btn btn-info pd-x-20" runat="server"
                                                    OnClick="btnEditAddressItems_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                       document.getElementById('editAddressItems_loading').style.display = '';
                                                       document.getElementById('ContentPlaceHolder1_btnEditAddressItemsCancel').style.display = 'none';
                                                       document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnEditAddressItemsCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnEditAddressItems" />
                                <asp:PostBackTrigger ControlID="chkaEditAddressItemsDescriptionSelect" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End Edit AddressItems modal-dialog -->


                        <!-- Begin Delete AddressItems modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="deleteAddressItemsModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-trash-a"></i>
                                                    <asp:Literal ID="ltrDeleteAddressItemsModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    &nbsp&nbsp
                                                   <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <asp:Literal ID="ltrDeleteAddressItemsMessage" runat="server"></asp:Literal>
                                            <asp:HiddenField ID="hdnFieldDeleteAddressItemsID" runat="server" />
                                            <div class="card pd-20 pd-sm-30">
                                                <div class="row mg-t-20">
                                                    <h6>
                                                        <asp:Literal ID="ltrDeleteAddressItemsModalBody" runat="server"></asp:Literal></h6>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="deleteAddressItems_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnAddressItemsDelete" class="btn btn-info pd-x-20" runat="server"
                                                    OnClick ="btnAddressItemsDelete_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                       document.getElementById('deleteAddressItems_loading').style.display = '';
                                                       document.getElementById('ContentPlaceHolder1_btnAddressItemsDeleteCancel').style.display = 'none';
                                                       document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnAddressItemsDeleteCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnAddressItemsDelete" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End Delete AddressItems modal-dialog -->


                        <div class="row">
                            <div class="col-lg-10">
                                <asp:DropDownList ID="drlAddressListForItems" AutoPostBack="true" OnSelectedIndexChanged="drlAddressListForItems_SelectedIndexChanged"
                                    class="selectpicker form-control"
                                    data-live-search="true" data-hide-disabled="true"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-2" id="pnlNewAddressItemBox" runat="server">
                                <asp:LinkButton ID="lnkNewAddressItemsModal" runat="server" class="btn btn-outline-info btn-block" OnClick="lnkNewAddressItemsModal_Click">
                                    <i class="icon ion-plus-circled"></i>
                                    <asp:Literal ID="ltrNewAddressItemsLabel1" runat="server"></asp:Literal>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <br />
                        <br />

                        <!--grdAddressItems-->
                        <div class="table-responsive">
                            <asp:GridView class="table table-hover" ID="grdAddressItems" runat="server" OnRowDataBound="grdAddressItems_RowDataBound"
                                AutoGenerateColumns="False" GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="DESCRIPTION" />
                                    <asp:BoundField DataField="CODE" />
                                    <asp:BoundField DataField="RFIDCODE" />
                                    <asp:BoundField DataField="BARCODE" />
                                    <asp:BoundField DataField="QRCODE" />
                                    <asp:BoundField DataField="PARENT_DESCRIPTION" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="dropdown">
                                                <asp:LinkButton ID="lnkActionAddressItemsData"
                                                    runat="server"
                                                    class="btn btn-link" data-toggle="dropdown">
                                                    <i class="icon ion-more"></i></asp:LinkButton>
                                                <ul class="dropdown-menu">
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkAddressItemsDataEdit" OnClick="lnkAddressItemsDataEdit_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkAddressItemsDelete" OnClick ="lnkAddressItemsDelete_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                                    </li>
                                                </ul>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="bg-info" />
                            </asp:GridView>
                        </div>

                    </asp:View>
                </asp:MultiView>
            </div>
        </div>





        <style>
            .card {
                border: 1px solid #dadada;
            }
        </style>

        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openAddNewAddressModal() {
                $('#addNewAddressModal').modal({ show: true });
            }

            function openEditAddressModal() {
                $('#editAddressModal').modal({ show: true });
            }

            function openDeleteAddressModal() {
                $('#deleteAddressModal').modal({ show: true });
            }

            function openAddNewAddressItemsModal() {
                $('#addNewAddressItemsModal').modal({ show: true });
            }

            function openEditAddressItemsModal() {
                $('#editNewAddressItemsModal').modal({ show: true });
            }

            function openDeleteAddressItemsModal() {
                $('#deleteAddressItemsModal').modal({ show: true });
            }

        </script>


    </div>
</asp:Content>

