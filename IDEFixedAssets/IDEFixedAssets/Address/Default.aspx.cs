﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Address_Default : System.Web.UI.Page
{
    UserData u_data = new UserData();
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserData"] == null) Config.Rd("/exit");
        u_data = (UserData)Session["UserData"];
        /*if not admin*/
        if (!u_data.LoginUserType.Equals("admin")) Config.Rd("/index");
        
        if (!IsPostBack)
        {
            FillPageData(u_data);
            FillAddressList();
        }
        
    }


    void fillPriority()
    {
        drlAddNewAddressPriority.Items.Clear();
        drlAddNewAddressPriority.DataTextField = "RECORD_ID";
        drlAddNewAddressPriority.DataSource = db.GetPriorityList();
        drlAddNewAddressPriority.DataBind();
        drlAddNewAddressPriority.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlAddressPriorityLabel"), "0"));

        drlEditAddressPriority.Items.Clear();
        drlEditAddressPriority.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlAddressPriorityLabel"), "0"));
        for (int i = 1; i <= 100; i++)
        {
            drlEditAddressPriority.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    private void FillPageData(UserData u_data)
    {
        ltrPageHeaderAddressLabel.Text = Config.getXmlValue(u_data.lang, "pageHeaderAddressLabel");
        lnkAddress.Text = Config.getXmlValue(u_data.lang, "addressTabLinkNameLabel");
        lnkAddressItems.Text = Config.getXmlValue(u_data.lang, "addressItemsTabLinkLabel");
        ltrNewAddressLabel.Text = Config.getXmlValue(u_data.lang, "lnkNewAddressLabel");
        

        /*New Address Modal*/
        ltrAddNewAddressModalHeader.Text = Config.getXmlValue(u_data.lang, "newAddressModalHeaderLabel");
        txtAddNewAddressTitle.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAddressTitleLabel"));
        txtAddNewAddressDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAddressDescriptionLabel"));

        fillPriority();
      

        btnAddNewAddress.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnAddNewAddressCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");


        /*Edit Address Modal*/
        ltrEditAddressModalHeader.Text = Config.getXmlValue(u_data.lang, "editAddressModalHeaderLabel");
        txtEditAddressTitle.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAddressTitleLabel"));
        txtEditAddressDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAddressDescriptionLabel"));

        btnEditAddress.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnEditAddressCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");


        /*Delete Address Modal*/ 
        ltrDeleteAddressModalHeader.Text = Config.getXmlValue(u_data.lang, "deleteAddressModalHeaderLabel");
        ltrDeleteAddressModalBody.Text = Config.getXmlValue(u_data.lang, "deleteAddressConfirmModalBodyLabel");
        btnAddressDelete.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnAddressDeleteCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");


        /*New AddressItems Modal*/
        ltrAddNewAddressItemsModalHeader.Text = Config.getXmlValue(u_data.lang, "newAddressItemsModalHeaderLabel");
        txtAddNewAddressItemsDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "addressItemsDescriptionLabel"));
        txtAddNewAddressItemsRfIdCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "addressItemsRfIdCodeLabel"));
        txtAddNewAddressItemsBarCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "addressItemsBarCodeLabel"));
        txtAddNewAddressItemsQRCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "addressItemsQRCodeLABEL"));
        btnAddNewAddressItems.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnAddNewAddressItemsCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        ltrCheckBoxWarehouse.Text = Config.getXmlValue(u_data.lang, "checkBoxWarehouseLabel");


        /*Edit AddressItems Modal*/
        ltrEditAddressItemsModalHeader.Text = Config.getXmlValue(u_data.lang, "editAddressItemsModalHeaderLabel");
        txtEditAddressItemsDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "addressItemsDescriptionLabel"));
        txtEditAddressItemsRfIdCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "addressItemsRfIdCodeLabel"));
        txtEditAddressItemsBarCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "addressItemsBarCodeLabel"));
        txtEditAddressItemsQRCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "addressItemsQRCodeLABEL"));
        btnEditAddressItems.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnEditAddressItemsCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        ltrEditCheckBoxWarehouse.Text = Config.getXmlValue(u_data.lang, "checkBoxWarehouseLabel");


        /*Delete AddressItems Modal*/
        ltrDeleteAddressItemsModalHeader.Text = Config.getXmlValue(u_data.lang, "deleteAddressItemsModalHeaderLabel");
        ltrDeleteAddressItemsModalBody.Text = Config.getXmlValue(u_data.lang, "deleteAddressItemsConfirmModalBodyLabel");
        btnAddressItemsDelete.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnAddressItemsDeleteCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");


    }

    void FillAddressList()
    {
        drlAddNewAddressList.Items.Clear();
        drlAddNewAddressList.DataTextField = "TITLE";
        drlAddNewAddressList.DataValueField = "RECORD_ID";
        drlAddNewAddressList.DataSource = db.GetAddressList();
        drlAddNewAddressList.DataBind();
        drlAddNewAddressList.Visible = drlAddNewAddressList.Items.Count > 0;
        drlAddNewAddressList.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlAddressCategoryLabel"), "0"));

        grdAddress.DataSource = db.GetAddressList();
        grdAddress.DataBind();
  
        foreach (GridViewRow row in grdAddress.Rows)
        {
            LinkButton lnkAddressDataEdit = row.FindControl("lnkAddressDataEdit") as LinkButton;
            lnkAddressDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkAddressDelete = row.FindControl("lnkAddressDelete") as LinkButton;
            lnkAddressDelete.Text = Config.getXmlValue(u_data.lang, "dataBtnDeleteLabel");
        }
        
    }
   
    protected void lnkAddress_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        lnkAddress.CssClass = "nav-link active";
        lnkAddressItems.CssClass = "nav-link";
    }

   
    
    protected void lnkNewAddressModal_Click(object sender, EventArgs e)
    {
        ltrAddNewAddressMessage.Text = "";
        drlAddNewAddressPriority.SelectedValue = "0";
        drlAddNewAddressList.SelectedValue = "0";
        txtAddNewAddressTitle.Text = "";
        txtAddNewAddressDescription.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddNewAddressModal();", true);
    }
    
    protected void btnAddNewAddress_Click(object sender, EventArgs e)
    {
        ltrAddNewAddressMessage.Text = "";
        if (txtAddNewAddressTitle.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddNewAddressModal();", true);
            ltrAddNewAddressMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddressTitleMessage"));
            return;
        }
        if (drlAddNewAddressPriority.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddNewAddressModal();", true);
            ltrAddNewAddressMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "drlAddressPriorityMessage"));
            return;
        }

        string result = db.AddAddress(Convert.ToInt32(drlAddNewAddressList.SelectedValue), txtAddNewAddressTitle.Text.Trim(),txtAddNewAddressDescription.Text.Trim(), Convert.ToInt32(drlAddNewAddressPriority.SelectedValue), (short)u_data.LogonUserID);

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAddressSuccessMessage"));
        } 
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillAddressList();
        fillPriority();
    }
    protected void grdAddress_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdAddressTitleLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdAddressDescriptionLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdAddressPrioritetyLabel");
            e.Row.Cells[3].Text = Config.getXmlValue(u_data.lang, "grdAddressParentTitleLabel");
        }
    }
    protected void btnEditAddress_Click(object sender, EventArgs e)
    {
        ltrEditAddressMessage.Text = "";
        int addressId = Convert.ToInt32(hdnFieldEditAddressID.Value);

        if (txtEditAddressTitle.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditAddressModal();", true);
            ltrEditAddressMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddressTitleMessage"));
            return;
        }
        if (drlEditAddressPriority.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditAddressModal();", true);
            ltrEditAddressMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "drlAddressPriorityMessage"));
            return;
        }

        if (drlEditAddressList.SelectedValue == addressId.ToString())
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "editAddressWarningMessage1"));
            return;
        }

        string result = db.UpdateAddress(Convert.ToInt32(addressId), Convert.ToInt32(drlEditAddressList.SelectedValue), 
            txtEditAddressTitle.Text.Trim(), txtEditAddressDescription.Text.Trim(),Convert.ToInt32(drlEditAddressPriority.SelectedValue), 
            (short)u_data.LogonUserID);
 
        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "editAddressSuccessMessage"));
        }
        else if (result.Equals("NOTACCESS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "editAddressNotAccessWarningMessage2"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillAddressList();
        fillPriority();
        
    }
    
    protected void lnkAddressDataEdit_Click(object sender, EventArgs e)
    {
        ltrEditAddressMessage.Text = "";

        drlEditAddressList.Items.Clear();
        drlEditAddressList.DataTextField = "TITLE";
        drlEditAddressList.DataValueField = "RECORD_ID";
        drlEditAddressList.DataSource = db.GetAddressList();
        drlEditAddressList.DataBind();
        drlEditAddressList.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlAddressCategoryLabel"), "0"));

        LinkButton lnkAddressDataEdit = (LinkButton)sender;
        hdnFieldEditAddressID.Value = lnkAddressDataEdit.CommandArgument;
        FillAddressDataById(Convert.ToInt32(hdnFieldEditAddressID.Value));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditAddressModal();", true);
    }

    private void FillAddressDataById(int editAddressID)
    {
        DataRow drEditAddress = db.GetAddressById(editAddressID);
        if (drEditAddress != null)
        {
            txtEditAddressTitle.Text = Convert.ToString(drEditAddress["TITLE"]);
            txtEditAddressDescription.Text = Convert.ToString(drEditAddress["DESCRIPTION"]);
            drlEditAddressList.SelectedValue = Convert.ToString(drEditAddress["PARENT_ID"]);
            drlEditAddressPriority.SelectedValue = Convert.ToString(drEditAddress["PRIORITY"]);
        }
    }
    
    protected void lnkAddressDelete_Click(object sender, EventArgs e)
    {
        LinkButton lnkAddressDelete = (LinkButton)sender;
        hdnFieldDeleteAddressID.Value = lnkAddressDelete.CommandArgument;      
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteAddressModal();", true);
    }
    protected void btnAddressDelete_Click(object sender, EventArgs e)
    {
        int deleteAddressId = Convert.ToInt32(hdnFieldDeleteAddressID.Value);
        string result = db.DeleteAddress(deleteAddressId, (short)u_data.LogonUserID);
        
        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "deleteAddressItemSuccessMessage"));
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillAddressList();
        fillPriority();
    }







    /*ADDRESS ITEMS*/

    void addressPageItemsTab()
    {
        MultiView1.ActiveViewIndex = 1;
        lnkAddress.CssClass = "nav-link";
        lnkAddressItems.CssClass = "nav-link active";

        drlAddressListForItems.DataValueField = "RECORD_ID";
        drlAddressListForItems.DataTextField = "TITLE";
        drlAddressListForItems.DataSource = db.GetAddressList();
        drlAddressListForItems.DataBind();
        drlAddressListForItems.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlAddressCategoryLabel1"), "0"));
        pnlNewAddressItemBox.Visible = (drlAddressListForItems.SelectedValue != null && drlAddressListForItems.SelectedValue != "0");
        ltrNewAddressItemsLabel1.Text = Config.getXmlValue(u_data.lang, "lnkNewAddressItemsLabel");
    }

    void loadDataFor_addNewAddressItems(int addressId)
    {
        drlWarehouseListAdd.DataTextField = "DESCRIPTION";
        drlWarehouseListAdd.DataValueField = "CODE";
        drlWarehouseListAdd.DataSource = db.GetWarehouse("ACTIVE");
        drlWarehouseListAdd.DataBind();
        drlWarehouseListAdd.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang,"selectWarehouseLabel"), "0"));
        drlWarehouseListAdd.SelectedValue = "0";

        drlAddNewAddressItemsList.Items.Clear();
        drlAddNewAddressItemsList.DataValueField = "RECORD_ID";
        drlAddNewAddressItemsList.DataTextField = "DESCRIPTION";
        drlAddNewAddressItemsList.DataSource = db.GetAddressItemTopCategoryList(addressId);
        drlAddNewAddressItemsList.DataBind();
        drlAddNewAddressItemsList.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlAddressCategoryLabel"), "0"));
        drlAddNewAddressItemsList.SelectedValue = "0";

        txtAddNewAddressItemsDescription.Text = "";
        txtAddNewAddressItemsRfIdCode.Text = "";
        txtAddNewAddressItemsBarCode.Text = "";
        txtAddNewAddressItemsQRCode.Text = "";   
    }



    protected void lnkAddressItems_Click(object sender, EventArgs e)
    {
        addressPageItemsTab();
        FillAddressItemsList(Convert.ToInt32(drlAddressListForItems.SelectedValue));
    }


    protected void drlAddressListForItems_SelectedIndexChanged(object sender, EventArgs e)
    {
        int addressId = Convert.ToInt32(drlAddressListForItems.SelectedValue);
        pnlNewAddressItemBox.Visible = (drlAddressListForItems.SelectedValue != null && drlAddressListForItems.SelectedValue != "0");
        FillAddressItemsList(addressId);
        loadDataFor_editAddressItems(addressId);
    }
    
    protected void lnkNewAddressItemsModal_Click(object sender, EventArgs e)
    {
        ltrAddNewAddressItemsMessage.Text = "";
        int addressId = Convert.ToInt32(drlAddressListForItems.SelectedValue);
        loadDataFor_addNewAddressItems(addressId);

        chkaAddNewAddressItemsDescriptionSelect.Checked = false;
        divAddNewAddressItemsDescription.Visible = (!chkaAddNewAddressItemsDescriptionSelect.Checked);
        divWarehouseListAdd.Visible = (chkaAddNewAddressItemsDescriptionSelect.Checked); 
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddNewAddressItemsModal();", true);
    }
    protected void btnAddNewAddressItems_Click(object sender, EventArgs e)
    {
        ltrAddNewAddressItemsMessage.Text = "";
        string description = "", code = null,rfidCode = null,barCode = null,qrCode = null;
        int addressId = Convert.ToInt32(drlAddressListForItems.SelectedValue);
        int addressItems_ownerId = Convert.ToInt32(drlAddNewAddressItemsList.SelectedValue);

        if (chkaAddNewAddressItemsDescriptionSelect.Checked)
        {
            if (drlWarehouseListAdd.SelectedValue == "0")
            {
                ltrAddNewAddressItemsMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "selectWarehouseLabel"));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddNewAddressItemsModal();", true);
                return;
            }
            description = drlWarehouseListAdd.SelectedItem.Text;
            code = drlWarehouseListAdd.SelectedValue;
        }
        else 
        {
            if (txtAddNewAddressItemsDescription.Text.Trim().Length < 1)
            {
                ltrAddNewAddressItemsMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addressItemsDescriptionMessage"));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddNewAddressItemsModal();", true);
                return;
            }
            description = txtAddNewAddressItemsDescription.Text.Trim();
        }
        if (txtAddNewAddressItemsRfIdCode.Text.Trim().Length > 0) rfidCode = txtAddNewAddressItemsRfIdCode.Text.Trim();
        if (txtAddNewAddressItemsBarCode.Text.Trim().Length > 0) barCode = txtAddNewAddressItemsBarCode.Text.Trim();
        if (txtAddNewAddressItemsQRCode.Text.Trim().Length > 0) qrCode = txtAddNewAddressItemsQRCode.Text.Trim();

        string result = db.AddAddressItems(addressId, addressItems_ownerId, description, code, rfidCode, barCode, qrCode,(short)u_data.LogonUserID);
        
        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAddressItemsSuccessMessage"));
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillAddressItemsList(addressId);
    }
   
    protected void chkaAddNewAddressItemsDescriptionSelect_CheckedChanged(object sender, EventArgs e)
    {
        ltrAddNewAddressItemsMessage.Text = "";
        divAddNewAddressItemsDescription.Visible = (!chkaAddNewAddressItemsDescriptionSelect.Checked);
        divWarehouseListAdd.Visible = (chkaAddNewAddressItemsDescriptionSelect.Checked);
        drlWarehouseListAdd.SelectedValue = "0";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddNewAddressItemsModal();", true);
    }


    void FillAddressItemsList(int addressId)
    {
        grdAddressItems.DataSource = db.GetAddressItemList(addressId);
        grdAddressItems.DataBind();

        foreach (GridViewRow row in grdAddressItems.Rows)
        {
            LinkButton lnkAddressItemsDataEdit = row.FindControl("lnkAddressItemsDataEdit") as LinkButton;
            lnkAddressItemsDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkAddressItemsDelete = row.FindControl("lnkAddressItemsDelete") as LinkButton;
            lnkAddressItemsDelete.Text = Config.getXmlValue(u_data.lang, "dataBtnDeleteLabel");
        }

    }

    protected void grdAddressItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdAddressItemsDescriptionLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdAddressItemsCodeLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdAddressItemsRFIDCODELabel");
            e.Row.Cells[3].Text = Config.getXmlValue(u_data.lang, "grdAddressItemsBARCODELabel");
            e.Row.Cells[4].Text = Config.getXmlValue(u_data.lang, "grdAddressItemsQRCODELabel");
            e.Row.Cells[5].Text = Config.getXmlValue(u_data.lang, "grdAddressItemsParnetDescriptionLabel");
        }
    }



    void loadDataFor_editAddressItems(int addressId)
    {
        drlWarehouseListEdit.Items.Clear();
        drlWarehouseListEdit.DataTextField = "DESCRIPTION";
        drlWarehouseListEdit.DataValueField = "CODE";
        drlWarehouseListEdit.DataSource = db.GetWarehouse("ACTIVE");
        drlWarehouseListEdit.DataBind();
        drlWarehouseListEdit.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectWarehouseLabel"), "0"));
        drlWarehouseListEdit.SelectedValue = "0";

        txtEditAddressItemsDescription.Text = "";
        txtEditAddressItemsRfIdCode.Text = "";
        txtEditAddressItemsBarCode.Text = "";
        txtEditAddressItemsQRCode.Text = "";
    }

    protected void lnkAddressItemsDataEdit_Click(object sender, EventArgs e)
    {
        ltrEditAddressItemsMessage.Text = "";

        drlEditAddressItemsList.Items.Clear();
        drlEditAddressItemsList.DataValueField = "RECORD_ID";
        drlEditAddressItemsList.DataTextField = "DESCRIPTION";
        drlEditAddressItemsList.DataSource = db.GetAddressItemTopCategoryList(Convert.ToInt32(drlAddressListForItems.SelectedValue));
        drlEditAddressItemsList.DataBind();
        drlEditAddressItemsList.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlAddressCategoryLabel"), "0"));

        LinkButton lnkAddressItemsDataEdit = (LinkButton)sender;
        hdnFieldEditAddressItemsID.Value = lnkAddressItemsDataEdit.CommandArgument;
        FillAddressItemsDataById(Convert.ToInt32(hdnFieldEditAddressItemsID.Value));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditAddressItemsModal();", true);
    }

    private void FillAddressItemsDataById(int editAddressItemsID)
    {
        DataRow drEditAddressItems = db.GetAddressItemsById(editAddressItemsID);
        drlEditAddressItemsList.SelectedValue = "0";
        if (drEditAddressItems != null)
        {
            if (Convert.ToString(drEditAddressItems["CODE"]).Trim() != "") // 
            {
                divWarehouseListEdit.Visible = true;
                divEditAddressItemsDescription.Visible = false;
                txtEditAddressItemsDescription.Text = "";
                drlWarehouseListEdit.SelectedValue = Convert.ToString(drEditAddressItems["CODE"]);
                chkaEditAddressItemsDescriptionSelect.Checked = true;
            }
            else
            {
                divWarehouseListEdit.Visible = false;
                divEditAddressItemsDescription.Visible = true;
                drlWarehouseListEdit.SelectedValue = "0";
                txtEditAddressItemsDescription.Text = Convert.ToString(drEditAddressItems["DESCRIPTION"]);
                chkaEditAddressItemsDescriptionSelect.Checked = false;
            }

            txtEditAddressItemsRfIdCode.Text = Convert.ToString(drEditAddressItems["RFIDCODE"]);
            txtEditAddressItemsBarCode.Text = Convert.ToString(drEditAddressItems["BARCODE"]);
            txtEditAddressItemsQRCode.Text = Convert.ToString(drEditAddressItems["QRCODE"]);


            if (Convert.ToString(drEditAddressItems["OWNER_RECID"]) != "0") // if not main category
            {
                drlEditAddressItemsList.SelectedValue = Convert.ToString(drEditAddressItems["OWNER_RECID"]);
            }
            
           
        }
    }
    protected void chkaEditAddressItemsDescriptionSelect_CheckedChanged(object sender, EventArgs e)
    {
        ltrEditAddressItemsMessage.Text = "";
        divEditAddressItemsDescription.Visible = (!chkaEditAddressItemsDescriptionSelect.Checked);
        divWarehouseListEdit.Visible = (chkaEditAddressItemsDescriptionSelect.Checked);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditAddressItemsModal();", true);
    }
    
    protected void btnEditAddressItems_Click(object sender, EventArgs e)
    {
        ltrEditAddressItemsMessage.Text = "";
        string description = "", code = null, rfidCode = null, barCode = null, qrCode = null;
        int addressId = Convert.ToInt32(drlAddressListForItems.SelectedValue);
        int addressItems_ownerId = Convert.ToInt32(drlEditAddressItemsList.SelectedValue);
        int record_id = Convert.ToInt32(hdnFieldEditAddressItemsID.Value);

        if (chkaEditAddressItemsDescriptionSelect.Checked)
        {
            if (drlWarehouseListEdit.SelectedValue == "0")
            {
                ltrEditAddressItemsMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "selectWarehouseLabel"));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditAddressItemsModal();", true);
                return;
            }
            description = drlWarehouseListEdit.SelectedItem.Text;
            code = drlWarehouseListEdit.SelectedValue;
        }
        else
        {
            if (txtEditAddressItemsDescription.Text.Trim().Length < 1)
            {
                ltrEditAddressItemsMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addressItemsDescriptionMessage"));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditAddressItemsModal();", true);
                return;
            }
            description = txtEditAddressItemsDescription.Text.Trim();
        }
        if (txtEditAddressItemsRfIdCode.Text.Trim().Length > 0) rfidCode = txtEditAddressItemsRfIdCode.Text.Trim();
        if (txtEditAddressItemsBarCode.Text.Trim().Length > 0) barCode = txtEditAddressItemsBarCode.Text.Trim();
        if (txtEditAddressItemsQRCode.Text.Trim().Length > 0) qrCode = txtEditAddressItemsQRCode.Text.Trim();

        if (addressItems_ownerId == record_id)
        {
            ltrEditAddressItemsMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "editAddressItemsWarningMessage1"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditAddressItemsModal();", true);
            return;
        }

        string result = db.UpdateAddressItems(record_id, addressItems_ownerId, description, code, rfidCode, barCode, qrCode, (short)u_data.LogonUserID);

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAddressItemsSuccessMessage"));
        }
        else if (result.Equals("NOTACCESS"))
        {
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "editAddressItemsNotAccessWarningMessage2"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);          
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillAddressItemsList(addressId);
    }
    protected void lnkAddressItemsDelete_Click(object sender, EventArgs e)
    {
        LinkButton lnkAddressItemsDelete = (LinkButton)sender;
        hdnFieldDeleteAddressItemsID.Value = lnkAddressItemsDelete.CommandArgument;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteAddressItemsModal();", true);
    }
    protected void btnAddressItemsDelete_Click(object sender, EventArgs e)
    {
        int addressId = Convert.ToInt32(drlAddressListForItems.SelectedValue);
        int deleteAddressItemsId = Convert.ToInt32(hdnFieldDeleteAddressItemsID.Value);
        string result = db.DeleteAddressItems(deleteAddressItemsId, (short)u_data.LogonUserID);

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "deleteAddressItemsSuccessMessage"));
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillAddressItemsList(addressId);
    }
}