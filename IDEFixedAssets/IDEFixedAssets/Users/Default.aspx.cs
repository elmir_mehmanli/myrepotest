﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Users_Default : System.Web.UI.Page
{
    UserData u_data = new UserData();
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserData"] == null) Config.Rd("/exit");
        u_data = (UserData)Session["UserData"];
       

        if (!IsPostBack)
        {
            /*if not admin*/
            if (!u_data.LoginUserType.Equals("admin")) Config.Rd("/index");

            /*Page Labes*/
            FillPageData(u_data);
            
            /*User List*/
            FillUsers();

            /*Add User- Fill UserGender & UserType dropdown*/
            drlUserGender.Items.Clear();
            drlUserGender.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserGenderLabel"), ""));
            drlUserGender.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserGenderMLabel"), "M"));
            drlUserGender.Items.Insert(2, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserGenderFLabel"), "F"));

            drlUserType.Items.Clear();
            drlUserType.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserTypeLabel"), ""));
            drlUserType.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserTypeAdminLabel"), "admin"));
            drlUserType.Items.Insert(2, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserTypeUserLabel"), "user"));


            /*Edit User- Fill UserGender & UserType dropdown*/
            drlEditUserGender.Items.Clear();
            drlEditUserGender.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserGenderLabel"), ""));
            drlEditUserGender.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserGenderMLabel"), "M"));
            drlEditUserGender.Items.Insert(2, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserGenderFLabel"), "F"));

            drlEditUserType.Items.Clear();
            drlEditUserType.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserTypeLabel"), ""));
            drlEditUserType.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserTypeAdminLabel"), "admin"));
            drlEditUserType.Items.Insert(2, new ListItem(Config.getXmlValue(u_data.lang, "userPageUserTypeUserLabel"), "user"));
          
        }
    }

    void FillPageData(UserData u_data)
    {
        ltrPageHeaderUsersLabel.Text = Config.getXmlValue(u_data.lang, "pageHeaderUsersLabel");
        ltrAddNewUser.Text = Config.getXmlValue(u_data.lang, "addNewUserLabel");

        /*Add User Inputs and Labels*/
        ltrAddNewUserModalHeader.Text = Config.getXmlValue(u_data.lang, "addNewUserModalHeaderLabel");
        txtUserName.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "userPageUserFirstNameLabel"));
        txtUserSurname.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "userPageUserSurNameLabel"));
        txtUserContactNumber.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "userPageUserContactNumberLabel"));
        txtUserLogin.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "loginNameLabel"));
        txtUserPassword.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "loginPasswordLabel"));
        txtUserPasswordRepeat.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "loginPasswordLabelRepeat"));
        txtUserPosition.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "userPageUserPositionLabel"));
        btnUserAdd.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnUserAddCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");



        /*Edit User Inputs and Labels*/
        ltrEditUserModalHeader.Text = Config.getXmlValue(u_data.lang, "editUserModalHeaderLabel");
        txtEditUserName.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "userPageUserFirstNameLabel"));
        txtEditUserSurname.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "userPageUserSurNameLabel"));
        txtEditUserContactNumber.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "userPageUserContactNumberLabel"));
        txtEditUserLogin.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "loginNameLabel"));
        txtEditUserPosition.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "userPageUserPositionLabel"));
        btnUserEdit.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnUserEditCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");


        /*Change Password User  Labels*/
        ltrChangePasswordUserModalHeader.Text = Config.getXmlValue(u_data.lang, "pageHeaderUserProfileLabel");
        txtChangePasswordUserPassword.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "loginPasswordLabel"));
        txtChangePasswordUserPasswordRepeat.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "loginPasswordLabelRepeat"));
        btnUserChangePassword.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnUserChangePasswordCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");


        /*Block  User  Labels*/
        ltrBlockUserModalHeader.Text = Config.getXmlValue(u_data.lang, "blockUserModalHeaderLabel");    
        btnUserBlock.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnUserBlockCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");
        ltrBlockUserModalBody.Text = Config.getXmlValue(u_data.lang, "blockUserModalBodyLabel");

    }

    void FillUsers()
    {
        grdUsers.DataSource = db.GetUserList();
        grdUsers.DataBind();
        grdUsers.UseAccessibleHeader = true;
        if (grdUsers.Rows.Count > 0)
        {
            grdUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
            /*if ever needed, it would be true  */
            grdUsers.Columns[4].Visible = false;
        }

        foreach (GridViewRow row in grdUsers.Rows)
        {
            LinkButton lnkViewUserData = row.FindControl("lnkViewUserData") as LinkButton;
            lnkViewUserData.ToolTip = Config.getXmlValue(u_data.lang, "viewUserDataLabel");  

            LinkButton lnkUserDataEdit = row.FindControl("lnkUserDataEdit") as LinkButton;
            lnkUserDataEdit.Text = Config.getXmlValue(u_data.lang, "userDataEditLabel");

            LinkButton lnkUserChangePassword = row.FindControl("lnkUserChangePassword") as LinkButton;
            lnkUserChangePassword.Text = Config.getXmlValue(u_data.lang, "changePasswordLabel");

            LinkButton lnkUserBlock = row.FindControl("lnkUserBlock") as LinkButton;
            lnkUserBlock.Text = Config.getXmlValue(u_data.lang, "userBlockLabel");
        }
    }

    protected void grdUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdUsersUserNameLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdUsersUserFullNameLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdUsersUserContactNumberLabel");
            e.Row.Cells[3].Text = Config.getXmlValue(u_data.lang, "grdUsersUserPositionLabel");      
        }
    }
    protected void btnUserAdd_Click(object sender, EventArgs e)
    {
        ltrMessage.Text = "";
        if (txtUserName.Text.Trim().Length < 3)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_UsernameValidator"));
            return;
        }
        if (txtUserSurname.Text.Trim().Length < 3)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_SurnameValidator"));
            return;
        }

        if (txtUserLogin.Text.Trim().Length < 3 || !Config.isLoginSymbols(txtUserLogin.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_LoginValidator"));
            return;
        }

        if (txtUserPassword.Text.Trim().Length < 6 || !Config.isEnglish(txtUserPassword.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changePasswordErrorMessage1"));
            return;
        }

        if (txtUserPassword.Text != txtUserPasswordRepeat.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changePasswordErrorMessage2"));
            return;
        }

        if (drlUserGender.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_UserGenderValidator"));
            return;
        }

        if (drlUserType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_UserTypeValidator"));
            return;
        }
        string imageName = "";
        if (drlUserGender.SelectedValue == "M") imageName = "/img/man.png";
        if (drlUserGender.SelectedValue == "F") imageName = "/img/woman.png";

        string result = db.AddUser(txtUserName.Text, txtUserSurname.Text, txtUserLogin.Text, txtUserPassword.Text, txtUserContactNumber.Text,
            (short)u_data.LogonUserID, drlUserGender.SelectedValue, txtUserPosition.Text, imageName, drlUserType.SelectedValue);

        if (result == "EXISTS_USER")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_ExistsUser"));
            return;
        }

        if (result == "EXISTS_USER")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_ExistsUserMessage"));
            return;
        }

        if (result == "ERROR")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }

        Config.Rd("/users");

    }
   
    
    protected void lnkUserDataEdit_Click(object sender, EventArgs e)
    {
        ltrEditMessage.Text = "";
        LinkButton lnkUserDataEdit = (LinkButton)sender;
        hdnFieldEditUserID.Value = lnkUserDataEdit.CommandArgument;
        FillUserDataById(Convert.ToInt16(hdnFieldEditUserID.Value));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditUserModal();", true);
    }

    private void FillUserDataById(short editUserID)
    {
        DataRow drEditUser = db.GetUserById(editUserID);
        if (drEditUser != null)
        {
            txtEditUserName.Text = Convert.ToString(drEditUser["NAME"]);
            txtEditUserSurname.Text = Convert.ToString(drEditUser["SURNAME"]);
            txtEditUserContactNumber.Text = Convert.ToString(drEditUser["CONTACTNUMBER"]);
            txtEditUserLogin.Text = Convert.ToString(drEditUser["USER_NAME"]);
            txtEditUserPosition.Text = Convert.ToString(drEditUser["POSITION"]);
            drlEditUserGender.SelectedValue = Convert.ToString(drEditUser["GENDER"]);
            drlEditUserType.SelectedValue = Convert.ToString(drEditUser["TYPE"]);

        }
    }
    protected void btnUserEdit_Click(object sender, EventArgs e)
    {
        ltrMessage.Text = "";

        short editUserId = Convert.ToInt16(hdnFieldEditUserID.Value);

        if (txtEditUserName.Text.Trim().Length < 3)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditUserModal();", true);
            ltrEditMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_UsernameValidator"));
            return;
        }
        if (txtEditUserSurname.Text.Trim().Length < 3)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditUserModal();", true);
            ltrEditMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_SurnameValidator"));
            return;
        }


        if (drlEditUserGender.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditUserModal();", true);
            ltrEditMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_UserGenderValidator"));
            return;
        }

        if (drlEditUserType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditUserModal();", true);
            ltrEditMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addNewUser_UserTypeValidator"));
            return;
        }
        string imageName = "";
        if (drlEditUserGender.SelectedValue == "M") imageName = "/img/man.png";
        if (drlEditUserGender.SelectedValue == "F") imageName = "/img/woman.png";

        string result = db.UpdateUser(editUserId, txtEditUserName.Text, txtEditUserSurname.Text, txtEditUserContactNumber.Text, (short)u_data.LogonUserID, drlEditUserGender.SelectedValue, txtEditUserPosition.Text, imageName, drlEditUserType.SelectedValue);
        if (result != "OK")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditUserModal();", true);
            ltrEditMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        Config.Rd("/users");
    }
    
    protected void lnkUserChangePassword_Click(object sender, EventArgs e)
    {
        ltrChangePasswordMessage.Text = "";
        LinkButton lnkUserChangePassword = (LinkButton)sender;
        hdnFieldChangePasswordUserID.Value = lnkUserChangePassword.CommandArgument;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openChangePasswordUserModal();", true);
    }
  
    protected void btnUserChangePassword_Click(object sender, EventArgs e)
    {
        short changePasswordUserId = Convert.ToInt16(hdnFieldChangePasswordUserID.Value);

        if (txtChangePasswordUserPassword.Text.Trim().Length < 6 || !Config.isEnglish(txtChangePasswordUserPassword.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openChangePasswordUserModal();", true);
            ltrChangePasswordMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changePasswordErrorMessage1"));
            return;
        }

        if (txtChangePasswordUserPassword.Text != txtChangePasswordUserPasswordRepeat.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openChangePasswordUserModal();", true);
            ltrChangePasswordMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changePasswordErrorMessage2"));
            return;
        }

        string result = db.ChangePassword(changePasswordUserId, txtChangePasswordUserPassword.Text.Trim(), (short)u_data.LogonUserID);
        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openChangePasswordUserModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        Config.Rd("/users");
       
    }
   
    
    protected void lnkUserBlock_Click(object sender, EventArgs e)
    {
        ltrBlockMessage.Text = "";
        LinkButton lnkUserBlock = (LinkButton)sender;
        hdnFieldBlockUserID.Value = lnkUserBlock.CommandArgument;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openBlockUserModal();", true);
    }
    protected void btnUserBlock_Click(object sender, EventArgs e)
    {
        short blockUserId = Convert.ToInt16(hdnFieldBlockUserID.Value);
        string result = db.BlockUser(blockUserId, "DEACTIVE", (short)u_data.LogonUserID);
        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openBlockUserModal();", true);
            ltrBlockMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        Config.Rd("/users");
    }
}