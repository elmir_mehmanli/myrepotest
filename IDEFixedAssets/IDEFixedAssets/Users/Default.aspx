﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Users_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../lib/jquery/jquery.js"></script>
    <script>
        $(document).ready(function () {
            $("#txtUserPasswordShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtUserPasswordShow input').attr("type") == "text") {
                    $('#txtUserPasswordShow input').attr('type', 'password');
                    $('#txtUserPasswordShow i').addClass("fa-eye-slash");
                    $('#txtUserPasswordShow i').removeClass("fa-eye");
                } else if ($('#txtUserPasswordShow input').attr("type") == "password") {
                    $('#txtUserPasswordShow input').attr('type', 'text');
                    $('#txtUserPasswordShow i').removeClass("fa-eye-slash");
                    $('#txtUserPasswordShow i').addClass("fa-eye");
                }
            });
        });

        $(document).ready(function () {
            $("#txtUserPasswordRepeatShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtUserPasswordRepeatShow input').attr("type") == "text") {
                    $('#txtUserPasswordRepeatShow input').attr('type', 'password');
                    $('#txtUserPasswordRepeatShow i').addClass("fa-eye-slash");
                    $('#txtUserPasswordRepeatShow i').removeClass("fa-eye");
                } else if ($('#txtUserPasswordRepeatShow input').attr("type") == "password") {
                    $('#txtUserPasswordRepeatShow input').attr('type', 'text');
                    $('#txtUserPasswordRepeatShow i').removeClass("fa-eye-slash");
                    $('#txtUserPasswordRepeatShow i').addClass("fa-eye");
                }
            });
        });


        $(document).ready(function () {
            $("#txtChangePasswordUserPasswordShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtChangePasswordUserPasswordShow input').attr("type") == "text") {
                    $('#txtChangePasswordUserPasswordShow input').attr('type', 'password');
                    $('#txtChangePasswordUserPasswordShow i').addClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordShow i').removeClass("fa-eye");
                } else if ($('#txtChangePasswordUserPasswordShow input').attr("type") == "password") {
                    $('#txtChangePasswordUserPasswordShow input').attr('type', 'text');
                    $('#txtChangePasswordUserPasswordShow i').removeClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordShow i').addClass("fa-eye");
                }
            });
        });

        $(document).ready(function () {
            $("#txtChangePasswordUserPasswordRepeatShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtChangePasswordUserPasswordRepeatShow input').attr("type") == "text") {
                    $('#txtChangePasswordUserPasswordRepeatShow input').attr('type', 'password');
                    $('#txtChangePasswordUserPasswordRepeatShow i').addClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordRepeatShow i').removeClass("fa-eye");
                } else if ($('#txtChangePasswordUserPasswordRepeatShow input').attr("type") == "password") {
                    $('#txtChangePasswordUserPasswordRepeatShow input').attr('type', 'text');
                    $('#txtChangePasswordUserPasswordRepeatShow i').removeClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordRepeatShow i').addClass("fa-eye");
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .dropdown-menu {
            min-width: 120%;
            margin: 0.125rem 0 0;
            font-size: 0.875rem;
            list-style: none;
            transform: translate3d(0px, 0px, 0px)!important;
        }

        .list-group-item {
            position: relative;
            display: block;
            padding: 5px 10px;
            background-color: #fff;
            border: 0px;
        }


            .list-group-item :hover {
                background-color: #dadada;
                padding: 4px 7px;
            }
    </style>

    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">IDE Fixed Assets</a>
            <span class="breadcrumb-item active">
                <asp:Literal ID="ltrPageHeaderUsersLabel" runat="server"></asp:Literal>
            </span>
        </nav>

        <div class="sl-pagebody">

            <!-- SMALL MODAL -->
            <div id="alertmodal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                        <div class="modal-footer justify-content-right" style="padding: 5px">
                            <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
                <!-- modal-dialog -->
            </div>
            <!-- modal -->

            <div class="card pd-sm-10">
                <asp:LinkButton ID="btnAddNew" runat="server"
                    CssClass="btn btn-outline-info btn-block"
                    Style="width: 20% !important"
                    data-toggle="modal" data-target="#addNewUserModal">
                    <i class="icon ion-person-add"></i>
                    <asp:Literal ID="ltrAddNewUser" runat="server"></asp:Literal>
                </asp:LinkButton>
                <br />
                <div class="table-responsive">
                    <asp:GridView ID="grdUsers" class="table" runat="server" AutoGenerateColumns="False" DataKeyNames="RECORD_ID" GridLines="None" EnableModelValidation="True" OnRowDataBound="grdUsers_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="USER_NAME" />
                            <asp:BoundField DataField="FULLNAME" />
                            <asp:BoundField DataField="CONTACTNUMBER" />
                            <asp:BoundField DataField="POSITION" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkViewUserData"
                                        runat="server"
                                        class="btn btn-link" CommandArgument='<%# Eval("RECORD_ID") %>'>
                                               <i class="fa fa-eye"></i></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <div class="dropdown">
                                        <asp:LinkButton ID="lnkActionUserData"
                                            runat="server"
                                            class="btn btn-link" data-toggle="dropdown">
                                                <i class="icon ion-more"></i></asp:LinkButton>
                                        <ul class="dropdown-menu">
                                            <li class="list-group-item">
                                                <asp:LinkButton ID="lnkUserDataEdit" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' OnClick="lnkUserDataEdit_Click" />
                                            </li>
                                            <li class="list-group-item">
                                                <asp:LinkButton ID="lnkUserChangePassword" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' OnClick="lnkUserChangePassword_Click" />
                                            </li>
                                            <li class="list-group-item">
                                                <asp:LinkButton ID="lnkUserBlock" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' OnClick="lnkUserBlock_Click" />
                                            </li>
                                        </ul>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>


                <!-- Begin New User modal-dialog -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="addNewUserModal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-person-add"></i>
                                            &nbsp&nbsp
                                                    <asp:Literal ID="ltrAddNewUserModalHeader" runat="server"></asp:Literal>
                                        </h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrMessage" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtUserName" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtUserSurname" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtUserContactNumber" MaxLength="12" runat="server" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtUserLogin" runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg input-group" id ="txtUserPasswordShow">
                                                    <asp:TextBox ID="txtUserPassword" runat="server" MaxLength="50" class="form-control" TextMode="Password"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                      <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                     </div>
                                                </div>
                                                <div class="col-lg input-group" id ="txtUserPasswordRepeatShow">
                                                    <asp:TextBox ID="txtUserPasswordRepeat" runat="server" MaxLength="50" class="form-control" TextMode="Password"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                      <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                     </div>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtUserPosition" runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    <asp:DropDownList ID="drlUserGender" class="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-lg">
                                                    <asp:DropDownList ID="drlUserType" class="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="add_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnUserAdd" class="btn btn-info pd-x-20" runat="server" OnClick="btnUserAdd_Click"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loading').style.display = '';
                                                           document.getElementById('btnUserAddCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button runat="server" id="btnUserAddCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUserAdd" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- End New User modal-dialog -->




                <!-- Begin Edit User modal-dialog -->
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="editNewUserModal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                            &nbsp&nbsp
                                                <asp:Literal ID="ltrEditUserModalHeader" runat="server"></asp:Literal>
                                        </h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrEditMessage" runat="server"></asp:Literal>
                                        <asp:HiddenField ID="hdnFieldEditUserID" runat="server" />

                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtEditUserName" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtEditUserSurname" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtEditUserContactNumber" MaxLength="12" runat="server" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtEditUserLogin" ReadOnly runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>

                                                <div class="col-lg">
                                                    <asp:TextBox ID="txtEditUserPosition" runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>

                                            </div>
                                            <div class="row mg-t-20">

                                                <div class="col-lg">
                                                    <asp:DropDownList ID="drlEditUserGender" class="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-lg">
                                                    <asp:DropDownList ID="drlEditUserType" class="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="edit_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnUserEdit" class="btn btn-info pd-x-20" runat="server" OnClick="btnUserEdit_Click"
                                            OnClientClick="this.style.display = 'none';
                                                       document.getElementById('edit_loading').style.display = '';
                                                       document.getElementById('btnUserEditCancel').style.display = 'none';
                                                       document.getElementById('alert_msg').style.display = 'none';" />
                                        <button runat="server" id="btnUserEditCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUserEdit" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- End Edit User modal-dialog -->




                <!-- Begin Change Password User modal-dialog -->
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="changePasswordUserModal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-ios-gear-outline"></i>
                                            &nbsp&nbsp
                                    <asp:Literal ID="ltrChangePasswordUserModalHeader" runat="server"></asp:Literal>
                                        </h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrChangePasswordMessage" runat="server"></asp:Literal>
                                        <asp:HiddenField ID="hdnFieldChangePasswordUserID" runat="server" />
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row mg-t-20">
                                                <div class="col-lg input-group" id="txtChangePasswordUserPasswordShow">
                                                    <asp:TextBox ID="txtChangePasswordUserPassword" runat="server" MaxLength="50" class="form-control" TextMode="Password"></asp:TextBox>
                                                      <div class="input-group-addon">
                                                        <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                      </div>
                                                </div>
                                                <div class="col-lg input-group" id ="txtChangePasswordUserPasswordRepeatShow">
                                                    <asp:TextBox ID="txtChangePasswordUserPasswordRepeat" runat="server" MaxLength="50" class="form-control" TextMode="Password"></asp:TextBox>
                                                    <div class="input-group-addon">
                                                        <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="changePassword_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnUserChangePassword" class="btn btn-info pd-x-20" runat="server" OnClick="btnUserChangePassword_Click"
                                            OnClientClick="this.style.display = 'none';
                                           document.getElementById('changePassword_loading').style.display = '';
                                           document.getElementById('btnUserChangePasswordCancel').style.display = 'none';
                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button runat="server" id="btnUserChangePasswordCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUserChangePassword" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- End Change Password User modal-dialog -->




                <!-- Begin Block User modal-dialog -->
                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="blockUserModal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-trash-a"></i>
                                            &nbsp&nbsp
                                    <asp:Literal ID="ltrBlockUserModalHeader" runat="server"></asp:Literal>
                                        </h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <asp:Literal ID="ltrBlockMessage" runat="server"></asp:Literal>
                                    <asp:HiddenField ID="hdnFieldBlockUserID" runat="server" />
                                    <div class="card pd-20 pd-sm-30">
                                        <div class="row mg-t-20">
                                            <h5>
                                                <asp:Literal ID="ltrBlockUserModalBody" runat="server"></asp:Literal></h5>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="userBlock_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnUserBlock" class="btn btn-info pd-x-20" runat="server" OnClick="btnUserBlock_Click"
                                            OnClientClick="this.style.display = 'none';
                                           document.getElementById('userBlock_loading').style.display = '';
                                           document.getElementById('btnUserBlockCancel').style.display = 'none';
                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button runat="server" id="btnUserBlockCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUserBlock" />
                    </Triggers>
                </asp:UpdatePanel>
                <!-- End Block User modal-dialog -->




            </div>
        </div>
        <style>
            .card {
                border: 1px solid #dadada;
            }
        </style>

        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openAddUserModal() {
                $('#addNewUserModal').modal({ show: true });
            }

            function openEditUserModal() {
                $('#editNewUserModal').modal({ show: true });
            }

            function openChangePasswordUserModal() {
                $('#changePasswordUserModal').modal({ show: true });
            }

            function openBlockUserModal() {
                $('#blockUserModal').modal({ show: true });
            }

        </script>

    </div>
</asp:Content>

