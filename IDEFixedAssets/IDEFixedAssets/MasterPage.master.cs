﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UserData u_data = new UserData();
        DbProcess db = new DbProcess();

        string setting_display_none = "";
        string[] setting_m = { "USERS", "WAREHOUSE", "ADDRESS" };
        string cc_url = Request.RawUrl.ToString().ToUpper();
        foreach (string _s_menu in setting_m)
        {
            if (cc_url.IndexOf(_s_menu) > -1)
            {
                setting_display_none = "style = \"display: block;\"";
            }
        }


        if (!IsPostBack)
        {
            if (Session["UserData"] == null) Config.Rd("/exit");
            u_data = (UserData)Session["UserData"];
            

            ltrHeaderPicName.Text =
                      String.Format("<a href=\"\" class=\"nav-link nav-link-profile\" data-toggle=\"dropdown\"/>" +
                       "<span class=\"logged-name\">{0}<span class=\"hidden-md-down\"> {1}</span></span>" +
                       "<img src=\"{2}\" class=\"wd-32 rounded-circle\" alt=\"\"/></a>",
                       Config.getKeyFromSplit(u_data.LoginUserName, ' ', 1),
                       Config.getKeyFromSplit(u_data.LoginUserName, ' ', 0),
                       u_data.LoginUserImage);


            //Left Menu Begin
            DataTable dtMenu = db.GetMenu(u_data.lang,u_data.LoginUserType);


            string[] main_menu_list = { "INDEX", "DOCUMENTS" };
            string[] setting_menu_list = { "USERS", "WAREHOUSE", "ADDRESS" };
            string active_menu = "INDEX", sub_active_menu = "", sub_current_menu = "";
            /*Active menu*/
            string page_url = Request.RawUrl.ToString().ToUpper();
            string current_menu = "";

            //main menu
            foreach (string _menu in main_menu_list)
            {
                if (page_url.IndexOf(_menu) > -1) active_menu = _menu;
            }

            //setting menu
            foreach (string _setting_menu in setting_menu_list)
            {
                if (page_url.IndexOf(_setting_menu) > -1)
                {
                    active_menu = "#1";
                    sub_active_menu = _setting_menu;
                }
            }
            /**/


            if (dtMenu != null)
            {
                IEnumerable<DataRow> queryHeadMenu =
                from headMenu in dtMenu.AsEnumerable()
                where headMenu.Field<Int16>("Parent_ID") == 0
                orderby headMenu.Field<Int16>("PRIORITY")
                select headMenu;

                DataTable dtHeadMenu = queryHeadMenu.CopyToDataTable();
                LtrMenu.Text = "";


                if (dtHeadMenu != null)
                {
                    foreach (DataRow drHeadMenu in dtHeadMenu.Rows)
                    {
                        DataTable dtSubMenu = new DataTable("dtSubMenu");
                        IEnumerable<DataRow> querySubMenu =
                           from subMenu in dtMenu.AsEnumerable()
                           where subMenu.Field<Int16>("Parent_ID") == Convert.ToInt32(drHeadMenu["ID"])
                           orderby subMenu.Field<Int16>("PRIORITY")
                           select subMenu;
                        if (querySubMenu.Any())
                        {
                            dtSubMenu = querySubMenu.CopyToDataTable<DataRow>();
                        }

                        string isSumMenuIcon = "";
                        if (dtSubMenu.Rows.Count > 0)
                        {
                            isSumMenuIcon = "<i class=\"menu-item-arrow fa fa-angle-down\"></i>";
                        }

                        current_menu = Convert.ToString(drHeadMenu["Url"]).Trim().Replace("/", "").ToUpper();

                        LtrMenu.Text += "<a href=\"" + Convert.ToString(drHeadMenu["Url"]).Trim() + "\" class=\"sl-menu-link" +
                            (current_menu == active_menu ? " active" : "") + "\">" +
                                "<div class=\"sl-menu-item\">" +
                                    "<i class=\"" + Convert.ToString(drHeadMenu["Icon"]).Trim() + "\"></i>" +
                                    "<span class=\"menu-item-label\">" + Convert.ToString(drHeadMenu["Name"]).Trim() + "</span>" + isSumMenuIcon +
                                "</div>" +
                            "</a>";

                        if (dtSubMenu.Rows.Count > 0)
                        {
                            LtrMenu.Text += "<ul class=\"sl-menu-sub nav flex-column\" " + setting_display_none + ">";
                            foreach (DataRow drSubMenu in dtSubMenu.Rows)
                            {
                                sub_current_menu = Convert.ToString(drSubMenu["Url"]).Trim().Replace("/", "").ToUpper();
                                LtrMenu.Text += "<li class=\"nav-item\">" +
                                "<a href=\"" + Convert.ToString(drSubMenu["Url"]).Trim() + "\" class=\"nav-link" + 
                                (sub_active_menu == sub_current_menu ?  " active" : "") + "\">" + Convert.ToString(drSubMenu["Name"]).Trim() + "</a></li>";
                            }
                            LtrMenu.Text += "</ul>";
                        }
                    }
                }
            }

            // Left Menu End

            /*Header data begin*/
            ltrChangePasswordLabel.Text = Config.getXmlValue(u_data.lang, "changePasswordLabel");
            ltrSignLogOutLabel.Text = Config.getXmlValue(u_data.lang, "signLogOutLabel");
            /*Header data end*/

        }


    }
}
