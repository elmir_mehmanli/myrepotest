﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Index_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script>
        function checkItemsGrid(ele) {
            var checkboxes = document.getElementsByTagName('input');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        if (checkboxes[i].id.indexOf('chkItemsGridId') != -1) {
                            checkboxes[i].checked = true;
                        }
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        if (checkboxes[i].id.indexOf('chkItemsGridId') != -1) {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
        }
    </script>



    <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

            .pagination-ys table > tbody > tr :hover {
                display: inline;
                background-color: rgb(233, 236, 239);
            }

            .pagination-ys table > tbody > tr > td > a,
            .pagination-ys table > tbody > tr > td > span {
                position: relative;
                float: left;
                padding: 8px 12px;
                line-height: 1.42857143;
                text-decoration: none;
                color: #5b93d3;
                background-color: #ffffff;
                border: 1px solid #dddddd;
                margin-left: -1px;
            }

            .pagination-ys table > tbody > tr > td > span {
                position: relative;
                float: left;
                padding: 8px 12px;
                line-height: 1.42857143;
                text-decoration: none;
                margin-left: -1px;
                z-index: 2;
                color: #aea79f;
                background-color: #f5f5f5;
                border-color: #dddddd;
                cursor: default;
            }

            .pagination-ys table > tbody > tr > td:first-child > a,
            .pagination-ys table > tbody > tr > td:first-child > span {
                margin-left: 0;
                border-bottom-left-radius: 4px;
                border-top-left-radius: 4px;
            }

            .pagination-ys table > tbody > tr > td:last-child > a,
            .pagination-ys table > tbody > tr > td:last-child > span {
                border-bottom-right-radius: 4px;
                border-top-right-radius: 4px;
            }

            .pagination-ys table > tbody > tr > td > a:hover,
            .pagination-ys table > tbody > tr > td > span:hover,
            .pagination-ys table > tbody > tr > td > a:focus,
            .pagination-ys table > tbody > tr > td > span:focus {
                color: #5b93d3;
                background-color: #eeeeee;
                border-color: #dddddd;
            }
    </style>



    <style>
        .dropdown-menu {
            min-width: 120%;
            margin: 0.125rem 0 0;
            font-size: 0.875rem;
            list-style: none;
            transform: translate3d(0px, 0px, 0px)!important;
        }

        .list-group-item {
            position: relative;
            display: block;
            padding: 5px 10px;
            background-color: #fff;
            border: 0px;
        }


            .list-group-item :hover {
                background-color: #dadada;
                padding: 4px 7px;
            }


        .popover-body {
            padding: 0px !important;
        }

        .table_td td {
            padding: 0.25rem !important;
            vertical-align: middle !important;
        }
    </style>




    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">IDE Fixed Assets</a>
            <span class="breadcrumb-item active">
                <asp:Literal ID="ltrPageHeaderAssetsLabel" runat="server"></asp:Literal>
            </span>
        </nav>

        <div class="sl-pagebody">
            <!-- SMALL MODAL -->
            <div id="alertmodal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                        <div class="modal-footer justify-content-right" style="padding: 5px">
                            <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
                <!-- modal-dialog -->
            </div>
            <!-- modal -->

            <div class="card pd-20 pd-sm-15">
                <asp:MultiView ID="MultiViewMain" runat="server" ActiveViewIndex="0">
                    <asp:View ID="ViewListAssets" runat="server">
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:LinkButton ID="lnkAddNewAssets" runat="server" class="btn btn-outline-info btn-block" OnClick="lnkAddNewAssets_Click">
                                    <i class="icon ion-plus-circled"></i>
                                    <asp:Literal ID="ltrAddNewAssetsLabel" runat="server"></asp:Literal>
                                </asp:LinkButton>
                                <asp:HiddenField ID="hdnOperationType" runat="server" />
                                <asp:HiddenField ID="hdnItemRecordId" runat="server" />
                            </div>
                              <div class="col-lg-2">
                           <asp:DropDownList ID="drlAssetsListStatus" OnSelectedIndexChanged ="drlAssetsListStatus_SelectedIndexChanged"  class="form-control" AutoPostBack ="true" runat="server" ></asp:DropDownList>
                        </div>
                        </div>
                        <br />
                        <br />

                        <div>
                            <div style="float: right;margin-bottom:7px">
                                <asp:DropDownList ID="drlGridItemPageSize" runat="server" AutoPostBack="True" CssClass="form-control" Width="100px" OnSelectedIndexChanged="drlGridItemPageSize_SelectedIndexChanged">
                                    <asp:ListItem Selected="True">10</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div style="clear: both"></div>
                        </div>

                        <div class="table-responsive">
                            <asp:GridView class="table display responsive nowrap dataTable no-footer dtr-inline" ID="grdItems" runat="server" DataKeyNames="RECORD_ID,STATUS"
                                AutoGenerateColumns="False" GridLines="None" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanging="grdItems_PageIndexChanging"
                                OnRowDataBound="grdItems_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <label class="ckbox">
                                                <input type="checkbox" id="chkItemsHeader" runat="server" onchange="checkItemsGrid(this)" />
                                                <span></span>
                                            </label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <label class="ckbox">
                                                <asp:CheckBox ID="chkItemsGridId" runat="server"></asp:CheckBox>
                                                <span></span>
                                            </label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CODE" />
                                    <asp:BoundField DataField="DESCRIPTION" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkGeneralWarehouseRemain" Text='<%#Eval("WAREHOUSE_REMAIN") %>' Style="cursor: pointer" data-html="true" data-container="body" data-toggle="popover" data-popover-color="default" data-placement="right"
                                                runat="server"
                                                class="btn btn-link">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UNIT_CODE" />
                                    <asp:BoundField DataField="MARK_DESCRIPTION" />
                                    <asp:BoundField DataField="GROUP_DESCRIPTION" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="dropdown">
                                                <asp:LinkButton ID="lnkActionAddressData"
                                                    runat="server"
                                                    class="btn btn-link" data-toggle="dropdown">
                                                             <i class="icon ion-more"></i></asp:LinkButton>
                                                <ul class="dropdown-menu">
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkItemsDataEdit" runat="server" OnClick="lnkItemsDataEdit_Click" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkItemsView" runat="server" OnClick="lnkItemsView_Click" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkDataHistory" runat="server" OnClick="lnkDataHistory_Click" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <asp:LinkButton ID="lnkItemsDelete" runat="server" OnClick="lnkItemsDelete_Click" CommandArgument='<%# Eval("RECORD_ID")  %>' />
                                                    </li>
                                                </ul>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="bg-info" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                        <asp:HiddenField ID="hdnGridItemsSize" runat="server" />




                        <!-- Begin View History modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="viewAssetsHistoryModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                    &nbsp&nbsp
                                                     <asp:Literal ID="ltrViewAsstets_HistoryModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row" style="width: 450px">
                                                        <div class="col-lg-8">
                                                            <asp:Literal ID="ltrViewAsstets_HistoryCreateUserLabel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:Literal ID="ltrViewAsstets_HistoryCreateUser" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="width: 450px">
                                                        <div class="col-lg-8">
                                                            <asp:Literal ID="ltrViewAsstets_HistoryCreateDateLabel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:Literal ID="ltrViewAsstets_HistoryCreateDate" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="width: 450px">
                                                        <div class="col-lg-8">
                                                            <asp:Literal ID="ltrViewAsstets_HistoryModifiedUserLabel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:Literal ID="ltrViewAsstets_HistoryModifiedUser" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="width: 450px">
                                                        <div class="col-lg-8">
                                                            <asp:Literal ID="ltrViewAsstets_HistoryModifiedDateLabel" runat="server"></asp:Literal>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:Literal ID="ltrViewAsstets_HistoryModifiedDate" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button runat="server" id="btnViewAsstets_HistoryCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- End View Historymodal-dialog -->



                        <!-- Begin Delete Item modal-dialog -->
                        <asp:UpdatePanel ID="UpdatePanel17" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="deleteItemModal" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-trash-a"></i>
                                                    <asp:Literal ID="ltrDeleteItemModalHeader" runat="server"></asp:Literal>
                                                </h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    &nbsp&nbsp
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <asp:HiddenField ID="hdnFieldItemRecordID" runat="server" />
                                            <div class="card pd-20 pd-sm-30">
                                                <div class="row mg-t-20">
                                                    <h6>
                                                        <asp:Literal ID="ltrDeleteItemModalBody" runat="server"></asp:Literal></h6>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="deleteItem_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnDeleteItem" class="btn btn-info pd-x-20" runat="server"
                                                    OnClick="btnDeleteItem_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                   document.getElementById('deleteItem_loading').style.display = '';
                                                   document.getElementById('ContentPlaceHolder1_btnDeleteItemCancel').style.display = 'none';
                                                   document.getElementById('alert_msg').style.display = 'none';" />
                                                <button runat="server" id="btnDeleteItemCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnDeleteItem" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- End Delete Item modal-dialog -->
                    </asp:View>
                    <asp:View ID="ViewAddAssets" runat="server">

                        <div class="d-flex align-items-center justify-content-end ht-md-60 bd pd-x-20 mg-t-10" style="border-bottom-width: 0px;">
                            <div class="d-md-flex pd-y-20 pd-md-y-0">
                                <img id="imgInsertUpdateItems_loading" style="display: none" src="../img/loader.gif" />
                                <asp:Button ID="btnInsertUpdateItem" OnClick="btnInsertUpdateItem_Click" class="btn btn-info" runat="server"
                                    OnClientClick="this.style.display = 'none';
                                                     document.getElementById('imgInsertUpdateItems_loading').style.display = '';" />
                                <asp:Button ID="btnInsertUpdateItemCancel" class="btn btn-secondary mg-md-l-10 mg-t-10 mg-md-t-0" runat="server" OnClick="btnInsertUpdateItemCancel_Click" />
                            </div>
                        </div>
                        <div class="card bd">
                            <div class="card-header bd-b">
                                <asp:Literal ID="ltrAddAssetsMainDataPanelHeader" runat="server"></asp:Literal>
                            </div>
                            <div class="card-body bg-white-200">
                                <div class="row">
                                    <div class="col-lg-3 form-group">
                                        <label>
                                            <asp:Literal ID="ltrAddAssetsCodeLabel" runat="server"></asp:Literal>
                                            <span class="tx-danger">*</span></label>
                                        <asp:TextBox ID="txtAddAssetsCode" MaxLength="25" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label>
                                            <asp:Literal ID="ltrAddAssetsDescriptionLabel" runat="server"></asp:Literal>
                                            <span class="tx-danger">*</span></label>
                                        <asp:TextBox ID="txtAddAssetsDescription" MaxLength="51" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>
                                            <asp:Literal ID="ltrAddAssetsStatusLabel" runat="server"></asp:Literal>
                                            <span class="tx-danger">*</span></label>
                                        <asp:DropDownList ID="drlAddAssetsStatus" runat="server" class="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row mg-t-20">
                                    <div class="col-lg-6 form-group">
                                        <label>
                                            <asp:Literal ID="ltrAddAssetsDescription2Label" runat="server"></asp:Literal>
                                            <span class="tx-danger"></span>
                                        </label>
                                        <asp:TextBox ID="txtAddAssetsDescription2" TextMode="MultiLine" Rows="3" MaxLength="201" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label>
                                            <asp:Literal ID="ltrAddAssetsNoteLabel" runat="server"></asp:Literal></label>
                                        <asp:TextBox ID="txtAddAssetsNote" TextMode="MultiLine" Rows="3" MaxLength="201" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card bd">
                            <div class="card-header bd-b">
                                <ul class="nav nav-tabs card-header-tabs">
                                    <li class="nav-item">
                                        <asp:LinkButton ID="lnkAddGeneralDataTab" OnClick="lnkAddGeneralDataTab_Click" CssClass="nav-link active" runat="server">
                                            <asp:Literal ID="ltrAddGeneralDataTab" runat="server"></asp:Literal>
                                        </asp:LinkButton>
                                    </li>
                                    <li class="nav-item">
                                        <asp:LinkButton ID="lnkAddUnitTab" OnClick="lnkAddUnitTab_Click" CssClass="nav-link" runat="server">
                                            <asp:Literal ID="ltrAddUnitTab" runat="server"></asp:Literal>
                                        </asp:LinkButton>
                                    </li>
                                    <li class="nav-item">
                                        <asp:LinkButton ID="lnkAdd_BarCode_RFID_QRTab" OnClick="lnkAdd_BarCode_RFID_QRTab_Click" CssClass="nav-link" runat="server">
                                            <asp:Literal ID="ltrAdd_BarCode_RFID_QRTab" runat="server"></asp:Literal>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body color-gray-lighter">
                                <asp:MultiView ID="MultiViewAddTabs" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="ViewAddGeneralTabl" runat="server">
                                        <div class="row mg-t-5">
                                            <div class="col-md-2">
                                                <div class="pd-10 bd">
                                                    <ul class="nav nav-pills flex-column" role="tablist">
                                                        <li class="nav-item">
                                                            <asp:LinkButton OnClick="lnkAddSubMarkTab_Click" CssClass="nav-link active" ID="lnkAddSubMarkTab" runat="server">
                                                                <asp:Literal ID="ltrAddSubMarkTab" runat="server"></asp:Literal>
                                                            </asp:LinkButton>
                                                        </li>
                                                        <li class="nav-item">
                                                            <asp:LinkButton OnClick="lnkAddSubGroupTab_Click" CssClass="nav-link" ID="lnkAddSubGroupTab" runat="server">
                                                                <asp:Literal ID="ltrAddSubGroupTab" runat="server"></asp:Literal>
                                                            </asp:LinkButton>
                                                        </li>
                                                        <li class="nav-item">
                                                            <asp:LinkButton OnClick="lnkAddSubSpeCodesTab_Click" CssClass="nav-link" ID="lnkAddSubSpeCodesTab" runat="server">
                                                                <asp:Literal ID="ltrAddSubSpeCodesTab" runat="server"></asp:Literal>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <asp:MultiView ID="MultiViewAddSubTabs" runat="server" ActiveViewIndex="0">
                                                    <asp:View ID="ViewMark" runat="server">

                                                        <div class="row bg-gray-200">
                                                            <div class="form-group col-lg-12">
                                                                <label>
                                                                    <asp:Literal ID="ltrSelectMarkLabel" runat="server"></asp:Literal>
                                                                    <span class="tx-danger"></span>
                                                                </label>
                                                                <asp:DropDownList ID="drlSelectMark" runat="server"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="row mg-t-20">
                                                            <div class="col-lg-2">
                                                                <asp:LinkButton ID="lnkAddMarkModal" runat="server" class="btn btn-outline-info btn-block"
                                                                    data-toggle="modal" data-target="#addAssets_AddMarkModal">
                                                                    <i class="icon ion-plus-circled"></i>
                                                                    <asp:Literal ID="ltrAddMarkModal" runat="server"></asp:Literal>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList ID="drlFillMarkByStatus"
                                                                    OnSelectedIndexChanged="drlFillMarkByStatus_SelectedIndexChanged"
                                                                    AutoPostBack="true" runat="server" class="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="grdAddAssets_AddMark" class="table table-white table-hover table_td" runat="server" AutoGenerateColumns="False"
                                                                DataKeyNames="RECORD_ID,STATUS" GridLines="None" EnableModelValidation="True" OnRowDataBound="grdAddAssets_AddMark_RowDataBound">
                                                                <Columns>
                                                                    <asp:BoundField DataField="CODE" />
                                                                    <asp:BoundField DataField="DESCRIPTION" />
                                                                    <asp:BoundField DataField="STATUS" />
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <div class="dropdown">
                                                                                <asp:LinkButton ID="lnkActionAddAssetsAddMarkData"
                                                                                    runat="server"
                                                                                    class="btn btn-link" data-toggle="dropdown">
                                                                                <i class="icon ion-more"></i></asp:LinkButton>
                                                                                <ul class="dropdown-menu">
                                                                                    <li class="list-group-item">
                                                                                        <asp:LinkButton ID="lnkAddAssets_AddMarkDataEdit" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' OnClick="lnkAddAssets_AddMarkDataEdit_Click" />
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <asp:LinkButton ID="lnkAddAssets_AddMarkChangeStatus" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' OnClick="lnkAddAssets_AddMarkChangeStatus_Click" />
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>

                                                        <!-- Begin New Mark modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="addAssets_AddMarkModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                                                    &nbsp&nbsp
                                                                                <asp:Literal ID="ltrAddAsstets_AddMarkModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pd-20">
                                                                                <asp:Literal ID="ltrAddAsstets_AddMarkMessage" runat="server"></asp:Literal>
                                                                                <div class="card pd-20 pd-sm-40">
                                                                                    <div class="row">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_AddMarkCode" Width="300px" MaxLength="25" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_AddMarkDescription" Width="300px" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:DropDownList ID="drlAddAsstets_AddMarkStatus"
                                                                                                class="form-control" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="AddAsstets_AddMark_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnAddAsstets_AddMark" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnAddAsstets_AddMark_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                                   document.getElementById('AddAsstets_AddMark_loading').style.display = '';
                                                                                                   document.getElementById('ContentPlaceHolder1_btnAddAsstets_AddMarkCancel').style.display = 'none';
                                                                                                   document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnAddAsstets_AddMarkCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnAddAsstets_AddMark" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End New Mark modal-dialog -->


                                                        <!-- Begin Edit Mark modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="addAssets_EditMarkModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                                                    &nbsp&nbsp
                                                                                <asp:Literal ID="ltrAddAsstets_EditMarkModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pd-20">
                                                                                <asp:HiddenField ID="hdnAddAsstets_EditMark" runat="server" />
                                                                                <asp:Literal ID="ltrAddAsstets_EditMarkMessage" runat="server"></asp:Literal>
                                                                                <div class="card pd-20 pd-sm-40">
                                                                                    <div class="row">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_EditMarkCode" Width="300px" MaxLength="25" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_EditMarkDescription" Width="300px" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:DropDownList ID="drlAddAsstets_EditMarkStatus"
                                                                                                class="form-control" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="AddAsstets_EditMark_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnAddAsstets_EditMark" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnAddAsstets_EditMark_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                                   document.getElementById('AddAsstets_EditMark_loading').style.display = '';
                                                                                                   document.getElementById('ContentPlaceHolder1_btnAddAsstets_EditMarkCancel').style.display = 'none';
                                                                                                   document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnAddAsstets_EditMarkCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnAddAsstets_EditMark" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End Edit Mark modal-dialog -->


                                                        <!-- Begin Change Status Mark modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="changeStatusMarkModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                                                    <asp:Literal ID="ltrChangeStatusMarkModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    &nbsp&nbsp
                                                                                 <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <asp:Literal ID="ltrChangeStatusMarkMessage" runat="server"></asp:Literal>
                                                                            <asp:HiddenField ID="hdnFieldChangeStatusMarkID" runat="server" />
                                                                            <asp:HiddenField ID="hdnFieldChangeStatusMarkStatus" runat="server" />
                                                                            <div class="card pd-20 pd-sm-30">
                                                                                <div class="row mg-t-20">
                                                                                    <h6>
                                                                                        <asp:Literal ID="ltrChangeStatusMarkModalBody" runat="server"></asp:Literal></h6>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="markChangeStatus_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnMarkChangeStatus" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnMarkChangeStatus_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                  document.getElementById('markChangeStatus_loading').style.display = '';
                                                                                  document.getElementById('ContentPlaceHolder1_btnMarkChangeStatusCancel').style.display = 'none';
                                                                                  document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnMarkChangeStatusCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnMarkChangeStatus" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End Block Mark modal-dialog -->


                                                    </asp:View>
                                                    <asp:View ID="ViewGroup" runat="server">



                                                        <div class="row bg-gray-200">
                                                            <div class="form-group col-lg-12">
                                                                <label>
                                                                    <asp:Literal ID="ltrSelectGroupLabel" runat="server"></asp:Literal>
                                                                    <span class="tx-danger"></span>
                                                                </label>
                                                                <asp:DropDownList ID="drlSelectGroup" runat="server"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="row mg-t-20">
                                                            <div class="col-lg-2">
                                                                <asp:LinkButton ID="lnkAddGroupModal" runat="server" class="btn btn-outline-info btn-block"
                                                                    data-toggle="modal" data-target="#addAssets_AddGroupModal">
                                                                    <i class="icon ion-plus-circled"></i>
                                                                    <asp:Literal ID="ltrAddGroupModal" runat="server"></asp:Literal>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList ID="drlFillGroupByStatus" OnSelectedIndexChanged="drlFillGroupByStatus_SelectedIndexChanged"
                                                                    AutoPostBack="true" runat="server" class="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="grdAddAssets_AddGroup" class="table table-white table-hover table_td" runat="server" AutoGenerateColumns="False"
                                                                DataKeyNames="RECORD_ID,STATUS" GridLines="None" EnableModelValidation="True" OnRowDataBound="grdAddAssets_AddGroup_RowDataBound">
                                                                <Columns>
                                                                    <asp:BoundField DataField="CODE" />
                                                                    <asp:BoundField DataField="DESCRIPTION" />
                                                                    <asp:BoundField DataField="STATUS" />
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <div class="dropdown">
                                                                                <asp:LinkButton ID="lnkActionAddAssetsAddGroupData"
                                                                                    runat="server"
                                                                                    class="btn btn-link" data-toggle="dropdown">
                                                                                <i class="icon ion-more"></i></asp:LinkButton>
                                                                                <ul class="dropdown-menu">
                                                                                    <li class="list-group-item">
                                                                                        <asp:LinkButton ID="lnkAddAssets_AddGroupDataEdit" OnClick="lnkAddAssets_AddGroupDataEdit_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <asp:LinkButton ID="lnkAddAssets_AddGroupChangeStatus" OnClick="lnkAddAssets_AddGroupChangeStatus_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>

                                                        <!-- Begin New Group modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="addAssets_AddGroupModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                                                    &nbsp&nbsp
                                                                                <asp:Literal ID="ltrAddAsstets_AddGroupModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pd-20">
                                                                                <asp:Literal ID="ltrAddAsstets_AddGroupMessage" runat="server"></asp:Literal>
                                                                                <div class="card pd-20 pd-sm-40">
                                                                                    <div class="row">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_AddGroupCode" Width="300px" MaxLength="25" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_AddGroupDescription" Width="300px" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:DropDownList ID="drlAddAsstets_AddGroupStatus"
                                                                                                class="form-control" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="AddAsstets_AddGroup_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnAddAsstets_AddGroup" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnAddAsstets_AddGroup_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                                   document.getElementById('AddAsstets_AddGroup_loading').style.display = '';
                                                                                                   document.getElementById('ContentPlaceHolder1_btnAddAsstets_AddGroupCancel').style.display = 'none';
                                                                                                   document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnAddAsstets_AddGroupCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnAddAsstets_AddGroup" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End New Group modal-dialog -->


                                                        <!-- Begin Edit Group modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="addAssets_EditGroupModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                                                    &nbsp&nbsp
                                                                                <asp:Literal ID="ltrAddAsstets_EditGroupModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pd-20">
                                                                                <asp:HiddenField ID="hdnAddAsstets_EditGroup" runat="server" />
                                                                                <asp:Literal ID="ltrAddAsstets_EditGroupMessage" runat="server"></asp:Literal>
                                                                                <div class="card pd-20 pd-sm-40">
                                                                                    <div class="row">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_EditGroupCode" Width="300px" MaxLength="25" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_EditGroupDescription" Width="300px" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:DropDownList ID="drlAddAsstets_EditGroupStatus"
                                                                                                class="form-control" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="AddAsstets_EditGroup_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnAddAsstets_EditGroup" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnAddAsstets_EditGroup_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                                   document.getElementById('AddAsstets_EditGroup_loading').style.display = '';
                                                                                                   document.getElementById('ContentPlaceHolder1_btnAddAsstets_EditGroupCancel').style.display = 'none';
                                                                                                   document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnAddAsstets_EditGroupCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnAddAsstets_EditGroup" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End Edit Group modal-dialog -->


                                                        <!-- Begin Change Status Group modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="changeStatusGroupModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                                                    <asp:Literal ID="ltrChangeStatusGroupModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    &nbsp&nbsp
                                                                                 <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <asp:Literal ID="ltrChangeStatusGroupMessage" runat="server"></asp:Literal>
                                                                            <asp:HiddenField ID="hdnFieldChangeStatusGroupID" runat="server" />
                                                                            <asp:HiddenField ID="hdnFieldChangeStatusGroupStatus" runat="server" />
                                                                            <div class="card pd-20 pd-sm-30">
                                                                                <div class="row mg-t-20">
                                                                                    <h6>
                                                                                        <asp:Literal ID="ltrChangeStatusGroupModalBody" runat="server"></asp:Literal></h6>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="groupChangeStatus_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnGroupChangeStatus" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnGroupChangeStatus_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                  document.getElementById('groupChangeStatus_loading').style.display = '';
                                                                                  document.getElementById('ContentPlaceHolder1_btnGroupChangeStatusCancel').style.display = 'none';
                                                                                  document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnGroupChangeStatusCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnGroupChangeStatus" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End Block Group modal-dialog -->


                                                    </asp:View>
                                                    <asp:View ID="ViewSpeCodes" runat="server">


                                                        <div class="row bg-gray-200">
                                                            <div class="form-group col-lg-2">
                                                                <label>
                                                                    <asp:Literal ID="ltrSelectSpecode1Label" runat="server"></asp:Literal>
                                                                    <span class="tx-danger"></span>
                                                                </label>
                                                                <asp:DropDownList ID="drlSelectSpecode1" runat="server"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="form-group col-lg-2">
                                                                <label>
                                                                    <asp:Literal ID="ltrSelectSpecode2Label" runat="server"></asp:Literal>
                                                                    <span class="tx-danger"></span>
                                                                </label>
                                                                <asp:DropDownList ID="drlSelectSpecode2" runat="server"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="form-group col-lg-2">
                                                                <label>
                                                                    <asp:Literal ID="ltrSelectSpecode3Label" runat="server"></asp:Literal>
                                                                    <span class="tx-danger"></span>
                                                                </label>
                                                                <asp:DropDownList ID="drlSelectSpecode3" runat="server"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="form-group col-lg-2">
                                                                <label>
                                                                    <asp:Literal ID="ltrSelectSpecode4Label" runat="server"></asp:Literal>
                                                                    <span class="tx-danger"></span>
                                                                </label>
                                                                <asp:DropDownList ID="drlSelectSpecode4" runat="server"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="form-group col-lg-2">
                                                                <label>
                                                                    <asp:Literal ID="ltrSelectSpecode5Label" runat="server"></asp:Literal>
                                                                    <span class="tx-danger"></span>
                                                                </label>
                                                                <asp:DropDownList ID="drlSelectSpecode5" runat="server"
                                                                    class="selectpicker form-control"
                                                                    data-live-search="true" data-hide-disabled="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="row mg-t-20">
                                                            <div class="col-lg-2">
                                                                <asp:LinkButton ID="lnkAddSpecodeModal" runat="server" class="btn btn-outline-info btn-block"
                                                                    data-toggle="modal" data-target="#addAssets_AddSpecodeModal">
                                                                    <i class="icon ion-plus-circled"></i>
                                                                    <asp:Literal ID="ltrAddSpecodeModal" runat="server"></asp:Literal>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList ID="drlFillSpecodeByStatus" OnSelectedIndexChanged="drlFillSpecodeByStatus_SelectedIndexChanged"
                                                                    AutoPostBack="true" runat="server" class="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="grdAddAssets_AddSpecode" class="table table-white table-hover table_td" runat="server" AutoGenerateColumns="False"
                                                                DataKeyNames="RECORD_ID,STATUS" GridLines="None" EnableModelValidation="True" OnRowDataBound="grdAddAssets_AddSpecode_RowDataBound">
                                                                <Columns>
                                                                    <asp:BoundField DataField="SPECODE" />
                                                                    <asp:BoundField DataField="DESCRIPTION" />
                                                                    <asp:BoundField DataField="SPECODE_DESCRIPTION" />
                                                                    <asp:BoundField DataField="STATUS" />
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <div class="dropdown">
                                                                                <asp:LinkButton ID="lnkActionAddAssetsAddSpecodeData"
                                                                                    runat="server"
                                                                                    class="btn btn-link" data-toggle="dropdown">
                                                                                <i class="icon ion-more"></i></asp:LinkButton>
                                                                                <ul class="dropdown-menu">
                                                                                    <li class="list-group-item">
                                                                                        <asp:LinkButton ID="lnkAddAssets_AddSpecodeDataEdit" OnClick="lnkAddAssets_AddSpecodeDataEdit_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <asp:LinkButton ID="lnkAddAssets_AddSpecodeChangeStatus" OnClick="lnkAddAssets_AddSpecodeChangeStatus_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>

                                                        <!-- Begin New Specode modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="addAssets_AddSpecodeModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                                                    &nbsp&nbsp
                                                                                    <asp:Literal ID="ltrAddAsstets_AddSpecodeModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pd-20">
                                                                                <asp:Literal ID="ltrAddAsstets_AddSpecodeMessage" runat="server"></asp:Literal>
                                                                                <div class="card pd-20 pd-sm-40">
                                                                                    <div class="row">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_AddSpecodeCode" Width="300px" MaxLength="25" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_AddSpecodeDescription" Width="300px" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:DropDownList ID="drlAddAsstets_AddSpecodeType"
                                                                                                class="form-control" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="AddAsstets_AddSpecode_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnAddAsstets_AddSpecode" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnAddAsstets_AddSpecode_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                      document.getElementById('AddAsstets_AddSpecode_loading').style.display = '';
                                                                                      document.getElementById('ContentPlaceHolder1_btnAddAsstets_AddSpecodeCancel').style.display = 'none';
                                                                                      document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnAddAsstets_AddSpecodeCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnAddAsstets_AddSpecode" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End New Specode modal-dialog -->


                                                        <!-- Begin Edit Specode modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="addAssets_EditSpecodeModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                                                    &nbsp&nbsp
                                                                                <asp:Literal ID="ltrAddAsstets_EditSpecodeModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pd-20">
                                                                                <asp:HiddenField ID="hdnAddAsstets_EditSpecode" runat="server" />
                                                                                <asp:Literal ID="ltrAddAsstets_EditSpecodeMessage" runat="server"></asp:Literal>
                                                                                <div class="card pd-20 pd-sm-40">
                                                                                    <div class="row">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_EditSpecodeCode" Width="300px" MaxLength="25" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:TextBox ID="txtAddAsstets_EditSpecodeDescription" Width="300px" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mg-t-20">
                                                                                        <div class="col-lg">
                                                                                            <asp:DropDownList ID="drlAddAsstets_EditSpecodeType"
                                                                                                class="form-control" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="AddAsstets_EditSpecode_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnAddAsstets_EditSpecode" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnAddAsstets_EditSpecode_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                            document.getElementById('AddAsstets_EditSpecode_loading').style.display = '';
                                                                                            document.getElementById('ContentPlaceHolder1_btnAddAsstets_EditSpecodeCancel').style.display = 'none';
                                                                                            document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnAddAsstets_EditSpecodeCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnAddAsstets_EditSpecode" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End Edit Specode modal-dialog -->


                                                        <!-- Begin Change Status Specode modal-dialog -->
                                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div id="changeStatusSpecodeModal" class="modal fade" data-backdrop="static">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content tx-size-sm">
                                                                            <div class="modal-header pd-x-20">
                                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                                                    <asp:Literal ID="ltrChangeStatusSpecodeModalHeader" runat="server"></asp:Literal>
                                                                                </h6>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    &nbsp&nbsp
                                                                                <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <asp:Literal ID="ltrChangeStatusSpecodeMessage" runat="server"></asp:Literal>
                                                                            <asp:HiddenField ID="hdnFieldChangeStatusSpecodeID" runat="server" />
                                                                            <asp:HiddenField ID="hdnFieldChangeStatusSpecodeStatus" runat="server" />
                                                                            <div class="card pd-20 pd-sm-30">
                                                                                <div class="row mg-t-20">
                                                                                    <h6>
                                                                                        <asp:Literal ID="ltrChangeStatusSpecodeModalBody" runat="server"></asp:Literal></h6>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <img id="specodeChangeStatus_loading" style="display: none" src="../img/loader.gif" />
                                                                                <asp:Button ID="btnSpecodeChangeStatus" class="btn btn-info pd-x-20" runat="server"
                                                                                    OnClick="btnSpecodeChangeStatus_Click"
                                                                                    OnClientClick="this.style.display = 'none';
                                                                                    document.getElementById('specodeChangeStatus_loading').style.display = '';
                                                                                    document.getElementById('ContentPlaceHolder1_btnSpecodeChangeStatusCancel').style.display = 'none';
                                                                                    document.getElementById('alert_msg').style.display = 'none';" />
                                                                                <button runat="server" id="btnSpecodeChangeStatusCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSpecodeChangeStatus" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <!-- End Block Specode modal-dialog -->


                                                    </asp:View>
                                                </asp:MultiView>
                                            </div>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="ViewAddUnitTab" runat="server">
                                        <div class="row mg-t-20">
                                            <div class="col-lg-2" runat="server" id="DivAddUnitModal">
                                                <asp:LinkButton ID="lnkAddUnitModal" runat="server" class="btn btn-outline-info btn-block"
                                                    data-toggle="modal" data-target="#addAssets_AddUnitModal">
                                                    <i class="icon ion-plus-circled"></i>
                                                    <asp:Literal ID="ltrAddUnitModal" runat="server"></asp:Literal>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="table-responsive">
                                            <asp:GridView ID="grdAddAssets_ItemUnit" class="table table-white table-hover" runat="server" AutoGenerateColumns="False"
                                                DataKeyNames="RECORD_ID,IS_MAIN_UNIT,COEFFICIENT,UNIT_CODE,ITEM_RECID,MAIN_UNIT_CODE,MAIN_UNIT_COEFFICINET" GridLines="None" EnableModelValidation="True"
                                                OnRowDataBound="grdAddAssets_ItemUnit_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="UNIT_CODE" />
                                                    <asp:BoundField DataField="COEFFICIENT" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Literal ID="ltrItemUnit_CalculatedCoeifficient" runat="server"></asp:Literal>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkAddAssetsChangeCoefficientData" OnClick="lnkAddAssetsChangeCoefficientData_Click" CommandArgument='<%# Eval("RECORD_ID") %>' runat="server">
                                                         <i class="icon ion-plus-circled"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkAddAssetsDeleteCoefficientData" OnClick="lnkAddAssetsDeleteCoefficientData_Click" CommandArgument='<%# Eval("RECORD_ID") %>' runat="server">
                                                         <i class="icon ion-trash-a"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>


                                        <!-- Begin UnitCode template modal-dialog -->
                                        <div id="addAssets_AddUnitModal" class="modal fade" data-backdrop="static">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content tx-size-sm">
                                                    <div class="modal-header pd-x-20">
                                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                            &nbsp&nbsp
                                                                <asp:Literal ID="ltrAddAsstets_AddUnitModalHeader" runat="server"></asp:Literal>
                                                        </h6>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body pd-20" style="max-height: 400px; overflow-y: auto">
                                                        <div class="card pd-20 pd-sm-40">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="grdAddAssets_UnitTeplate" class="table table-white table-hover" runat="server" AutoGenerateColumns="False"
                                                                    DataKeyNames="RECORD_ID" GridLines="None" EnableModelValidation="True"
                                                                    OnRowDataBound="grdAddAssets_UnitTeplate_RowDataBound">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="CODE" />
                                                                        <asp:BoundField DataField="UNIT_TYPE_DESCRIPTION" />
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton ID="lnkSelectUnitTemplate" OnClick="lnkSelectUnitTemplate_Click"
                                                                                            runat="server"
                                                                                            class="btn btn-link">
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkSelectUnitTemplate" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Right" Width="150px" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- End UnitCode template modal-dialog -->



                                        <!-- Begin Delete ItemUnit modal-dialog -->
                                        <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="deleteItemUnitModal" class="modal fade" data-backdrop="static">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content tx-size-sm">
                                                            <div class="modal-header pd-x-20">
                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-trash-a"></i>
                                                                    <asp:Literal ID="ltrDeleteItemUnitModalHeader" runat="server"></asp:Literal>
                                                                </h6>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    &nbsp&nbsp
                                                               <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <asp:HiddenField ID="hdnFieldDeleteItemUnitID" runat="server" />
                                                            <asp:HiddenField ID="hdnFieldDeleteItemUnitIsMainUnit" runat="server" />
                                                            <div class="card pd-20 pd-sm-30">
                                                                <div class="row mg-t-20">
                                                                    <h6>
                                                                        <asp:Literal ID="ltrDeleteItemUnitModalBody" runat="server"></asp:Literal></h6>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <img id="deleteItemUnit_loading" style="display: none" src="../img/loader.gif" />
                                                                <asp:Button ID="btnDeleteItemUnit" class="btn btn-info pd-x-20" runat="server"
                                                                    OnClick="btnDeleteItemUnit_Click"
                                                                    OnClientClick="this.style.display = 'none';
                                                                 document.getElementById('deleteItemUnit_loading').style.display = '';
                                                                 document.getElementById('ContentPlaceHolder1_btnDeleteItemUnitCancel').style.display = 'none';
                                                                 document.getElementById('alert_msg').style.display = 'none';" />
                                                                <button runat="server" id="btnDeleteItemUnitCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnDeleteItemUnit" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End Delete ItemUnit modal-dialog -->


                                        <!-- Begin Edit ItemUnit Coefficient modal-dialog -->
                                        <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="addAssets_EditItemUnitModal" class="modal fade" data-backdrop="static">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content tx-size-sm">
                                                            <div class="modal-header pd-x-20">
                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                                    &nbsp&nbsp
                                                                <asp:Literal ID="ltrAddAsstets_EditItemUnitModalHeader" runat="server"></asp:Literal>
                                                                </h6>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body pd-20">
                                                                <asp:Literal ID="ltrAddAsstets_EditItemUnitModalMessage" runat="server"></asp:Literal>
                                                                <asp:HiddenField ID="hdnFieldEditItemUnitID" runat="server" />
                                                                <asp:HiddenField ID="hdnFieldEditItemUnitIsMainUnit" runat="server" />
                                                                <asp:HiddenField ID="hdnFieldEditItemUnitMainCoefficient" runat="server" />
                                                                <div class="card pd-20 pd-sm-40">

                                                                    <div class="row">
                                                                        <div class="col-lg-4">
                                                                            <label class="form-control-label" style="font-weight: bold">
                                                                                <asp:Literal ID="ltrAddAsstets_EditItemUnit_Coefficient0" runat="server"></asp:Literal>
                                                                                = 
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-lg-5">
                                                                            <asp:TextBox ID="txtAddAsstets_EditItemUnit_Coefficient0" Style="text-align: right" MaxLength="9" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <label class="form-control-label" style="font-weight: bold">
                                                                                <asp:Literal ID="ltrAddAsstets_EditItemUnit_MainCoefficient0" runat="server"></asp:Literal>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mg-t-20" runat="server" id="secondDiv">
                                                                        <div class="col-lg-4">
                                                                            <label class="form-control-label" style="font-weight: bold">
                                                                                <asp:Literal ID="ltrAddAsstets_EditItemUnit_MainCoefficient1" runat="server"></asp:Literal>
                                                                                = 
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-lg-5">
                                                                            <asp:TextBox ID="txtAddAsstets_EditItemUnit_MainCoefficient" Style="text-align: right" MaxLength="9" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <label class="form-control-label" style="font-weight: bold">
                                                                                <asp:Literal ID="ltrAddAsstets_EditItemUnit_Coefficient1" runat="server"></asp:Literal>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <img id="AddAsstets_EditItemUnit_loading" style="display: none" src="../img/loader.gif" />
                                                                <asp:LinkButton ID="lnkCalculateItemUnit" OnClick="lnkCalculateItemUnit_Click" Width="40px" CssClass="btn btn-outline-info btn-block" runat="server"><i class="fa fa-refresh"></i></asp:LinkButton>
                                                                <asp:Button ID="btnAddAsstets_EditItemUnit" class="btn btn-info pd-x-20" runat="server"
                                                                    OnClick="btnAddAsstets_EditItemUnit_Click"
                                                                    OnClientClick="this.style.display = 'none';
                                                                                   document.getElementById('AddAsstets_EditItemUnit_loading').style.display = '';
                                                                                   document.getElementById('ContentPlaceHolder1_btnAddAsstets_EditItemUnitCancel').style.display = 'none';
                                                                                   document.getElementById('alert_msg').style.display = 'none';" />
                                                                <button runat="server" id="btnAddAsstets_EditItemUnitCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnAddAsstets_EditItemUnit" />
                                                <asp:PostBackTrigger ControlID="lnkCalculateItemUnit" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End Edit ItemUnit Coefficient modal-dialog -->


                                    </asp:View>
                                    <asp:View ID="ViewAdd_BarCode_RFID_QRTab" runat="server">

                                        <div class="row mg-t-20">
                                            <div class="col-lg-2" runat="server" id="DivAddBarcodeModal">
                                                <asp:LinkButton ID="lnkAddBarcodeModal" runat="server" class="btn btn-outline-info btn-block"
                                                    data-toggle="modal" data-target="#addAssets_AddBarcodeModal">
                                                    <i class="icon ion-plus-circled"></i>
                                                    <asp:Literal ID="ltrAddBarcodeModal" runat="server"></asp:Literal>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="table-responsive">
                                            <asp:GridView ID="grdAddAssets_ItemBarcode" class="table table-white table-hover" runat="server" AutoGenerateColumns="False"
                                                DataKeyNames="RECORD_ID,CODE_TYPE" GridLines="None" EnableModelValidation="True" OnRowDataBound="grdAddAssets_ItemBarcode_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="CODE" />
                                                    <asp:BoundField DataField="DESCRIPTION" />
                                                    <asp:BoundField DataField="CODE_TYPE_DESCRIPTION" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <div class="dropdown">
                                                                <asp:LinkButton ID="lnkActionAddAssetsAddBarcodeData" Height="20px"
                                                                    runat="server"
                                                                    class="btn btn-link" data-toggle="dropdown">
                                                                <i class="icon ion-more"></i></asp:LinkButton>
                                                                <ul class="dropdown-menu">
                                                                    <li class="list-group-item">
                                                                        <asp:LinkButton ID="lnkAddAssets_AddBarcodeDataEdit" OnClick="lnkAddAssets_AddBarcodeDataEdit_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                                    </li>
                                                                    <li class="list-group-item">
                                                                        <asp:LinkButton ID="lnkAddAssets_AddBarcodeDataDelete" OnClick="lnkAddAssets_AddBarcodeDataDelete_Click" runat="server" CommandArgument='<%# Eval("RECORD_ID") %>' />
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>



                                        <!-- Begin New Barcode modal-dialog -->
                                        <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="addAssets_AddBarcodeModal" class="modal fade" data-backdrop="static">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content tx-size-sm">
                                                            <div class="modal-header pd-x-20">
                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>
                                                                    &nbsp&nbsp
                                                                    <asp:Literal ID="ltrAddAsstets_AddBarcodeModalHeader" runat="server"></asp:Literal>
                                                                </h6>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body pd-20">
                                                                <asp:Literal ID="ltrAddAsstets_AddBarcodeMessage" runat="server"></asp:Literal>
                                                                <div class="card pd-20 pd-sm-40">
                                                                    <div class="row">
                                                                        <div class="col-lg">
                                                                            <asp:TextBox ID="txtAddAsstets_AddBarcodeCode" Width="300px" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row mg-t-20">
                                                                        <div class="col-lg">
                                                                            <asp:TextBox ID="txtAddAsstets_AddBarcodeDescription" Width="300px" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row mg-t-20">
                                                                        <div class="col-lg">
                                                                            <asp:DropDownList ID="drlAddAsstets_AddBarcodeType"
                                                                                class="form-control" runat="server">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <img id="AddAsstets_AddBarcode_loading" style="display: none" src="../img/loader.gif" />
                                                                <asp:Button ID="btnAddAsstets_AddBarcode" class="btn btn-info pd-x-20" runat="server"
                                                                    OnClick="btnAddAsstets_AddBarcode_Click"
                                                                    OnClientClick="this.style.display = 'none';
                                                                    document.getElementById('AddAsstets_AddBarcode_loading').style.display = '';
                                                                    document.getElementById('ContentPlaceHolder1_btnAddAsstets_AddBarcodeCancel').style.display = 'none';
                                                                    document.getElementById('alert_msg').style.display = 'none';" />
                                                                <button runat="server" id="btnAddAsstets_AddBarcodeCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnAddAsstets_AddBarcode" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End New Barcode modal-dialog -->


                                        <!-- Begin Edit Barcode modal-dialog -->
                                        <asp:UpdatePanel ID="UpdatePanel14" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="addAssets_EditBarcodeModal" class="modal fade" data-backdrop="static">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content tx-size-sm">
                                                            <div class="modal-header pd-x-20">
                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>
                                                                    &nbsp&nbsp
                                                                 <asp:Literal ID="ltrAddAsstets_EditBarcodeModalHeader" runat="server"></asp:Literal>
                                                                </h6>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body pd-20">
                                                                <asp:Literal ID="ltrAddAsstets_EditBarcodeMessage" runat="server"></asp:Literal>
                                                                <asp:HiddenField ID="hdnAddAsstets_EditBarcodeRecId" runat="server" />
                                                                <div class="card pd-20 pd-sm-40">
                                                                    <div class="row">
                                                                        <div class="col-lg">
                                                                            <asp:TextBox ID="txtAddAsstets_EditBarcodeCode" Width="300px" MaxLength="50" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row mg-t-20">
                                                                        <div class="col-lg">
                                                                            <asp:TextBox ID="txtAddAsstets_EditBarcodeDescription" Width="300px" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row mg-t-20">
                                                                        <div class="col-lg">
                                                                            <asp:DropDownList ID="drlAddAsstets_EditBarcodeType"
                                                                                class="form-control" runat="server">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <img id="AddAsstets_EditBarcode_loading" style="display: none" src="../img/loader.gif" />
                                                                <asp:Button ID="btnAddAsstets_EditBarcode" class="btn btn-info pd-x-20" runat="server"
                                                                    OnClick="btnAddAsstets_EditBarcode_Click"
                                                                    OnClientClick="this.style.display = 'none';
                                                                                  document.getElementById('AddAsstets_EditBarcode_loading').style.display = '';
                                                                                  document.getElementById('ContentPlaceHolder1_btnAddAsstets_EditBarcodeCancel').style.display = 'none';
                                                                                  document.getElementById('alert_msg').style.display = 'none';" />
                                                                <button runat="server" id="btnAddAsstets_EditBarcodeCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnAddAsstets_EditBarcode" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End Edit Barcode modal-dialog -->




                                        <!-- Begin Delete Item barcode modal-dialog -->
                                        <asp:UpdatePanel ID="UpdatePanel15" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="deleteItemBarcodeModal" class="modal fade" data-backdrop="static">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content tx-size-sm">
                                                            <div class="modal-header pd-x-20">
                                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-trash-a"></i>
                                                                    <asp:Literal ID="ltrDeleteItemBarcodeModalHeader" runat="server"></asp:Literal>
                                                                </h6>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    &nbsp&nbsp
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <asp:HiddenField ID="hdnFieldDeleteItemBarcodeRecordID" runat="server" />
                                                            <div class="card pd-20 pd-sm-30">
                                                                <div class="row mg-t-20">
                                                                    <h6>
                                                                        <asp:Literal ID="ltrDeleteItemBarcodeModalBody" runat="server"></asp:Literal></h6>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <img id="deleteItemBarcode_loading" style="display: none" src="../img/loader.gif" />
                                                                <asp:Button ID="btnDeleteItemBarcode" class="btn btn-info pd-x-20" runat="server"
                                                                    OnClick="btnDeleteItemBarcode_Click"
                                                                    OnClientClick="this.style.display = 'none';
                                                                                  document.getElementById('deleteItemBarcode_loading').style.display = '';
                                                                                  document.getElementById('ContentPlaceHolder1_btnDeleteItemBarcodeCancel').style.display = 'none';
                                                                                  document.getElementById('alert_msg').style.display = 'none';" />
                                                                <button runat="server" id="btnDeleteItemBarcodeCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnDeleteItemBarcode" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <!-- End Delete Item barcode modal-dialog -->



                                    </asp:View>
                                </asp:MultiView>
                            </div>
                        </div>


                    </asp:View>

                </asp:MultiView>

            </div>

        </div>




        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openAddAssets_AddMarkModal() {
                $('#addAssets_AddMarkModal').modal({ show: true });
            }

            function openAddAssets_EditMarkModal() {
                $('#addAssets_EditMarkModal').modal({ show: true });
            }

            function openAddAssets_changeStatusMarkModal() {
                $('#changeStatusMarkModal').modal({ show: true });
            }


            function openAddAssets_AddGroupModal() {
                $('#addAssets_AddGroupModal').modal({ show: true });
            }

            function openAddAssets_EditGroupModal() {
                $('#addAssets_EditGroupModal').modal({ show: true });
            }

            function openAddAssets_changeStatusGroupModal() {
                $('#changeStatusGroupModal').modal({ show: true });
            }


            function openAddAssets_AddSpecodeModal() {
                $('#addAssets_AddSpecodeModal').modal({ show: true });
            }

            function openAddAssets_EditSpecodeModal() {
                $('#addAssets_EditSpecodeModal').modal({ show: true });
            }

            function openAddAssets_changeStatusSpecodeModal() {
                $('#changeStatusSpecodeModal').modal({ show: true });
            }

            function openAddAssets_deleteItemUnitModal() {
                $('#deleteItemUnitModal').modal({ show: true });
            }

            function openAddAssets_EditItemUnitModal() {
                $('#addAssets_EditItemUnitModal').modal({ show: true });
            }

            function openAddAssets_AddItemBarcodeModal() {
                $('#addAssets_AddBarcodeModal').modal({ show: true });
            }

            function openAddAssets_EditItemBarcodeModal() {
                $('#addAssets_EditBarcodeModal').modal({ show: true });
            }

            function openAddAssets_DeleteItemBarcodeModal() {
                $('#deleteItemBarcodeModal').modal({ show: true });
            }

            function openViewAssets_HistoryModal() {
                $('#viewAssetsHistoryModal').modal({ show: true });
            }

            function openDeleteAssets_DeleteItemModal() {
                $('#deleteItemModal').modal({ show: true });
            }


        </script>

    </div>
</asp:Content>

