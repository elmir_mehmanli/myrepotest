﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Index_Default : System.Web.UI.Page
{
    UserData u_data = new UserData();
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserData"] == null) Config.Rd("/exit");
        u_data = (UserData)Session["UserData"];
        if (!IsPostBack)
        {
            db.DeletePendingItem((short)u_data.LogonUserID);
            FillPageData(u_data);
            FillMark(drlFillMarkByStatus.SelectedValue);
            FillGroup(drlFillGroupByStatus.SelectedValue);
            FillSpecode(drlFillSpecodeByStatus.SelectedValue);

            /*Items*/
            if (Session["drlGridItemPageSize"] == null) Session["drlGridItemPageSize"] = "10";
            drlGridItemPageSize.SelectedValue = Convert.ToString(Session["drlGridItemPageSize"]);
            
            grdItems.VirtualItemCount = db.GetItemCount(drlAssetsListStatus.SelectedValue);
            grdItems.PageIndex = Session["GridItemPageIndex"] == null ? 0 : Convert.ToInt32(Session["GridItemPageIndex"]);
            FillItems(Session["GridItemPageIndex"] == null ? 0 : Convert.ToInt32(Session["GridItemPageIndex"]));           
            drlGridItemPageSize.Visible = grdItems.Rows.Count > 0;
           
            /**/
        }

       
    }


    void FillItems(int PageIndex)
    {
        /*Fill Item Grid*/
        grdItems.PageSize = Convert.ToInt32(Convert.ToString(Session["drlGridItemPageSize"]));
        grdItems.DataSource = db.GetItemList(drlAssetsListStatus.SelectedValue, PageIndex, Convert.ToInt32(Session["drlGridItemPageSize"]));
        grdItems.DataBind();
       
        foreach (GridViewRow row in grdItems.Rows)
        {
            LinkButton lnkItemsDataEdit = row.FindControl("lnkItemsDataEdit") as LinkButton;
            lnkItemsDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkItemsView = row.FindControl("lnkItemsView") as LinkButton;
            lnkItemsView.Text = Config.getXmlValue(u_data.lang, "lnkGridItemEditLabel");

            LinkButton lnkDataHistory = row.FindControl("lnkDataHistory") as LinkButton;
            lnkDataHistory.Text = Config.getXmlValue(u_data.lang, "lnkGridItemHistoryLabel");

            LinkButton lnkItemsDelete = row.FindControl("lnkItemsDelete") as LinkButton;
            lnkItemsDelete.Text = Config.getXmlValue(u_data.lang, "dataBtnDeleteLabel");

            string itemStatus = Convert.ToString(grdItems.DataKeys[row.RowIndex].Values["STATUS"]);
            lnkItemsDelete.Enabled = (itemStatus == "DEACTIVE");


            LinkButton lnkGeneralWarehouseRemain = row.FindControl("lnkGeneralWarehouseRemain") as LinkButton;
            lnkGeneralWarehouseRemain.Enabled = false; 

            int itemRecordId = Convert.ToInt32(grdItems.DataKeys[row.RowIndex].Values["RECORD_ID"]);

            string table_temp = @"<table class = ""table table-white"" style = ""margin-bottom:0 !important"">
                                  {0}
                                </table>";

           DataTable dtWRemain = db.GetWareHouseRemains(itemRecordId);
           string _tdata = "";
           if (dtWRemain.Rows.Count > 0)
           {
               foreach (DataRow drWRemain  in dtWRemain.Rows)
               {
                   _tdata += "<tr><td>" + drWRemain["WAREHOUSE_NAME"] + "</td><td>" + decimal.Parse(Convert.ToString(drWRemain["QUANTITY"])).ToString("G29") + "</td></tr>";
               }
               lnkGeneralWarehouseRemain.Attributes.Add("data-content", string.Format(table_temp, _tdata));
           }
  

        }
    }
    

  
    void FillMark(string status)
    {
        drlSelectMark.Items.Clear();
        drlSelectMark.DataTextField = "DESCRIPTION";
        drlSelectMark.DataValueField = "RECORD_ID";
        drlSelectMark.DataSource = db.GetMarkList("ACTIVE");
        drlSelectMark.DataBind();
        drlSelectMark.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusMarkaSelectLabel"), ""));
        ltrSelectMarkLabel.Text = Config.getXmlValue(u_data.lang, "statusMarkaSelectLabel");

        grdAddAssets_AddMark.DataSource = db.GetMarkList(status);
        grdAddAssets_AddMark.DataBind();
        grdAddAssets_AddMark.UseAccessibleHeader = true;
        if (grdAddAssets_AddMark.Rows.Count > 0)
        {
            grdAddAssets_AddMark.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        foreach (GridViewRow row in grdAddAssets_AddMark.Rows)
        {
            LinkButton lnkAddAssets_AddMarkDataEdit = row.FindControl("lnkAddAssets_AddMarkDataEdit") as LinkButton;
            lnkAddAssets_AddMarkDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkAddAssets_AddMarkChangeStatus = row.FindControl("lnkAddAssets_AddMarkChangeStatus") as LinkButton;
            lnkAddAssets_AddMarkChangeStatus.Text = Config.getXmlValue(u_data.lang, row.Cells[2].Text == "ACTIVE" ? "dataBtnDeaktivLabel" : "dataBtnAktivLabel");

            lnkAddAssets_AddMarkDataEdit.Visible = row.Cells[2].Text == "ACTIVE";


        }
    }

    void FillGroup(string status)
    {
        drlSelectGroup.Items.Clear();
        drlSelectGroup.DataTextField = "DESCRIPTION";
        drlSelectGroup.DataValueField = "RECORD_ID";
        drlSelectGroup.DataSource = db.GetGroupList("ACTIVE");
        drlSelectGroup.DataBind();
        drlSelectGroup.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusGroupSelectLabel"), ""));
        ltrSelectGroupLabel.Text = Config.getXmlValue(u_data.lang, "statusGroupSelectLabel");

        grdAddAssets_AddGroup.DataSource = db.GetGroupList(status);
        grdAddAssets_AddGroup.DataBind();
        grdAddAssets_AddGroup.UseAccessibleHeader = true;
        if (grdAddAssets_AddGroup.Rows.Count > 0)
        {
            grdAddAssets_AddGroup.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        foreach (GridViewRow row in grdAddAssets_AddGroup.Rows)
        {
            LinkButton lnkAddAssets_AddGroupDataEdit = row.FindControl("lnkAddAssets_AddGroupDataEdit") as LinkButton;
            lnkAddAssets_AddGroupDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkAddAssets_AddGroupChangeStatus = row.FindControl("lnkAddAssets_AddGroupChangeStatus") as LinkButton;
            lnkAddAssets_AddGroupChangeStatus.Text = Config.getXmlValue(u_data.lang, row.Cells[2].Text == "ACTIVE" ? "dataBtnDeaktivLabel" : "dataBtnAktivLabel");

            lnkAddAssets_AddGroupDataEdit.Visible = row.Cells[2].Text == "ACTIVE";
        }
    }

    private void FillPageData(UserData u_data)
    {
        drlAssetsListStatus.Items.Clear();
        drlAssetsListStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "drlAsstetStatusActive"), "ACTIVE"));
        drlAssetsListStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "drlAsstetStatusDeActive"), "DEACTIVE"));

        ltrPageHeaderAssetsLabel.Text = Config.getXmlValue(u_data.lang, "pageHeaderAssetsLabel");

        ltrAddNewAssetsLabel.Text = Config.getXmlValue(u_data.lang, "lnkAddNewAssetsLabel");
        ltrAddAssetsMainDataPanelHeader.Text = Config.getXmlValue(u_data.lang, "detailsAssetsMainDataPanelHeaderLabel");
        txtAddAssetsCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "assetsCodeLabel"));
        ltrAddAssetsCodeLabel.Text = Config.getXmlValue(u_data.lang, "assetsCodeLabel");
        txtAddAssetsDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "assetsDescriptionLabel"));
        ltrAddAssetsDescriptionLabel.Text = Config.getXmlValue(u_data.lang, "assetsDescriptionLabel");
        txtAddAssetsDescription2.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "assetsDescription2Label"));
        ltrAddAssetsDescription2Label.Text = Config.getXmlValue(u_data.lang, "assetsDescription2Label");
        txtAddAssetsNote.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "assetsNoteLabel"));
        ltrAddAssetsNoteLabel.Text = Config.getXmlValue(u_data.lang, "assetsNoteLabel");

        drlAddAssetsStatus.Items.Clear();
        drlAddAssetsStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusValueActiveLabel"), "ACTIVE"));
        drlAddAssetsStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "statusValueDeActiveLabel"), "DEACTIVE"));
        ltrAddAssetsStatusLabel.Text = Config.getXmlValue(u_data.lang, "statusSelectLabel");

        ltrAddGeneralDataTab.Text = Config.getXmlValue(u_data.lang, "lnkGeneralDataTabLabel");
        ltrAddUnitTab.Text =   Config.getXmlValue(u_data.lang, "lnkUnitsTabLabel");
        ltrAdd_BarCode_RFID_QRTab.Text = Config.getXmlValue(u_data.lang, "lnkBarCodeRfidQrTabLabel");

        ltrAddSubMarkTab.Text = Config.getXmlValue(u_data.lang, "lnkAddSubMarkTabLabel");
        lnkAddSubGroupTab.Text = Config.getXmlValue(u_data.lang, "lnkAddSubGroupTabLabel");
        lnkAddSubSpeCodesTab.Text = Config.getXmlValue(u_data.lang, "lnkAddSubSpeCodesTabLabel");     

        #region Add Mark

        drlSelectMark.Items.Clear();
        drlSelectMark.DataTextField = "DESCRIPTION";
        drlSelectMark.DataValueField = "RECORD_ID";
        drlSelectMark.DataSource = db.GetMarkList("ACTIVE");
        drlSelectMark.DataBind();
        drlSelectMark.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusMarkaSelectLabel"), ""));
        ltrSelectMarkLabel.Text = Config.getXmlValue(u_data.lang, "statusMarkaSelectLabel");

        lnkAddMarkModal.Text = Config.getXmlValue(u_data.lang, "lnkAddMarkModalLabel");
        ltrAddAsstets_AddMarkModalHeader.Text = Config.getXmlValue(u_data.lang, "lnkAddMarkModalHeaderLabel");
        
        txtAddAsstets_AddMarkCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddMarkCodeLabel"));
        txtAddAsstets_AddMarkDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddMarkDescriptionLabel"));

        drlFillMarkByStatus.Items.Clear();
        drlFillMarkByStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusMarkValueActiveLabel"), "ACTIVE"));
        drlFillMarkByStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "statusMarkValueDeActiveLabel"), "DEACTIVE"));

        drlAddAsstets_AddMarkStatus.Items.Clear();
        drlAddAsstets_AddMarkStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusValueActiveLabel"), "ACTIVE"));
        drlAddAsstets_AddMarkStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "statusValueDeActiveLabel"), "DEACTIVE"));

        btnAddAsstets_AddMark.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnAddAsstets_AddMarkCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
      
        #endregion

        #region Edit Mark
        ltrAddAsstets_EditMarkModalHeader.Text = Config.getXmlValue(u_data.lang, "lnkEditMarkModalHeaderLabel");
        txtAddAsstets_EditMarkCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddMarkCodeLabel"));
        txtAddAsstets_EditMarkDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddMarkDescriptionLabel"));
        drlAddAsstets_EditMarkStatus.Items.Clear();
        drlAddAsstets_EditMarkStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusValueActiveLabel"), "ACTIVE"));
        drlAddAsstets_EditMarkStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "statusValueDeActiveLabel"), "DEACTIVE"));

        btnAddAsstets_EditMark.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnAddAsstets_EditMarkCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        #endregion

        #region ChangeStatus Mark
        ltrChangeStatusMarkModalHeader.Text = Config.getXmlValue(u_data.lang, "changeStatusAssetsMarkModalHeaderLabel");
        btnMarkChangeStatus.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnMarkChangeStatusCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");
        ltrChangeStatusMarkModalBody.Text = Config.getXmlValue(u_data.lang, "changeStatusAsstestMarkModalBodyLabel");
        #endregion


        #region Add Group

        drlSelectGroup.Items.Clear();
        drlSelectGroup.DataTextField = "DESCRIPTION";
        drlSelectGroup.DataValueField = "RECORD_ID";
        drlSelectGroup.DataSource = db.GetGroupList("ACTIVE");
        drlSelectGroup.DataBind();
        drlSelectGroup.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusGroupSelectLabel"), ""));
        ltrSelectGroupLabel.Text = Config.getXmlValue(u_data.lang, "statusGroupSelectLabel");


        lnkAddGroupModal.Text = Config.getXmlValue(u_data.lang, "lnkAddGroupModalLabel");
        ltrAddAsstets_AddGroupModalHeader.Text = Config.getXmlValue(u_data.lang, "lnkAddGroupModalHeaderLabel");

        txtAddAsstets_AddGroupCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddGroupCodeLabel"));
        txtAddAsstets_AddGroupDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddGroupDescriptionLabel"));

        drlFillGroupByStatus.Items.Clear();
        drlFillGroupByStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusGroupValueActiveLabel"), "ACTIVE"));
        drlFillGroupByStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "statusGroupValueDeActiveLabel"), "DEACTIVE"));

        drlAddAsstets_AddGroupStatus.Items.Clear();
        drlAddAsstets_AddGroupStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusValueActiveLabel"), "ACTIVE"));
        drlAddAsstets_AddGroupStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "statusValueDeActiveLabel"), "DEACTIVE"));

        btnAddAsstets_AddGroup.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnAddAsstets_AddGroupCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");

        #endregion

        #region Edit Group
        ltrAddAsstets_EditGroupModalHeader.Text = Config.getXmlValue(u_data.lang, "lnkEditGroupModalHeaderLabel");
        txtAddAsstets_EditGroupCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddGroupCodeLabel"));
        txtAddAsstets_EditGroupDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddGroupDescriptionLabel"));
        drlAddAsstets_EditGroupStatus.Items.Clear();
        drlAddAsstets_EditGroupStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusValueActiveLabel"), "ACTIVE"));
        drlAddAsstets_EditGroupStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "statusValueDeActiveLabel"), "DEACTIVE"));

        btnAddAsstets_EditGroup.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnAddAsstets_EditGroupCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        #endregion

        #region ChangeStatus Group
        ltrChangeStatusGroupModalHeader.Text = Config.getXmlValue(u_data.lang, "changeStatusAssetsGroupModalHeaderLabel");
        btnGroupChangeStatus.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnGroupChangeStatusCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");
        ltrChangeStatusGroupModalBody.Text = Config.getXmlValue(u_data.lang, "changeStatusAsstestGroupModalBodyLabel");
        #endregion


        #region Add Specode

        fillSpecodeDropdown();

        lnkAddSpecodeModal.Text = Config.getXmlValue(u_data.lang, "lnkAddSpecodeModalLabel");
        ltrAddAsstets_AddSpecodeModalHeader.Text = Config.getXmlValue(u_data.lang, "lnkAddSpecodeModalHeaderLabel");

        txtAddAsstets_AddSpecodeCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddSpecodeCodeLabel"));
        txtAddAsstets_AddSpecodeDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddSpecodeDescriptionLabel"));

        drlAddAsstets_AddSpecodeType.Items.Clear();
        drlAddAsstets_AddSpecodeType.DataTextField = "DESCRIPTION";
        drlAddAsstets_AddSpecodeType.DataValueField = "RECORD_ID";
        drlAddAsstets_AddSpecodeType.DataSource = db.GetSpecodeTemplateList();
        drlAddAsstets_AddSpecodeType.DataBind();
        drlAddAsstets_AddSpecodeType.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));


        drlFillSpecodeByStatus.Items.Clear();
        drlFillSpecodeByStatus.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "statusValueActiveLabel"), "ACTIVE"));
        drlFillSpecodeByStatus.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "statusValueDeActiveLabel"), "DEACTIVE"));

        btnAddAsstets_AddSpecode.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnAddAsstets_AddSpecodeCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");

        #endregion

        #region Edit Specode
        ltrAddAsstets_EditSpecodeModalHeader.Text = Config.getXmlValue(u_data.lang, "lnkEditSpecodeModalHeaderLabel");
        txtAddAsstets_EditSpecodeCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddSpecodeCodeLabel"));
        txtAddAsstets_EditSpecodeDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAssetsAddSpecodeDescriptionLabel"));

        drlAddAsstets_EditSpecodeType.Items.Clear();
        drlAddAsstets_EditSpecodeType.DataTextField = "DESCRIPTION";
        drlAddAsstets_EditSpecodeType.DataValueField = "RECORD_ID";
        drlAddAsstets_EditSpecodeType.DataSource = db.GetSpecodeTemplateList();
        drlAddAsstets_EditSpecodeType.DataBind();
        drlAddAsstets_EditSpecodeType.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));


        btnAddAsstets_EditSpecode.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnAddAsstets_EditSpecodeCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        #endregion

        #region ChangeStatus Specode
        ltrChangeStatusSpecodeModalHeader.Text = Config.getXmlValue(u_data.lang, "changeStatusAssetsSpecodeModalHeaderLabel");
        btnSpecodeChangeStatus.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnSpecodeChangeStatusCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");
        ltrChangeStatusSpecodeModalBody.Text = Config.getXmlValue(u_data.lang, "changeStatusAsstestSpecodeModalBodyLabel");
        #endregion


        #region AddUnit
        ltrAddUnitModal.Text = Config.getXmlValue(u_data.lang, "lnkAddUnitModalLabel");
        ltrAddAsstets_AddUnitModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrAddAsstets_AddUnitModalHeaderLabel");
        #endregion

        #region AddBarcode
        ltrAddBarcodeModal.Text = Config.getXmlValue(u_data.lang, "lnkAddBarcodeModalLabel");
        ltrAddAsstets_AddBarcodeModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrAddAsstets_AddBarcodeModalHeaderLabel");
        txtAddAsstets_AddBarcodeCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAddAsstets_AddBarcodeCodeLabel"));
        txtAddAsstets_AddBarcodeDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAddAsstets_AddBarcodeDescriptionLabel"));
        drlAddAsstets_AddBarcodeType.Items.Clear();
        drlAddAsstets_AddBarcodeType.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));
        drlAddAsstets_AddBarcodeType.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "barCodeType1"), "0"));
        drlAddAsstets_AddBarcodeType.Items.Insert(2, new ListItem(Config.getXmlValue(u_data.lang, "barCodeType2"), "1"));
        drlAddAsstets_AddBarcodeType.Items.Insert(3, new ListItem(Config.getXmlValue(u_data.lang, "barCodeType3"), "2"));
        btnAddAsstets_AddBarcode.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnAddAsstets_AddBarcodeCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        #endregion

        #region EditBarcode
        ltrAddAsstets_EditBarcodeModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrAddAsstets_EditBarcodeModalHeaderLabel");
        txtAddAsstets_EditBarcodeCode.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAddAsstets_AddBarcodeCodeLabel"));
        txtAddAsstets_EditBarcodeDescription.Attributes.Add("placeholder", Config.getXmlValue(u_data.lang, "txtAddAsstets_AddBarcodeDescriptionLabel"));
        drlAddAsstets_EditBarcodeType.Items.Clear();
        drlAddAsstets_EditBarcodeType.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));
        drlAddAsstets_EditBarcodeType.Items.Insert(1, new ListItem(Config.getXmlValue(u_data.lang, "barCodeType1"), "0"));
        drlAddAsstets_EditBarcodeType.Items.Insert(2, new ListItem(Config.getXmlValue(u_data.lang, "barCodeType2"), "1"));
        drlAddAsstets_EditBarcodeType.Items.Insert(3, new ListItem(Config.getXmlValue(u_data.lang, "barCodeType3"), "2"));
        btnAddAsstets_EditBarcode.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnAddAsstets_EditBarcodeCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        #endregion

        #region DeleteBarcode
        ltrDeleteItemBarcodeModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrAddAsstets_DeleteBarcodeModalHeaderLabel");
        ltrDeleteItemBarcodeModalBody.Text = Config.getXmlValue(u_data.lang, "ltrDeleteItemBarcodeModalBodyMessage");
        btnDeleteItemBarcode.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnDeleteItemBarcodeCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");
        #endregion

        #region Add Items Update Items
       
        #endregion


    }

    //add new item
    protected void lnkAddNewAssets_Click(object sender, EventArgs e)
    {
        hdnOperationType.Value = "INSERT";
        string item_code_temp = DateTime.Now.ToString("ddMMyyyyHHmmss") + "_" +u_data.LogonUserID;
        int getItemId = db.GetItemId(item_code_temp, (short)u_data.LogonUserID);
        if (getItemId < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        hdnItemRecordId.Value = getItemId.ToString();
        btnInsertUpdateItem.Text = Config.getXmlValue(u_data.lang, "btnAddTextLabel");
        btnInsertUpdateItemCancel.Text = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        MultiViewMain.ActiveViewIndex = 1;
        PageComponetEnable();
    }
    
    protected void lnkAddGeneralDataTab_Click(object sender, EventArgs e)
    {
        lnkAddGeneralDataTab.CssClass = "nav-link active";
        lnkAddUnitTab.CssClass = "nav-link";
        lnkAdd_BarCode_RFID_QRTab.CssClass = "nav-link";
        MultiViewAddTabs.ActiveViewIndex = 0;
        SaveTabData();
    }



    void SaveTabData()
    {
        if (hdnOperationType.Value == "VIEW")
        {
            DataRow drItem = db.GetItemByRecordId(Convert.ToInt32(hdnItemRecordId.Value));
            if (drItem == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
                return;
            }


            if (drlSelectMark.SelectedValue == "") drlSelectMark.SelectedValue = Convert.ToString(drItem["MARK_RECID"]);
            if (drlSelectGroup.SelectedValue == "") drlSelectGroup.SelectedValue = Convert.ToString(drItem["GROUP_RECID"]);
            if (drlSelectSpecode1.SelectedValue == "") drlSelectSpecode1.SelectedValue = Convert.ToString(drItem["SPECODE1_RECID"]);
            if (drlSelectSpecode2.SelectedValue == "") drlSelectSpecode2.SelectedValue = Convert.ToString(drItem["SPECODE2_RECID"]);
            if (drlSelectSpecode3.SelectedValue == "") drlSelectSpecode3.SelectedValue = Convert.ToString(drItem["SPECODE3_RECID"]);
            if (drlSelectSpecode4.SelectedValue == "") drlSelectSpecode4.SelectedValue = Convert.ToString(drItem["SPECODE4_RECID"]);
            if (drlSelectSpecode5.SelectedValue == "") drlSelectSpecode5.SelectedValue = Convert.ToString(drItem["SPECODE5_RECID"]);
        }
    }

    
    /*Begin Sub Tabs*/
    protected void lnkAddSubMarkTab_Click(object sender, EventArgs e)
    {
        lnkAddSubMarkTab.CssClass = "nav-link active";//elmirr
        lnkAddSubGroupTab.CssClass = "nav-link";
        lnkAddSubSpeCodesTab.CssClass = "nav-link";
        MultiViewAddSubTabs.ActiveViewIndex = 0;
        SaveTabData();
    }
    protected void lnkAddSubGroupTab_Click(object sender, EventArgs e)
    {
        lnkAddSubMarkTab.CssClass = "nav-link";
        lnkAddSubGroupTab.CssClass = "nav-link active";
        lnkAddSubSpeCodesTab.CssClass = "nav-link";
        MultiViewAddSubTabs.ActiveViewIndex = 1;
        SaveTabData();
    }
    protected void lnkAddSubSpeCodesTab_Click(object sender, EventArgs e)
    {
        lnkAddSubMarkTab.CssClass = "nav-link";
        lnkAddSubGroupTab.CssClass = "nav-link";
        lnkAddSubSpeCodesTab.CssClass = "nav-link active";
        MultiViewAddSubTabs.ActiveViewIndex = 2;
        SaveTabData();
    }
    /*End Sub Tabs*/
   

    /*Add New Mark*/
    protected void btnAddAsstets_AddMark_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_AddMarkMessage.Text = "";

        if (txtAddAsstets_AddMarkCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddMarkModal();", true);
            ltrAddAsstets_AddMarkMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddMarkCodeEmptyMessage"));
            return;
        }

        if (txtAddAsstets_AddMarkDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddMarkModal();", true);
            ltrAddAsstets_AddMarkMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddMarkDescriptionEmptyMessage"));
            return;
        }

        string _result = db.AddMark(txtAddAsstets_AddMarkCode.Text.Trim(), txtAddAsstets_AddMarkDescription.Text.Trim(), drlAddAsstets_AddMarkStatus.SelectedValue);

        if(_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAssetsAddMarkSuccessMessage"));
        }
        else if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddMarkModal();", true);
            ltrAddAsstets_AddMarkMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addAssetsAddMarkExistsMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        txtAddAsstets_AddMarkCode.Text = "";
        txtAddAsstets_AddMarkDescription.Text = "";
        drlAddAsstets_AddMarkStatus.SelectedValue = "ACTIVE";
        FillMark(drlAddAsstets_AddMarkStatus.SelectedValue);

    }
    protected void grdAddAssets_AddMark_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddMarkCodeLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddMarkDescriptionLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddMarkStatusLabel");
        }
    }
   
    protected void drlFillMarkByStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillMark(drlFillMarkByStatus.SelectedValue);
    }

    /*Edit Mark*/
    protected void lnkAddAssets_AddMarkDataEdit_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditMarkMessage.Text = "";
        LinkButton lnkAddAssets_AddMarkDataEdit = (LinkButton)sender;
        hdnAddAsstets_EditMark.Value = lnkAddAssets_AddMarkDataEdit.CommandArgument;
        FillMarkDataById(Convert.ToInt32(hdnAddAsstets_EditMark.Value));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditMarkModal();", true);
    }

    private void FillMarkDataById(int record_id)
    {
        DataRow drEditMark = db.GetMarkById(record_id);
        if (drEditMark != null)
        {
            txtAddAsstets_EditMarkCode.Text = Convert.ToString(drEditMark["CODE"]);
            txtAddAsstets_EditMarkDescription.Text = Convert.ToString(drEditMark["DESCRIPTION"]);
        }
    }
   
    protected void btnAddAsstets_EditMark_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditMarkMessage.Text = "";
        int edited_recor_id = Convert.ToInt32(hdnAddAsstets_EditMark.Value);

        if (txtAddAsstets_EditMarkCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditMarkModal();", true);
            ltrAddAsstets_EditMarkMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddMarkCodeEmptyMessage"));
            return;
        }

        if (txtAddAsstets_EditMarkDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditMarkModal();", true);
            ltrAddAsstets_EditMarkMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddMarkDescriptionEmptyMessage"));
            return;
        }

        string _result = db.UpdateMark(edited_recor_id,txtAddAsstets_EditMarkCode.Text.Trim(), txtAddAsstets_EditMarkDescription.Text.Trim(), drlAddAsstets_EditMarkStatus.SelectedValue);

        if (_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAssetsEditMarkSuccessMessage"));
        }
        else if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditMarkModal();", true);
            ltrAddAsstets_EditMarkMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addAssetsAddMarkExistsMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        txtAddAsstets_EditMarkCode.Text = "";
        txtAddAsstets_EditMarkDescription.Text = "";
        drlAddAsstets_EditMarkStatus.SelectedValue = "ACTIVE";
        FillMark(drlFillMarkByStatus.SelectedValue);
    }
    
    //change status Mark
    protected void lnkAddAssets_AddMarkChangeStatus_Click(object sender, EventArgs e)
    {
        ltrChangeStatusMarkMessage.Text = "";
        LinkButton lnkAddAssets_AddMarkChangeStatus = (LinkButton)sender;
        hdnFieldChangeStatusMarkID.Value = lnkAddAssets_AddMarkChangeStatus.CommandArgument;
        int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
        string status = Convert.ToString(grdAddAssets_AddMark.DataKeys[rowIndex].Values["STATUS"]);
        hdnFieldChangeStatusMarkStatus.Value = status;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_changeStatusMarkModal();", true);
    }
    protected void btnMarkChangeStatus_Click(object sender, EventArgs e)
    {
        ltrChangeStatusMarkMessage.Text = "";
        string result = db.ChangeStatusMark(Convert.ToInt32(hdnFieldChangeStatusMarkID.Value),
            hdnFieldChangeStatusMarkStatus.Value == "ACTIVE" ? "DEACTIVE" : "ACTIVE");

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "changeStatusAssetsMarkSuccessMessage"));
        }
        else if (result.Equals("BUSY_MARK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changeStatusAssetsMarkBusyMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillMark(drlFillMarkByStatus.SelectedValue);
    }
   
    
    
    //add Group
    protected void btnAddAsstets_AddGroup_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_AddGroupMessage.Text = "";

        if (txtAddAsstets_AddGroupCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddGroupModal();", true);
            ltrAddAsstets_AddGroupMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddGroupCodeEmptyMessage"));
            return;
        }

        if (txtAddAsstets_AddGroupDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddGroupModal();", true);
            ltrAddAsstets_AddGroupMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddGroupDescriptionEmptyMessage"));
            return;
        }

        string _result = db.AddGroup(txtAddAsstets_AddGroupCode.Text.Trim(), txtAddAsstets_AddGroupDescription.Text.Trim(), drlAddAsstets_AddGroupStatus.SelectedValue);

        if (_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAssetsAddGroupSuccessMessage"));
        }
        else if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddGroupModal();", true);
            ltrAddAsstets_AddGroupMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addAssetsAddGroupExistsMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        txtAddAsstets_AddGroupCode.Text = "";
        txtAddAsstets_AddGroupDescription.Text = "";
        drlAddAsstets_AddGroupStatus.SelectedValue = "ACTIVE";
        FillGroup(drlAddAsstets_AddGroupStatus.SelectedValue);
    }
    protected void drlFillGroupByStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillGroup(drlFillGroupByStatus.SelectedValue);
    }
    
    protected void grdAddAssets_AddGroup_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddGroupCodeLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddGroupDescriptionLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddGroupStatusLabel");
        }
    }
    
    /*Edit Group*/
    protected void lnkAddAssets_AddGroupDataEdit_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditGroupMessage.Text = "";
        LinkButton lnkAddAssets_AddGroupDataEdit = (LinkButton)sender;
        hdnAddAsstets_EditGroup.Value = lnkAddAssets_AddGroupDataEdit.CommandArgument;
        FillGroupDataById(Convert.ToInt32(hdnAddAsstets_EditGroup.Value));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditGroupModal();", true);
    }

    private void FillGroupDataById(int record_id)
    {
        DataRow drEditGroup = db.GetGroupById(record_id);
        if (drEditGroup != null)
        {
            txtAddAsstets_EditGroupCode.Text = Convert.ToString(drEditGroup["CODE"]);
            txtAddAsstets_EditGroupDescription.Text = Convert.ToString(drEditGroup["DESCRIPTION"]);
        }
    }

   
    protected void btnAddAsstets_EditGroup_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditGroupMessage.Text = "";
        int edited_recor_id = Convert.ToInt32(hdnAddAsstets_EditGroup.Value);

        if (txtAddAsstets_EditGroupCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditGroupModal();", true);
            ltrAddAsstets_EditGroupMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddGroupCodeEmptyMessage"));
            return;
        }

        if (txtAddAsstets_EditGroupDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditGroupModal();", true);
            ltrAddAsstets_EditGroupMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddGroupDescriptionEmptyMessage"));
            return;
        }

        string _result = db.UpdateGroup(edited_recor_id, txtAddAsstets_EditGroupCode.Text.Trim(), txtAddAsstets_EditGroupDescription.Text.Trim(), drlAddAsstets_EditGroupStatus.SelectedValue);

        if (_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAssetsEditGroupSuccessMessage"));
        }
        else if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditGroupModal();", true);
            ltrAddAsstets_EditGroupMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addAssetsAddGroupExistsMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        txtAddAsstets_EditGroupCode.Text = "";
        txtAddAsstets_EditGroupDescription.Text = "";
        drlAddAsstets_EditGroupStatus.SelectedValue = "ACTIVE";
        FillGroup(drlFillGroupByStatus.SelectedValue);
    }

    /*Change group status*/
    protected void lnkAddAssets_AddGroupChangeStatus_Click(object sender, EventArgs e)
    {
        ltrChangeStatusGroupMessage.Text = "";
        LinkButton lnkAddAssets_AddGroupChangeStatus = (LinkButton)sender;
        hdnFieldChangeStatusGroupID.Value = lnkAddAssets_AddGroupChangeStatus.CommandArgument;
        int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
        string status = Convert.ToString(grdAddAssets_AddGroup.DataKeys[rowIndex].Values["STATUS"]);
        hdnFieldChangeStatusGroupStatus.Value = status;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_changeStatusGroupModal();", true);
    }
    protected void btnGroupChangeStatus_Click(object sender, EventArgs e)
    {
        ltrChangeStatusGroupMessage.Text = "";
        string result = db.ChangeStatusGroup(Convert.ToInt32(hdnFieldChangeStatusGroupID.Value),
            hdnFieldChangeStatusGroupStatus.Value == "ACTIVE" ? "DEACTIVE" : "ACTIVE");

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "changeStatusAssetsGroupSuccessMessage"));
        }
        else if (result.Equals("BUSY_GROUP"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changeStatusAssetsGroupBusyMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillGroup(drlFillGroupByStatus.SelectedValue);
    }



    //add Specode
    protected void btnAddAsstets_AddSpecode_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_AddSpecodeMessage.Text = "";

        if (txtAddAsstets_AddSpecodeCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddSpecodeModal();", true);
            ltrAddAsstets_AddSpecodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddSpecodeCodeEmptyMessage"));
            return;
        }

        if (txtAddAsstets_AddSpecodeDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddSpecodeModal();", true);
            ltrAddAsstets_AddSpecodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddSpecodeDescriptionEmptyMessage"));
            return;
        }

        if (drlAddAsstets_AddSpecodeType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddSpecodeModal();", true);
            ltrAddAsstets_AddSpecodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "drlAssetsAddSpecodeSpecodeTypeEmptyMessage"));
            return;
        }


        string _result = db.AddSpecode(txtAddAsstets_AddSpecodeCode.Text.Trim(), txtAddAsstets_AddSpecodeDescription.Text.Trim(), drlAddAsstets_AddSpecodeType.SelectedValue);

        if (_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAssetsAddSpecodeSuccessMessage"));
        }
        else if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddSpecodeModal();", true);
            ltrAddAsstets_AddSpecodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addAssetsAddSpecodeExistsMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        txtAddAsstets_AddSpecodeCode.Text = "";
        txtAddAsstets_AddSpecodeDescription.Text = "";
        drlAddAsstets_AddSpecodeType.SelectedValue = "";
        FillSpecode(drlFillSpecodeByStatus.SelectedValue);
    }


    void fillSpecodeDropdown()
    {
        drlSelectSpecode1.Items.Clear();
        drlSelectSpecode1.DataTextField = "DESCRIPTION";
        drlSelectSpecode1.DataValueField = "RECORD_ID";
        drlSelectSpecode1.DataSource = db.GetSpecodeListByType(0);
        drlSelectSpecode1.DataBind();
        drlSelectSpecode1.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));
        ltrSelectSpecode1Label.Text = Config.getXmlValue(u_data.lang, "specode1Value");

        drlSelectSpecode2.Items.Clear();
        drlSelectSpecode2.DataTextField = "DESCRIPTION";
        drlSelectSpecode2.DataValueField = "RECORD_ID";
        drlSelectSpecode2.DataSource = db.GetSpecodeListByType(1);
        drlSelectSpecode2.DataBind();
        drlSelectSpecode2.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));
        ltrSelectSpecode2Label.Text = Config.getXmlValue(u_data.lang, "specode2Value");

        drlSelectSpecode3.Items.Clear();
        drlSelectSpecode3.DataTextField = "DESCRIPTION";
        drlSelectSpecode3.DataValueField = "RECORD_ID";
        drlSelectSpecode3.DataSource = db.GetSpecodeListByType(2);
        drlSelectSpecode3.DataBind();
        drlSelectSpecode3.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));
        ltrSelectSpecode3Label.Text = Config.getXmlValue(u_data.lang, "specode3Value");

        drlSelectSpecode4.Items.Clear();
        drlSelectSpecode4.DataTextField = "DESCRIPTION";
        drlSelectSpecode4.DataValueField = "RECORD_ID";
        drlSelectSpecode4.DataSource = db.GetSpecodeListByType(3);
        drlSelectSpecode4.DataBind();
        drlSelectSpecode4.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));
        ltrSelectSpecode4Label.Text = Config.getXmlValue(u_data.lang, "specode4Value");

        drlSelectSpecode5.Items.Clear();
        drlSelectSpecode5.DataTextField = "DESCRIPTION";
        drlSelectSpecode5.DataValueField = "RECORD_ID";
        drlSelectSpecode5.DataSource = db.GetSpecodeListByType(4);
        drlSelectSpecode5.DataBind();
        drlSelectSpecode5.Items.Insert(0, new ListItem(Config.getXmlValue(u_data.lang, "selectValueLabel"), ""));
        ltrSelectSpecode5Label.Text = Config.getXmlValue(u_data.lang, "specode5Value");
    }

    void FillSpecode(string status)
    {
        fillSpecodeDropdown();
        grdAddAssets_AddSpecode.DataSource = db.GetSpecodeList(status);
        grdAddAssets_AddSpecode.DataBind();
        grdAddAssets_AddSpecode.UseAccessibleHeader = true;
        if (grdAddAssets_AddSpecode.Rows.Count > 0)
        {
            grdAddAssets_AddSpecode.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        foreach (GridViewRow row in grdAddAssets_AddSpecode.Rows)
        {
            LinkButton lnkAddAssets_AddSpecodeDataEdit = row.FindControl("lnkAddAssets_AddSpecodeDataEdit") as LinkButton;
            lnkAddAssets_AddSpecodeDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkAddAssets_AddSpecodeChangeStatus = row.FindControl("lnkAddAssets_AddSpecodeChangeStatus") as LinkButton;
            lnkAddAssets_AddSpecodeChangeStatus.Text = Config.getXmlValue(u_data.lang, row.Cells[3].Text == "ACTIVE" ? "dataBtnDeaktivLabel" : "dataBtnAktivLabel");

            lnkAddAssets_AddSpecodeDataEdit.Visible = row.Cells[3].Text == "ACTIVE";


        }
    }


    protected void grdAddAssets_AddSpecode_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddSpecodeCodeLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddSpecodeDescriptionLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddSpecodeTypeLabel");
            e.Row.Cells[3].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_AddSpecodeStatusLabel");
        }
    }


    //Edit Specode
    protected void lnkAddAssets_AddSpecodeDataEdit_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditSpecodeMessage.Text = "";
        LinkButton lnkAddAssets_AddSpecodeDataEdit = (LinkButton)sender;
        hdnAddAsstets_EditSpecode.Value = lnkAddAssets_AddSpecodeDataEdit.CommandArgument;
        FillSpecodeDataById(Convert.ToInt32(hdnAddAsstets_EditSpecode.Value));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditSpecodeModal();", true);
    }

    private void FillSpecodeDataById(int record_id)
    {
        DataRow drEditSpecode = db.GetSpecodeById(record_id);
        if (drEditSpecode != null)
        {
            txtAddAsstets_EditSpecodeCode.Text = Convert.ToString(drEditSpecode["SPECODE"]);
            txtAddAsstets_EditSpecodeDescription.Text = Convert.ToString(drEditSpecode["DESCRIPTION"]);
            drlAddAsstets_EditSpecodeType.SelectedValue = Convert.ToString(drEditSpecode["SPECODE_TYPE"]);
        }
    }


    protected void btnAddAsstets_EditSpecode_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditSpecodeMessage.Text = "";
        int edited_recor_id = Convert.ToInt32(hdnAddAsstets_EditSpecode.Value);

        if (txtAddAsstets_EditSpecodeCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditSpecodeModal();", true);
            ltrAddAsstets_EditSpecodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddSpecodeCodeEmptyMessage"));
            return;
        }

        if (txtAddAsstets_EditSpecodeDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditSpecodeModal();", true);
            ltrAddAsstets_EditSpecodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAssetsAddSpecodeDescriptionEmptyMessage"));
            return;
        }

        if (drlAddAsstets_EditSpecodeType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditSpecodeModal();", true);
            ltrAddAsstets_EditSpecodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "drlAssetsAddSpecodeSpecodeTypeEmptyMessage"));
            return;
        }

        string _result = db.UpdateSpecode(edited_recor_id, txtAddAsstets_EditSpecodeCode.Text.Trim(), txtAddAsstets_EditSpecodeDescription.Text.Trim(), drlAddAsstets_EditSpecodeType.SelectedValue);

        if (_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "addAssetsEditSpecodeSuccessMessage"));
        }
        else if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditSpecodeModal();", true);
            ltrAddAsstets_EditSpecodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addAssetsAddSpecodeExistsMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        txtAddAsstets_EditSpecodeCode.Text = "";
        txtAddAsstets_EditSpecodeDescription.Text = "";
        drlAddAsstets_EditSpecodeType.SelectedValue = "";
        FillSpecode(drlFillSpecodeByStatus.SelectedValue);
    }

    
    
    //Chabge status Specode
    protected void lnkAddAssets_AddSpecodeChangeStatus_Click(object sender, EventArgs e)
    {
        ltrChangeStatusSpecodeMessage.Text = "";
        LinkButton lnkAddAssets_AddSpecodeChangeStatus = (LinkButton)sender;
        hdnFieldChangeStatusSpecodeID.Value = lnkAddAssets_AddSpecodeChangeStatus.CommandArgument;
        int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
        string status = Convert.ToString(grdAddAssets_AddSpecode.DataKeys[rowIndex].Values["STATUS"]);
        hdnFieldChangeStatusSpecodeStatus.Value = status;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_changeStatusSpecodeModal();", true);
    }
    protected void btnSpecodeChangeStatus_Click(object sender, EventArgs e)
    {
        ltrChangeStatusSpecodeMessage.Text = "";
        string result = db.ChangeStatusSpecode(Convert.ToInt32(hdnFieldChangeStatusSpecodeID.Value),
            hdnFieldChangeStatusSpecodeStatus.Value == "ACTIVE" ? "DEACTIVE" : "ACTIVE");

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "changeStatusAssetsSpecodeSuccessMessage"));
        }
        else if (result.Equals("BUSY_SPECODE"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "changeStatusAssetsSpecodeBusyMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
        }
        FillSpecode(drlFillSpecodeByStatus.SelectedValue);
    }
   
    
    protected void drlFillSpecodeByStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSpecode(drlFillSpecodeByStatus.SelectedValue);
    }





    //units
    protected void lnkAddUnitTab_Click(object sender, EventArgs e)
    {
        lnkAddGeneralDataTab.CssClass = "nav-link";
        lnkAddUnitTab.CssClass = "nav-link active";
        lnkAdd_BarCode_RFID_QRTab.CssClass = "nav-link";

        FillUnitTemplate();
        FillItemUnit();
        ltrDeleteItemUnitModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrDeleteItemUnitModalHeaderLabel");
        ltrAddAsstets_EditItemUnitModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrEditItemUnitModalHeaderLabel");
        MultiViewAddTabs.ActiveViewIndex = 1;


    }

    //fill unit template
    void FillUnitTemplate()
    {

        grdAddAssets_UnitTeplate.DataSource = db.GetUnitTeplate();
        grdAddAssets_UnitTeplate.DataBind();
        grdAddAssets_UnitTeplate.UseAccessibleHeader = true;
        if (grdAddAssets_UnitTeplate.Rows.Count > 0)
        {
            grdAddAssets_UnitTeplate.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        foreach (GridViewRow row in grdAddAssets_UnitTeplate.Rows)
        {
            LinkButton lnkSelectUnitTemplate = row.FindControl("lnkSelectUnitTemplate") as LinkButton;
            lnkSelectUnitTemplate.Text = Config.getXmlValue(u_data.lang, "selectValueLabel");

        }
    }
    
    //units template grid
    protected void grdAddAssets_UnitTeplate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_UnitTeplateCodeLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_UnitTeplateUnitTypeDescriptionLabel");
        }
    }

    //fill item Unit
    void FillItemUnit()
    {
        int item_record_id = Convert.ToInt32(hdnItemRecordId.Value);
        grdAddAssets_ItemUnit.DataSource = db.GetUnitByItemId(item_record_id);
        grdAddAssets_ItemUnit.DataBind();
        grdAddAssets_ItemUnit.UseAccessibleHeader = true;
        if (grdAddAssets_ItemUnit.Rows.Count > 0)
        {
            grdAddAssets_ItemUnit.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        foreach (GridViewRow row in grdAddAssets_ItemUnit.Rows)
        {
            string UNIT_CODE = Convert.ToString(grdAddAssets_ItemUnit.DataKeys[row.RowIndex].Values["UNIT_CODE"]);
            string MAIN_UNIT_CODE = Convert.ToString(grdAddAssets_ItemUnit.DataKeys[row.RowIndex].Values["MAIN_UNIT_CODE"]);
            float COEFFICIENT = float.Parse(grdAddAssets_ItemUnit.DataKeys[row.RowIndex].Values["COEFFICIENT"].ToString());
            float MAIN_UNIT_COEFFICINET = float.Parse(grdAddAssets_ItemUnit.DataKeys[row.RowIndex].Values["MAIN_UNIT_COEFFICINET"].ToString());

            string _text = "";
            if (UNIT_CODE != MAIN_UNIT_CODE)
            {
                if (COEFFICIENT != 0)
                {
                    _text = "<span class=\"tx-10\">" + UNIT_CODE + " = " + COEFFICIENT.ToString("0.000") + " " + MAIN_UNIT_CODE +
                    "<br/>" + MAIN_UNIT_CODE + " = " + (MAIN_UNIT_COEFFICINET / COEFFICIENT).ToString("0.000") + " " + UNIT_CODE + "</span>";
                }
            }

            Literal ltrItemUnit_CalculatedCoeifficient = row.FindControl("ltrItemUnit_CalculatedCoeifficient") as Literal;
            ltrItemUnit_CalculatedCoeifficient.Text = _text;
        }

    }

    protected void lnkSelectUnitTemplate_Click(object sender, EventArgs e)
    {
        int item_record_id = Convert.ToInt32(hdnItemRecordId.Value);

        int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
        int unit_record_id = Convert.ToInt32(grdAddAssets_UnitTeplate.DataKeys[rowIndex].Values["RECORD_ID"]);

        float coefficient = 1;

        bool isMainUnit =  db.isMainUnit(item_record_id);

        if (!isMainUnit) coefficient = 0;

        string _result = db.AddItemUnit(item_record_id, unit_record_id, coefficient, isMainUnit);

        if (!_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        FillItemUnit();
    }
   
    protected void grdAddAssets_ItemUnit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_ItemUnitCodeLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_ItemUnitCoefficientLabel");
        }
    }
    
    
    //delete ItemUnit
    protected void lnkAddAssetsDeleteCoefficientData_Click(object sender, EventArgs e)
    {
        LinkButton lnkAddAssetsDeleteCoefficientData = (LinkButton)sender;
        hdnFieldDeleteItemUnitID.Value = lnkAddAssetsDeleteCoefficientData.CommandArgument;
        int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
        bool is_main_unit = Convert.ToBoolean(grdAddAssets_ItemUnit.DataKeys[rowIndex].Values["IS_MAIN_UNIT"]);

        if (is_main_unit) ltrDeleteItemUnitModalBody.Text = Config.getXmlValue(u_data.lang, "ltrDeleteItemUnitModalBodyMessage2");
        else ltrDeleteItemUnitModalBody.Text = Config.getXmlValue(u_data.lang, "ltrDeleteItemUnitModalBodyMessage1");
        btnDeleteItemUnit.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnDeleteItemUnitCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");

        hdnFieldDeleteItemUnitIsMainUnit.Value = is_main_unit.ToString();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_deleteItemUnitModal();", true);
    }
   
    protected void btnDeleteItemUnit_Click(object sender, EventArgs e)
    {
        bool is_main_unit = Convert.ToBoolean(hdnFieldDeleteItemUnitIsMainUnit.Value);
        int record_id = Convert.ToInt32(hdnFieldDeleteItemUnitID.Value);
      //  DeleteItemUnit

        string _result = db.DeleteItemUnit(record_id, is_main_unit);

        if (!_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        FillItemUnit();
    }
   
    
    // edit item unit coefficient
    protected void lnkAddAssetsChangeCoefficientData_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditItemUnitModalMessage.Text = "";
        LinkButton lnkAddAssetsChangeCoefficientData = (LinkButton)sender;
        hdnFieldEditItemUnitID.Value = lnkAddAssetsChangeCoefficientData.CommandArgument;
        int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
       
        bool is_main_unit = Convert.ToBoolean(grdAddAssets_ItemUnit.DataKeys[rowIndex].Values["IS_MAIN_UNIT"]);
        float _coefficient = float.Parse(grdAddAssets_ItemUnit.DataKeys[rowIndex].Values["COEFFICIENT"].ToString());
        int item_recid = Convert.ToInt32(grdAddAssets_ItemUnit.DataKeys[rowIndex].Values["ITEM_RECID"]);
        string unit_code = Convert.ToString(grdAddAssets_ItemUnit.DataKeys[rowIndex].Values["UNIT_CODE"]);

        DataRow drMainUnitData = db.getMainUnitCode(item_recid);
        string main_unitCode = Convert.ToString(drMainUnitData["UNIT_CODE"]);
        string main_unitCoefficient = Convert.ToString(drMainUnitData["COEFFICIENT"]);

        hdnFieldEditItemUnitMainCoefficient.Value = main_unitCoefficient;


        btnAddAsstets_EditItemUnit.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnAddAsstets_EditItemUnitCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        hdnFieldEditItemUnitIsMainUnit.Value = is_main_unit.ToString();
        secondDiv.Visible = (is_main_unit == false);

        ltrAddAsstets_EditItemUnit_Coefficient0.Text = unit_code;
        ltrAddAsstets_EditItemUnit_MainCoefficient0.Text = main_unitCode;
        ltrAddAsstets_EditItemUnit_MainCoefficient1.Text = main_unitCode;
        ltrAddAsstets_EditItemUnit_Coefficient1.Text = unit_code;
        ltrAddAsstets_EditItemUnit_MainCoefficient0.Visible = is_main_unit == false;
        lnkCalculateItemUnit.Visible = is_main_unit == false;


        txtAddAsstets_EditItemUnit_Coefficient0.Text = _coefficient.ToString("0.000");
        txtAddAsstets_EditItemUnit_MainCoefficient.Text = _coefficient == 0 ? "0.000" : (float.Parse(main_unitCoefficient) / _coefficient).ToString("0.000");

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemUnitModal();", true);
    }

    Config _configFile = new Config();

    protected void lnkCalculateItemUnit_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditItemUnitModalMessage.Text = "";
        string input1 = txtAddAsstets_EditItemUnit_Coefficient0.Text.Trim();
        string input2 = txtAddAsstets_EditItemUnit_MainCoefficient.Text.Trim();

        decimal f_input1 = _configFile.ConvertToDecimal(input1);
        decimal f_input2 = _configFile.ConvertToDecimal(input2);

        if (f_input1 == -1 || f_input2 == -1 || f_input1 < 0 || f_input2 < 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "unitCoefficientValidationMessage"));
            return;
        }

        decimal main_unit_coefficient = _configFile.ConvertToDecimal(hdnFieldEditItemUnitMainCoefficient.Value);

        if (f_input1 > 0 && f_input2 > 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemUnitModal();", true);
            ltrAddAsstets_EditItemUnitModalMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "unitCoefficientValidationMessage2"));
            return;
        }


        if (f_input2 == 0)
        {
            if (f_input1 != 0)
            {
                txtAddAsstets_EditItemUnit_MainCoefficient.Text = (main_unit_coefficient / f_input1).ToString("0.000");
            }
        }

        if (f_input1 == 0)
        {
            if (f_input2 != 0)
            {
                txtAddAsstets_EditItemUnit_Coefficient0.Text = (main_unit_coefficient / f_input2).ToString("0.000");
            }
        }
       // lnkCalculateItemUnit.Visible = false;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemUnitModal();", true);
    }
    protected void btnAddAsstets_EditItemUnit_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditItemUnitModalMessage.Text = "";
        string input1 = txtAddAsstets_EditItemUnit_Coefficient0.Text.Trim();
        decimal f_input1 = _configFile.ConvertToDecimal(input1);
       
        if (f_input1 == -1 || f_input1 <= 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemUnitModal();", true);
            ltrAddAsstets_EditItemUnitModalMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "unitCoefficientValidationMessage"));
            return;
        }

        string _result = db.UpdateItemUnitCoefficient(Convert.ToInt32(hdnFieldEditItemUnitID.Value), f_input1);

        if (!_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        FillItemUnit();

    }






    protected void lnkAdd_BarCode_RFID_QRTab_Click(object sender, EventArgs e)
    {
        lnkAddGeneralDataTab.CssClass = "nav-link";
        lnkAddUnitTab.CssClass = "nav-link";
        lnkAdd_BarCode_RFID_QRTab.CssClass = "nav-link active";
        FillItemBarcode();
        MultiViewAddTabs.ActiveViewIndex = 2;
    }
    

    //add barcode
    protected void btnAddAsstets_AddBarcode_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_AddBarcodeMessage.Text = "";
        int item_record_id = Convert.ToInt32(hdnItemRecordId.Value);

        if (txtAddAsstets_AddBarcodeCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddItemBarcodeModal();", true);
            ltrAddAsstets_AddBarcodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddAsstets_AddBarcodeCodeEmptyMessage"));
            return;
        }
        if (txtAddAsstets_AddBarcodeDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddItemBarcodeModal();", true);
            ltrAddAsstets_AddBarcodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddAsstets_AddBarcodeDescriptionEmptyMessage"));
            return;
        }
        if (drlAddAsstets_AddBarcodeType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddItemBarcodeModal();", true);
            ltrAddAsstets_AddBarcodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "drlAddAsstets_AddBarcodeTypeEmptyMessage"));
            return;
        }

        string _result = db.AddItemBarcode(item_record_id, txtAddAsstets_AddBarcodeCode.Text.Trim(), txtAddAsstets_AddBarcodeDescription.Text.Trim(), Convert.ToInt16(drlAddAsstets_AddBarcodeType.SelectedValue));
        if (!_result.Equals("OK") && !_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        else if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_AddItemBarcodeModal();", true);
            ltrAddAsstets_AddBarcodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "existsItemBarcodeExistsBarcodeCodeMessage"));
            return;
        }
        txtAddAsstets_AddBarcodeCode.Text = "";
        txtAddAsstets_AddBarcodeDescription.Text = "";
        drlAddAsstets_AddBarcodeType.SelectedValue = "";
        FillItemBarcode();

    }

    void FillItemBarcode()
    {
        int item_record_id = Convert.ToInt32(hdnItemRecordId.Value);
        grdAddAssets_ItemBarcode.DataSource = db.GetBarcodeListByItemId(item_record_id);
        grdAddAssets_ItemBarcode.DataBind();
        grdAddAssets_ItemBarcode.UseAccessibleHeader = true;
        if (grdAddAssets_ItemBarcode.Rows.Count > 0)
        {
            grdAddAssets_ItemBarcode.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        foreach (GridViewRow row in grdAddAssets_ItemBarcode.Rows)
        {
            LinkButton lnkAddAssets_AddBarcodeDataEdit = row.FindControl("lnkAddAssets_AddBarcodeDataEdit") as LinkButton;
            lnkAddAssets_AddBarcodeDataEdit.Text = Config.getXmlValue(u_data.lang, "dataBtnEditLabel");

            LinkButton lnkAddAssets_AddBarcodeDataDelete = row.FindControl("lnkAddAssets_AddBarcodeDataDelete") as LinkButton;
            lnkAddAssets_AddBarcodeDataDelete.Text = Config.getXmlValue(u_data.lang, "dataBtnDeleteLabel");

        }
    }
    protected void grdAddAssets_ItemBarcode_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_ItemBarcodeCodeLabel");
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_ItemBarcodeDescriptionLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "grdAddAssets_ItemBarcodeTypeLabel");
        }
    }

    //edit barcode
    protected void lnkAddAssets_AddBarcodeDataEdit_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditBarcodeMessage.Text = "";
        LinkButton lnkAddAssets_AddBarcodeDataEdit = (LinkButton)sender;
        hdnAddAsstets_EditBarcodeRecId.Value = lnkAddAssets_AddBarcodeDataEdit.CommandArgument;
        FillbarcodeDataById(Convert.ToInt32(hdnAddAsstets_EditBarcodeRecId.Value));
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemBarcodeModal();", true);
    }

    private void FillbarcodeDataById(int record_id)
    {
        DataRow drEditBarcode = db.GetBarcodeById(record_id);
        if (drEditBarcode != null)
        {
            txtAddAsstets_EditBarcodeCode.Text = Convert.ToString(drEditBarcode["CODE"]);
            txtAddAsstets_EditBarcodeDescription.Text = Convert.ToString(drEditBarcode["DESCRIPTION"]);
            drlAddAsstets_EditBarcodeType.SelectedValue = Convert.ToString(drEditBarcode["CODE_TYPE"]);
        }
    }
    protected void btnAddAsstets_EditBarcode_Click(object sender, EventArgs e)
    {
        ltrAddAsstets_EditBarcodeMessage.Text = "";
        int edit_record_id = Convert.ToInt32(hdnAddAsstets_EditBarcodeRecId.Value);

        if (txtAddAsstets_EditBarcodeCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemBarcodeModal();", true);
            ltrAddAsstets_EditBarcodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddAsstets_AddBarcodeCodeEmptyMessage"));
            return;
        }
        if (txtAddAsstets_EditBarcodeDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemBarcodeModal();", true);
            ltrAddAsstets_EditBarcodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "txtAddAsstets_AddBarcodeDescriptionEmptyMessage"));
            return;
        }
        if (drlAddAsstets_EditBarcodeType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemBarcodeModal();", true);
            ltrAddAsstets_EditBarcodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "drlAddAsstets_AddBarcodeTypeEmptyMessage"));
            return;
        }

        string _result = db.EditItemBarcode(edit_record_id, txtAddAsstets_EditBarcodeCode.Text.Trim(), txtAddAsstets_EditBarcodeDescription.Text.Trim(), Convert.ToInt16(drlAddAsstets_EditBarcodeType.SelectedValue));
        if (!_result.Equals("OK") && !_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        else if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_EditItemBarcodeModal();", true);
            ltrAddAsstets_EditBarcodeMessage.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "existsItemBarcodeExistsBarcodeCodeMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "editBarcodeSuccessMessage"));
        }
        FillItemBarcode();
    }
  
    //delete barcode
    protected void lnkAddAssets_AddBarcodeDataDelete_Click(object sender, EventArgs e)
    {
        LinkButton lnkAddAssets_AddBarcodeDataDelete = (LinkButton)sender;
        hdnFieldDeleteItemBarcodeRecordID.Value = lnkAddAssets_AddBarcodeDataDelete.CommandArgument;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddAssets_DeleteItemBarcodeModal();", true);
    }
    protected void btnDeleteItemBarcode_Click(object sender, EventArgs e)
    {
      string _result =  db.DeleteItemBarcode(Convert.ToInt32(hdnFieldDeleteItemBarcodeRecordID.Value));
      if (!_result.Equals("OK"))
      {
          ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
          ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
          return;
      }  
      else
      {
          ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
          ltrAlertMsg.Text = Alert.SuccessMessage(Config.getXmlValue(u_data.lang, "deleteBarcodeSuccessMessage"));
      }
      FillItemBarcode();
    }
   
    
    //ADD OR UPDATE ITEMS
    protected void btnInsertUpdateItem_Click(object sender, EventArgs e)
    {
        if (txtAddAssetsCode.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addOrUpdate_ItemsCodeMessage"));
            return;
        }

        if (txtAddAssetsDescription.Text.Trim().Length < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addOrUpdate_ItemsDecsriptionMessage"));
            return;
        }
        int pItemRecId = Convert.ToInt32(hdnItemRecordId.Value);

        int pItemUnitCount = db.GetItemUnitCount(pItemRecId);
        if (pItemUnitCount < 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addOrUpdate_ItemsCheckUnitCountMessage"));
            return;
        }

        string pCode = txtAddAssetsCode.Text.Trim();
        string pDescription = txtAddAssetsDescription.Text.Trim();
        string pStatus = drlAddAssetsStatus.SelectedValue;
        string pDescription2 = txtAddAssetsDescription2.Text.Trim();
        string pNote = txtAddAssetsNote.Text.Trim();
        short? pModifiedUserId = null; if (hdnOperationType.Value == "UPDATE") pModifiedUserId = (short)u_data.LogonUserID;
        DateTime? pModifiedDate = null; if (hdnOperationType.Value == "UPDATE") pModifiedDate = Config.HostingTime;

        int ? pMarkId =   null;   
        if (drlSelectMark.SelectedValue != "") pMarkId = Convert.ToInt32(drlSelectMark.SelectedValue);
        int ? pGroupId =  null;   if (drlSelectGroup.SelectedValue != "") pGroupId = Convert.ToInt32(drlSelectGroup.SelectedValue);
            
        int ? pSpecode1 = null;   if (drlSelectSpecode1.SelectedValue != "") pSpecode1 = Convert.ToInt32(drlSelectSpecode1.SelectedValue);
        int ? pSpecode2 = null;   if (drlSelectSpecode2.SelectedValue != "") pSpecode2 = Convert.ToInt32(drlSelectSpecode2.SelectedValue);
        int ? pSpecode3 = null;   if (drlSelectSpecode3.SelectedValue != "") pSpecode3 = Convert.ToInt32(drlSelectSpecode3.SelectedValue);
        int ? pSpecode4 = null;   if (drlSelectSpecode4.SelectedValue != "") pSpecode4 = Convert.ToInt32(drlSelectSpecode4.SelectedValue);
        int ? pSpecode5 = null;   if (drlSelectSpecode5.SelectedValue != "") pSpecode5 = Convert.ToInt32(drlSelectSpecode5.SelectedValue);

        string _result = db.AddOrUpdateItem(pItemRecId, pCode, pDescription, pDescription2, pNote, pStatus, pModifiedUserId, pModifiedDate, pMarkId, pGroupId, pSpecode1, pSpecode2, pSpecode3, pSpecode4, pSpecode5);

        if (_result.Equals("EXISTS"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "addAssetsAddItemExistsMessage"));
            return;
        }
        else if (!_result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }

        if (hdnOperationType.Value == "INSERT") Session["GridItemPageIndex"] = 0;

        Config.Rd("/index");

        // will fill grid
       
    }

    //cancel add or update item
    protected void btnInsertUpdateItemCancel_Click(object sender, EventArgs e)
    {
        db.DeletePendingItem((short)u_data.LogonUserID);
        Config.Rd("~/index");

    }
   
   
    // Items
    
    protected void grdItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["GridItemPageIndex"] = e.NewPageIndex;
        grdItems.PageIndex = Convert.ToInt32(Session["GridItemPageIndex"]);
        FillItems(Convert.ToInt32(Session["GridItemPageIndex"])); 
    }
    
    protected void grdItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[1].Text = Config.getXmlValue(u_data.lang, "GridItemCodeLabel");
            e.Row.Cells[2].Text = Config.getXmlValue(u_data.lang, "GridItemDescriptionLabel");
            e.Row.Cells[3].Text = Config.getXmlValue(u_data.lang, "GridItemWarehouseRemainLabel");
            e.Row.Cells[4].Text = Config.getXmlValue(u_data.lang, "GridItemUnitCodeLabel");
            e.Row.Cells[5].Text = Config.getXmlValue(u_data.lang, "GridItemMarkDescriptionLabel");
            e.Row.Cells[6].Text = Config.getXmlValue(u_data.lang, "GridItemGroupDescriptionLabel");
        }
       
    }
    protected void drlGridItemPageSize_SelectedIndexChanged(object sender, EventArgs e)
    {
        grdItems.PageIndex = 0;
        Session["drlGridItemPageSize"] = drlGridItemPageSize.SelectedValue;
        Session["GridItemPageIndex"] = 0;
        FillItems(0); // when pagesize changed - first page load
    }


    //edit items
    protected void lnkItemsDataEdit_Click(object sender, EventArgs e)
    {
        LinkButton lnkItemsDataEdit = (LinkButton)sender;
        hdnItemRecordId.Value = lnkItemsDataEdit.CommandArgument;
        hdnOperationType.Value = "UPDATE";
        btnInsertUpdateItem.Text = Config.getXmlValue(u_data.lang, "btnAcceptLabel");
        btnInsertUpdateItemCancel.Text = Config.getXmlValue(u_data.lang, "btnCancelTextLabel");
        MultiViewMain.ActiveViewIndex = 1;
        DataRow drItem = db.GetItemByRecordId(Convert.ToInt32(hdnItemRecordId.Value));
        if (drItem == null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }
        PageComponetEnable();

        txtAddAssetsCode.Text = Convert.ToString(drItem["CODE"]);
        txtAddAssetsDescription.Text = Convert.ToString(drItem["DESCRIPTION"]);
        txtAddAssetsDescription2.Text = Convert.ToString(drItem["DESCRIPTION2"]);
        txtAddAssetsNote.Text = Convert.ToString(drItem["NOTE"]);
        drlAddAssetsStatus.SelectedValue = Convert.ToString(drItem["STATUS"]);

        drlSelectMark.SelectedValue = Convert.ToString(drItem["MARK_RECID"]);
        drlSelectGroup.SelectedValue = Convert.ToString(drItem["GROUP_RECID"]);
        drlSelectSpecode1.SelectedValue = Convert.ToString(drItem["SPECODE1_RECID"]);
        drlSelectSpecode2.SelectedValue = Convert.ToString(drItem["SPECODE2_RECID"]);
        drlSelectSpecode3.SelectedValue = Convert.ToString(drItem["SPECODE3_RECID"]);
        drlSelectSpecode4.SelectedValue = Convert.ToString(drItem["SPECODE4_RECID"]);
        drlSelectSpecode5.SelectedValue = Convert.ToString(drItem["SPECODE5_RECID"]);
    }


    void PageComponetDisable()
    {
        btnInsertUpdateItem.Visible = false;
        txtAddAssetsCode.Attributes.Add("readonly", "");
        txtAddAssetsDescription.Attributes.Add("readonly", "");
        drlAddAssetsStatus.Attributes.Add("disabled", "disabled");
        txtAddAssetsDescription2.Attributes.Add("readonly", "");
        txtAddAssetsNote.Attributes.Add("readonly", "");

        drlSelectMark.Attributes.Add("disabled", "disabled");
      
        lnkAddMarkModal.Visible = false;
        drlFillMarkByStatus.Visible = false;
        grdAddAssets_AddMark.Visible = false;
        drlSelectGroup.Attributes.Add("disabled", "disabled");
        lnkAddGroupModal.Visible = false;
        drlFillGroupByStatus.Visible = false;
        grdAddAssets_AddGroup.Visible = false;
        drlSelectSpecode1.Attributes.Add("disabled", "disabled");
        drlSelectSpecode2.Attributes.Add("disabled", "disabled");
        drlSelectSpecode3.Attributes.Add("disabled", "disabled");
        drlSelectSpecode4.Attributes.Add("disabled", "disabled");
        drlSelectSpecode5.Attributes.Add("disabled", "disabled");
        drlFillSpecodeByStatus.Visible = false;
        lnkAddSpecodeModal.Visible = false;
        grdAddAssets_AddSpecode.Visible = false;
        DivAddUnitModal.Visible = false;
        grdAddAssets_ItemUnit.Columns[3].Visible = false;
        grdAddAssets_ItemUnit.Columns[4].Visible = false;
        DivAddBarcodeModal.Visible = false;
        grdAddAssets_ItemBarcode.Columns[3].Visible = false;
    }


    void PageComponetEnable()
    {
        btnInsertUpdateItem.Visible = true;
        txtAddAssetsCode.Attributes.Remove("readonly");
        txtAddAssetsDescription.Attributes.Remove("readonly");
        drlAddAssetsStatus.Attributes.Remove("disabled");
        txtAddAssetsDescription2.Attributes.Remove("readonly");
        txtAddAssetsNote.Attributes.Remove("readonly");

        drlSelectMark.Attributes.Remove("disabled");

        lnkAddMarkModal.Visible = true;
        drlFillMarkByStatus.Visible = true;
        grdAddAssets_AddMark.Visible = true;
        drlSelectGroup.Attributes.Remove("disabled");
        lnkAddGroupModal.Visible = true;
        drlFillGroupByStatus.Visible = true;
        grdAddAssets_AddGroup.Visible = true;
        drlSelectSpecode1.Attributes.Remove("disabled");
        drlSelectSpecode2.Attributes.Remove("disabled");
        drlSelectSpecode3.Attributes.Remove("disabled");
        drlSelectSpecode4.Attributes.Remove("disabled");
        drlSelectSpecode5.Attributes.Remove("disabled");
        drlFillSpecodeByStatus.Visible = true;
        lnkAddSpecodeModal.Visible = true;
        grdAddAssets_AddSpecode.Visible = true;
      //  lnkAddUnitModal.Visible = true;
        DivAddUnitModal.Visible = true;
        
        grdAddAssets_ItemUnit.Columns[3].Visible = true;
        grdAddAssets_ItemUnit.Columns[4].Visible = true;
        //lnkAddBarcodeModal.Visible = true;
        DivAddBarcodeModal.Visible = true;
        grdAddAssets_ItemBarcode.Columns[3].Visible = true;
        
    }

    protected void lnkItemsView_Click(object sender, EventArgs e)
    {
        LinkButton lnkItemsView = (LinkButton)sender;
        hdnItemRecordId.Value = lnkItemsView.CommandArgument;
        btnInsertUpdateItemCancel.Text = Config.getXmlValue(u_data.lang, "btnBackTextLabel");
        hdnOperationType.Value = "VIEW";
        MultiViewMain.ActiveViewIndex = 1;
        DataRow drItem = db.GetItemByRecordId(Convert.ToInt32(hdnItemRecordId.Value));
        if (drItem == null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }

        txtAddAssetsCode.Text = Convert.ToString(drItem["CODE"]);
        txtAddAssetsDescription.Text = Convert.ToString(drItem["DESCRIPTION"]);
        txtAddAssetsDescription2.Text = Convert.ToString(drItem["DESCRIPTION2"]);
        txtAddAssetsNote.Text = Convert.ToString(drItem["NOTE"]);
        drlAddAssetsStatus.SelectedValue = Convert.ToString(drItem["STATUS"]);

        drlSelectMark.SelectedValue = Convert.ToString(drItem["MARK_RECID"]);
        drlSelectGroup.SelectedValue = Convert.ToString(drItem["GROUP_RECID"]);
        drlSelectSpecode1.SelectedValue = Convert.ToString(drItem["SPECODE1_RECID"]);
        drlSelectSpecode2.SelectedValue = Convert.ToString(drItem["SPECODE2_RECID"]);
        drlSelectSpecode3.SelectedValue = Convert.ToString(drItem["SPECODE3_RECID"]);
        drlSelectSpecode4.SelectedValue = Convert.ToString(drItem["SPECODE4_RECID"]);
        drlSelectSpecode5.SelectedValue = Convert.ToString(drItem["SPECODE5_RECID"]);
        PageComponetDisable();
    }
  
    //History
    protected void lnkDataHistory_Click(object sender, EventArgs e)
    {
        LinkButton lnkDataHistory = (LinkButton)sender;
        int itemRecordId = Convert.ToInt32(lnkDataHistory.CommandArgument);
        DataRow drItem = db.GetItemByRecordId(itemRecordId);
        ltrViewAsstets_HistoryModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrViewAsstets_HistoryModalHeaderLabel");
      
        ltrViewAsstets_HistoryCreateUserLabel.Text = Config.getXmlValue(u_data.lang, "ltrViewAsstets_HistoryCreateUserLabel");
        ltrViewAsstets_HistoryCreateUser.Text = "<i>" + Convert.ToString(drItem["CREATEDBY_USER"]) + "</i>";
        ltrViewAsstets_HistoryCreateDateLabel.Text = Config.getXmlValue(u_data.lang, "ltrViewAsstets_HistoryCreateDateLabel");
        ltrViewAsstets_HistoryCreateDate.Text = Convert.ToString(drItem["CREATED_DATE"]).Trim().Length != 0 ? "<i>" + Convert.ToDateTime(drItem["CREATED_DATE"]).ToString("dd.MM.yyyy HH:mm") + "</i>" : "<i>" + Convert.ToString(drItem["CREATED_DATE"]) + "</i>";

        ltrViewAsstets_HistoryModifiedUserLabel.Text = Config.getXmlValue(u_data.lang, "ltrViewAsstets_HistoryModifiedUserLabel");
        ltrViewAsstets_HistoryModifiedUser.Text = "<i>" +  Convert.ToString(drItem["MODIFIEDBY_USER"]) + "</i>";
        ltrViewAsstets_HistoryModifiedDateLabel.Text = Config.getXmlValue(u_data.lang, "ltrViewAsstets_HistoryModifiedDateLabel");
        ltrViewAsstets_HistoryModifiedDate.Text = Convert.ToString(drItem["MODIFIED_DATE"]).Trim().Length != 0 ? "<i>" + Convert.ToDateTime(drItem["MODIFIED_DATE"]).ToString("dd.MM.yyyy HH:mm") + "</i>" : Convert.ToString(drItem["MODIFIED_DATE"]);
        btnViewAsstets_HistoryCancel.InnerText = Config.getXmlValue(u_data.lang, "btnCloseButtonLabel");

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openViewAssets_HistoryModal();", true);
    }
   
    // delete Item
    protected void lnkItemsDelete_Click(object sender, EventArgs e)
    {
        LinkButton lnkItemsDelete = (LinkButton)sender;
        hdnFieldItemRecordID.Value = lnkItemsDelete.CommandArgument;
        ltrDeleteItemModalHeader.Text = Config.getXmlValue(u_data.lang, "ltrDeleteItemModalHeaderLabel");
        ltrDeleteItemModalBody.Text = Config.getXmlValue(u_data.lang, "ltrDeleteItemModalBodyMessage");


        btnDeleteItem.Text = Config.getXmlValue(u_data.lang, "yesLabel");
        btnDeleteItemCancel.InnerText = Config.getXmlValue(u_data.lang, "noLabel");

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteAssets_DeleteItemModal();", true);
    }
    protected void btnDeleteItem_Click(object sender, EventArgs e)
    {
        int deletedItemRecordId = Convert.ToInt32(hdnFieldItemRecordID.Value);

        string _result = db.DeleteItem(deletedItemRecordId);

        if (_result.Equals("OK"))
        {
            Config.Rd("~/index");
        }
        else if (_result.Equals("BUSY"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "deleteItemBusyMessage"));
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage(Config.getXmlValue(u_data.lang, "anyErrorString"));
            return;
        }

    }
    
    //active & deactive
    protected void drlAssetsListStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        grdItems.VirtualItemCount = db.GetItemCount(drlAssetsListStatus.SelectedValue);
        grdItems.PageIndex = 0;
        Session["drlGridItemPageSize"] = drlGridItemPageSize.SelectedValue;
        Session["GridItemPageIndex"] = 0;
        FillItems(0); // when pagesize changed - first page load
    }
}