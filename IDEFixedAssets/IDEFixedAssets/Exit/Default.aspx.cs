﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Exit_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["az"] = null;
        Session["UserData"] = null;
        Session.Clear();
        Session.Abandon();
        Config.Rd("/");
    }
}