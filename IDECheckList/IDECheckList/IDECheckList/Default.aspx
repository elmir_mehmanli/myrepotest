﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IDECheckList.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title></title>
    <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../lib/weather-icons/css/weather-icons.css" />
    <link rel="stylesheet" href="../lib/jquery-toggles/toggles-full.css" />
    <link rel="stylesheet" href="../lib/jquery.gritter/jquery.gritter.css" />
    <link rel="stylesheet" href="../lib/animate.css/animate.css" />

    <link rel="stylesheet" href="../css/quirk.css" />

    <script src="../lib/modernizr/modernizr.js"></script>
</head>
<body class="signwrapper">
    <form id="form1" runat="server">
        <div class="sign-overlay"></div>
        <div class="signpanel"></div>
        <div class="panel signin">
            <div class="panel-heading">
                <h1>IDE CheckList</h1>
            </div>
            <div class="panel-body">
                <div class="or">--</div>
                <div class="form-group mb15">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <asp:TextBox ID="txtLogin" class="form-control" autocomplete="off" placeholder="İstifadəçi adı" MaxLength="50" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group mb15">

                    <div class="form-group input-group" style="margin-bottom: 0" id="txtPasswordShow">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <asp:TextBox ID="txtPassword" class="form-control" autocomplete="off" TextMode="Password" MaxLength="50" runat="server" placeholder="Şifrə"></asp:TextBox>
                        <span class="input-group-addon" style ="padding-right:5px !important">
                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                        </span>
                    </div>
                </div>

                <div class="form-group mb25">
                    <asp:TextBox ID="txtSecurity" class="form-control" MaxLength="5" placeholder="Təhlükəsizlik kodu" autocomplete="off" runat="server" Font-Bold="True" ForeColor="Maroon"></asp:TextBox>
                </div>
                <div class="form-group mb25">
                    <asp:Image ID="ImgSecurity" runat="server" class="form-control" Style="padding: 0px !important" />
                </div>

                <div class="form-group">
                    <img id="login_loading" style="display: none" src="../img/loader_wbg.gif" />
                    <asp:Button ID="btnLogin" OnClick="btnLogin_Click"
                        class="btn btn-success btn-quirk btn-block" runat="server" Text="GİRİŞ"
                        OnClientClick="this.style.display = 'none';
                                          document.getElementById('login_loading').style.display = '';" />
                </div>
            </div>
        </div>
        <!-- panel -->


        <script src="../lib/jquery/jquery.js"></script>
        <script src="../lib/jquery-ui/jquery-ui.js"></script>
        <script src="../lib/bootstrap/js/bootstrap.js"></script>
        <script src="../lib/jquery-toggles/toggles.js"></script>
        <script src="../lib/jquery.gritter/jquery.gritter.js"></script>
        <script src="../js/quirk.js"></script>

        <script>
            function jsAlert(_title, _message, _type) {
                $.gritter.add({
                    title: _title,
                    text: _message,
                    class_name: 'with-icon times-circle ' + _type
                });
            }
        </script>


        <script>
            $(document).ready(function () {
                $("#txtPasswordShow a").on('click', function (event) {
                    event.preventDefault();
                    if ($('#txtPasswordShow input').attr("type") == "text") {
                        $('#txtPasswordShow input').attr('type', 'password');
                        $('#txtPasswordShow i').addClass("fa-eye-slash");
                        $('#txtPasswordShow i').removeClass("fa-eye");
                    } else if ($('#txtPasswordShow input').attr("type") == "password") {
                        $('#txtPasswordShow input').attr('type', 'text');
                        $('#txtPasswordShow i').removeClass("fa-eye-slash");
                        $('#txtPasswordShow i').addClass("fa-eye");
                    }
                });
            });
        </script>

    </form>
</body>
</html>
