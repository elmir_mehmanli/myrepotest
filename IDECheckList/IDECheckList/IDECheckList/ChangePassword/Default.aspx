﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IDECheckList.ChangePassword.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../lib/jquery/jquery.js"></script>
    <script>
        $(document).ready(function () {
            $("#txtUserPasswordShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtUserPasswordShow input').attr("type") == "text") {
                    $('#txtUserPasswordShow input').attr('type', 'password');
                    $('#txtUserPasswordShow i').addClass("fa-eye-slash");
                    $('#txtUserPasswordShow i').removeClass("fa-eye");
                } else if ($('#txtUserPasswordShow input').attr("type") == "password") {
                    $('#txtUserPasswordShow input').attr('type', 'text');
                    $('#txtUserPasswordShow i').removeClass("fa-eye-slash");
                    $('#txtUserPasswordShow i').addClass("fa-eye");
                }
            });
        });

        $(document).ready(function () {
            $("#txtUserPasswordRepeatShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtUserPasswordRepeatShow input').attr("type") == "text") {
                    $('#txtUserPasswordRepeatShow input').attr('type', 'password');
                    $('#txtUserPasswordRepeatShow i').addClass("fa-eye-slash");
                    $('#txtUserPasswordRepeatShow i').removeClass("fa-eye");
                } else if ($('#txtUserPasswordRepeatShow input').attr("type") == "password") {
                    $('#txtUserPasswordRepeatShow input').attr('type', 'text');
                    $('#txtUserPasswordRepeatShow i').removeClass("fa-eye-slash");
                    $('#txtUserPasswordRepeatShow i').addClass("fa-eye");
                }
            });
        });


        $(document).ready(function () {
            $("#txtChangePasswordUserPasswordShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtChangePasswordUserPasswordShow input').attr("type") == "text") {
                    $('#txtChangePasswordUserPasswordShow input').attr('type', 'password');
                    $('#txtChangePasswordUserPasswordShow i').addClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordShow i').removeClass("fa-eye");
                } else if ($('#txtChangePasswordUserPasswordShow input').attr("type") == "password") {
                    $('#txtChangePasswordUserPasswordShow input').attr('type', 'text');
                    $('#txtChangePasswordUserPasswordShow i').removeClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordShow i').addClass("fa-eye");
                }
            });
        });

        $(document).ready(function () {
            $("#txtChangePasswordUserPasswordRepeatShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtChangePasswordUserPasswordRepeatShow input').attr("type") == "text") {
                    $('#txtChangePasswordUserPasswordRepeatShow input').attr('type', 'password');
                    $('#txtChangePasswordUserPasswordRepeatShow i').addClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordRepeatShow i').removeClass("fa-eye");
                } else if ($('#txtChangePasswordUserPasswordRepeatShow input').attr("type") == "password") {
                    $('#txtChangePasswordUserPasswordRepeatShow input').attr('type', 'text');
                    $('#txtChangePasswordUserPasswordRepeatShow i').removeClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordRepeatShow i').addClass("fa-eye");
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="row">
        <div class="col-sm-6">
            Şifrə:
            <div class="form-group input-group" id="txtUserPasswordShow">
                <asp:TextBox ID="txtPassword" autocomplete="off" placeholder="Şifrə" runat="server" MaxLength="50" class="form-control" TextMode="Password"></asp:TextBox>
                <div class="input-group-addon">
                    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                </div>
            </div>
            Şifrə təkrar:                     
            <div class="form-group input-group" id="txtUserPasswordRepeatShow">
                <asp:TextBox ID="txtPasswordConfirm" autocomplete="off" placeholder="Şifrə təkrar" runat="server" MaxLength="50" class="form-control" TextMode="Password"></asp:TextBox>
                <div class="input-group-addon">
                    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                </div>
            </div>

            <div class="form-group">
                <img id="change_password_loading" style="display: none" src="../img/loader1.gif" />
                <asp:Button ID="btnChangePassword" class="btn btn-primary" runat="server" Text="Şifrəni dəyiş"
                    OnClick ="btnChangePassword_Click"
                    OnClientClick="this.style.display = 'none';
                                                       document.getElementById('change_password_loading').style.display = '';" />
            </div>
        </div>
    </div>
</asp:Content>
