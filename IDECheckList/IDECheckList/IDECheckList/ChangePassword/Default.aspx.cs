﻿using IDECheckList.App_Code;
using IDECheckList.DbProc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IDECheckList.App_Code._Config;

namespace IDECheckList.ChangePassword
{
    public partial class Default : System.Web.UI.Page
    {
        UserData u_data = new UserData();
        DbProcess db = new DbProcess();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserData"] == null) _Config.Rd("/exit");
            u_data = (UserData)Session["UserData"];
            if (!IsPostBack)
            {

            }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.Trim().Length < 6)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Şifrəni 6 simvoldan az olmayaraq daxil edin!");
                txtPassword.Focus();
                return;
            }

            if (txtPassword.Text.Trim() != txtPasswordConfirm.Text.Trim())
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Şifrələr eyni deyil.");
                txtPasswordConfirm.Focus();
                return;
            }

            string result = db.ChangePassword((Int16)u_data.LogonUserID, (Int16)u_data.LogonUserID, _Config.Sha1(txtPassword.Text.Trim()));


            if (result != "OK")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }
            else
            {
                _config.AlertMessageNew(this, MessageType.SUCCESS, "Uğurla dəyişdirildi");
            }

        }
    }
}