﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IDECheckList.markets.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <asp:LinkButton ID="lnkAddNewMarketModal" CssClass="btn btn-primary" runat="server"
       OnClick ="lnkAddNewMarketModal_Click" >
            <i class ="fa fa-plus"></i> Yeni Market</asp:LinkButton>
    <div class="mb40"></div>
    <div class="mb40"></div>

    <div class="table-responsive">
        <asp:GridView ID="grdMarkets" class="table table-bordered table-hover table-primary table-striped nomargin" runat="server" AutoGenerateColumns="False" DataKeyNames="RECORD_ID" GridLines="None">
            <Columns>
                <asp:BoundField DataField="CODE" HeaderText="KODU" />
                <asp:BoundField DataField="NAME" HeaderText="ADI" />
                <asp:BoundField DataField="NOTE" HeaderText="QEYD" />


                <asp:TemplateField>
                    <ItemTemplate>
                        <ul class="table-options">
                            <li>
                                <asp:LinkButton ID="lnkEditMarkets"  OnClick ="lnkEditMarkets_Click"
                                    title="Redaktə et" runat="server"
                                    class="fa fa-pencil" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                </asp:LinkButton>
                            </li>

                            <li>
                                <asp:LinkButton ID="lnkDeleteMarkets"  OnClick ="lnkDeleteMarkets_Click"
                                    title="Sil" runat="server"
                                    class="fa fa-trash" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                </asp:LinkButton>
                            </li>
                        </ul>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </asp:TemplateField>
            </Columns>

        </asp:GridView>
        <asp:HiddenField ID="hdnDeleteMarkets" runat="server" />
        <asp:HiddenField ID="hdnEditMarkets" runat="server" />
    </div>




    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  AddMarket-->
            <div class="modal bounceIn" id="modalAddMarket" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalAddMarketLabel"><i class="fa fa-plus"></i>&nbsp&nbspYeni market</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        Kodu :
                                        <asp:TextBox ID="txtAddMarketCode" autocomplete="off" placeholder="Kodu" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        Adı:
                                       <asp:TextBox ID="txtAddMarketName" autocomplete="off" MaxLength="150" placeholder="Adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                        Qeyd:
                                       <asp:TextBox ID="txtAddMarketNote" TextMode="MultiLine" autocomplete="off" MaxLength="300" placeholder="Qeyd" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <img id="add_Market_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnMarketAdd" class="btn btn-primary" runat="server" Text="Əlavə et"
                                OnClick ="btnMarketAdd_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_Market_loading').style.display = '';
                                                           document.getElementById('btnMarketAddCancel').style.display = 'none';" />
                            <button id="btnMarketAddCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnMarketAdd" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  EditMarket-->
            <div class="modal bounceIn" id="modalEditMarket" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalEditMarketLabel"><i class="icon ion-edit"></i>&nbsp&nbspMarketin yenilənməsi</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        Kodu :
                                        <asp:TextBox ID="txtEditMarketCode" autocomplete="off" placeholder="Kodu" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        Adı:
                                       <asp:TextBox ID="txtEditMarketName" autocomplete="off" MaxLength="150" placeholder="Adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                        Qeyd:
                                       <asp:TextBox ID="txtEditMarketNote" TextMode="MultiLine" autocomplete="off" MaxLength="300" placeholder="Qeyd" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <img id="Edit_Market_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnMarketEdit" class="btn btn-primary" runat="server" Text="Yenilə"
                                OnClick ="btnMarketEdit_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('Edit_Market_loading').style.display = '';
                                                           document.getElementById('btnMarketEditCancel').style.display = 'none';" />
                            <button id="btnMarketEditCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnMarketEdit" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  Alert-->
            <div class="modal bounceIn" id="modalAlert" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalAlertLabel">
                                <asp:Literal ID="ltrAlertModalHeaderLabel" runat="server"></asp:Literal>
                        </div>
                        <div class="modal-body">
                            <p>
                                <asp:Literal ID="ltrAlertModalBodyLabel" runat="server"></asp:Literal>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <img id="delete_Market_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnMarketDelete" class="btn btn-primary" runat="server" Text="Bəli"
                                OnClick ="btnMarketDelete_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_Market_loading').style.display = '';
                                                           document.getElementById('btnMarketDeleteCancel').style.display = 'none';" />
                            <button id="btnMarketDeleteCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnMarketDelete" />
        </Triggers>
    </asp:UpdatePanel>



    <script type="text/javascript">

        function openAlertModal() {
            $('#modalAlert').modal({ show: true });
        }

        function openAddMarketModal() {
            $('#modalAddMarket').modal({ show: true });
        }

        function openEditMarketModal() {
            $('#modalEditMarket').modal({ show: true });
        }

    </script>


</asp:Content>

