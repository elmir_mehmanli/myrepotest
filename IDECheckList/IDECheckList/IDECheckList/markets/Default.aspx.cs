﻿using IDECheckList.App_Code;
using IDECheckList.DbProc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IDECheckList.App_Code._Config;

namespace IDECheckList.markets
{
    public partial class Default : System.Web.UI.Page
    {
        UserData u_data = new UserData();
        DbProcess db = new DbProcess();

        _Config _config = new _Config();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserData"] == null) _Config.Rd("/exit");
            u_data = (UserData)Session["UserData"];
            if (!IsPostBack)
            {
                FillMarkets();
            }
            if (grdMarkets.Rows.Count > 0) grdMarkets.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        void FillMarkets()
        {
            grdMarkets.DataSource = db.GetMarketList();
            grdMarkets.DataBind();
            grdMarkets.UseAccessibleHeader = true;
            if (grdMarkets.Rows.Count > 0) grdMarkets.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void lnkAddNewMarketModal_Click(object sender, EventArgs e)
        {
            txtAddMarketCode.Text = "";
            txtAddMarketName.Text = "";
            txtAddMarketNote.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddMarketModal();", true);
        }


        //add market
        protected void btnMarketAdd_Click(object sender, EventArgs e)
        {
            if (txtAddMarketCode.Text.Trim().Length < 5)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 5 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddMarketModal();", true);
                return;
            }


            if (txtAddMarketName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddMarketModal();", true);
                return;
            }


            string result = db.AddMarkets(txtAddMarketCode.Text.Trim(), txtAddMarketName.Text.Trim(), txtAddMarketNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddMarketModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddMarketModal();", true);
                return;
            }

            _Config.Rd("/markets");
        }


        //edit market modal
        protected void lnkEditMarkets_Click(object sender, EventArgs e)
        {
            LinkButton lnkEditMarket = (LinkButton)sender;
            hdnEditMarkets.Value = lnkEditMarket.CommandArgument;
            FillMarketDataById(Convert.ToInt16(hdnEditMarkets.Value));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditMarketModal();", true);
        }

        private void FillMarketDataById(short editMarketID)
        {
            DataRow drEditMarket = db.GetMarketById(editMarketID);
            if (drEditMarket != null)
            {
                txtEditMarketCode.Text = Convert.ToString(drEditMarket["CODE"]);
                txtEditMarketName.Text = Convert.ToString(drEditMarket["NAME"]);
                txtEditMarketNote.Text = Convert.ToString(drEditMarket["NOTE"]);
            }
        }


        //edit market
        protected void btnMarketEdit_Click(object sender, EventArgs e)
        {
            short editMarketId = Convert.ToInt16(hdnEditMarkets.Value);
            if (txtEditMarketCode.Text.Trim().Length < 5)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 5 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditMarketModal();", true);
                return;
            }


            if (txtEditMarketName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditMarketModal();", true);
                return;
            }


            string result = db.UpdateMarkets(editMarketId, txtEditMarketCode.Text.Trim(), txtEditMarketName.Text.Trim(), txtEditMarketNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditMarketModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditMarketModal();", true);
                return;
            }

            _Config.Rd("/markets");
        }

        //delete market
        protected void lnkDeleteMarkets_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspMarketin silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "Market silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnDeleteMarkets.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
        }


        //delete market 
        protected void btnMarketDelete_Click(object sender, EventArgs e)
        {
            string result = db.DeleteMarkets(Int16.Parse(hdnDeleteMarkets.Value), (Int16)u_data.LogonUserID);

            if (result != "OK")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
                return;
            }

            _Config.Rd("/markets");
        }

       
    }
}