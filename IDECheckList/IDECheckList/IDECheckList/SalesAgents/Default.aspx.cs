﻿using IDECheckList.App_Code;
using IDECheckList.DbProc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IDECheckList.App_Code._Config;

namespace IDECheckList.SalesAgents
{
    public partial class Default : System.Web.UI.Page
    {
        UserData u_data = new UserData();
        DbProcess db = new DbProcess();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserData"] == null) _Config.Rd("/exit");
            u_data = (UserData)Session["UserData"];
            if (!IsPostBack)
            {
                FillSalesAgents();
            }
            if (grdSalesAgents.Rows.Count > 0) grdSalesAgents.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        void FillSalesAgents()
        {
            grdSalesAgents.DataSource = db.GetSalesAgentList();
            grdSalesAgents.DataBind();
            grdSalesAgents.UseAccessibleHeader = true;
            if (grdSalesAgents.Rows.Count > 0) grdSalesAgents.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        //add sales agent modal
        protected void lnkAddNewSalesAgentModal_Click(object sender, EventArgs e)
        {
            txtAddSalesAgentCode.Text = "";
            txtAddSalesAgentName.Text = "";
            txtAddSalesAgentNote.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddSalesAgentModal();", true);
        }

        //add sales agent
        protected void btnSalesAgentAdd_Click(object sender, EventArgs e)
        {
            if (txtAddSalesAgentCode.Text.Trim().Length < 5)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 24 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddSalesAgentModal();", true);
                return;
            }


            if (txtAddSalesAgentName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddSalesAgentModal();", true);
                return;
            }


            string result = db.AddSalesAgents(txtAddSalesAgentCode.Text.Trim(), txtAddSalesAgentName.Text.Trim(), txtAddSalesAgentNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddSalesAgentModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddSalesAgentModal();", true);
                return;
            }

            _Config.Rd("/SalesAgents");
        }

        //edit sales agent modal
        protected void lnkEditSalesAgents_Click(object sender, EventArgs e)
        {

            LinkButton lnkEditMarket = (LinkButton)sender;
            hdnEditSalesAgents.Value = lnkEditMarket.CommandArgument;
            FillSalesAgentDataById(Convert.ToInt16(hdnEditSalesAgents.Value));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditSalesAgentModal();", true);
        }

        private void FillSalesAgentDataById(short editSalesAgentID)
        {
            DataRow drEditMarket = db.GetSalesAgentById(editSalesAgentID);
            if (drEditMarket != null)
            {
                txtEditSalesAgentCode.Text = Convert.ToString(drEditMarket["CODE"]);
                txtEditSalesAgentName.Text = Convert.ToString(drEditMarket["NAME"]);
                txtEditSalesAgentNote.Text = Convert.ToString(drEditMarket["NOTE"]);
            }
        }

        //edit sales agent
        protected void btnSalesAgentEdit_Click(object sender, EventArgs e)
        {
            short editSalesAgentId = Convert.ToInt16(hdnEditSalesAgents.Value);
            if (txtEditSalesAgentCode.Text.Trim().Length < 24)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 24 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditSalesAgentModal();", true);
                return;
            }


            if (txtEditSalesAgentName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditSalesAgentModal();", true);
                return;
            }


            string result = db.UpdateSalesAgents(editSalesAgentId, txtEditSalesAgentCode.Text.Trim(), txtEditSalesAgentName.Text.Trim(), txtEditSalesAgentNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditSalesAgentModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditSalesAgentModal();", true);
                return;
            }

            _Config.Rd("/SalesAgents");
        }


        //delete SalesAgents modal
        protected void lnkDeleteSalesAgents_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspSatış təmsilçisi silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "Market silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnDeleteSalesAgents.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
        }


        //delete SalesAgents 
        protected void btnSalesAgentDelete_Click(object sender, EventArgs e)
        {
            string result = db.DeleteSalesAgent(Int16.Parse(hdnDeleteSalesAgents.Value), (Int16)u_data.LogonUserID);

            if (result != "OK")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
                return;
            }

            _Config.Rd("/SalesAgents");
        }
    }
}