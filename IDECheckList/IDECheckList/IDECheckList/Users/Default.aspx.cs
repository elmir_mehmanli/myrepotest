﻿using IDECheckList.App_Code;
using IDECheckList.DbProc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IDECheckList.App_Code._Config;
using System.Data;

namespace IDECheckList.Users
{
    public partial class Default : System.Web.UI.Page
    {
        UserData u_data = new UserData();
        DbProcess db = new DbProcess();

        _Config _config = new _Config();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserData"] == null) _Config.Rd("/exit");
            u_data = (UserData)Session["UserData"];
            if (!IsPostBack)
            {
                FillUsers();
            }
            if (grdUsers.Rows.Count > 0) grdUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        void FillUsers()
        {
            grdUsers.DataSource = db.GetUserList();
            grdUsers.DataBind();
            grdUsers.UseAccessibleHeader = true;
            grdUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void lnkAddNewUserModal_Click(object sender, EventArgs e)
        {
            txtUserName.Text = "";
            txtUserSurname.Text = "";
            txtUserLogin.Text = "";
            drlUserSex.SelectedValue = "";
            txtPosition.Text = "";
            txtContactNumber.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
        }

        //AddUser
        protected void btnUserAdd_Click(object sender, EventArgs e)
        {
            if (txtUserSurname.Text.Trim().Length < 3)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Soyadı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }


            if (txtUserName.Text.Trim().Length < 3)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }
           

            if (txtUserLogin.Text.Trim().Length < 3 || !_Config.isLoginSymbols(txtUserLogin.Text.Trim()))
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "İstifadəçi adını 3 simvoldan az olmayaraq , ingilis böyük,kiçik hərflərindən , rəqəm simvollarından istifadə edərək daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }

            if (txtUserPassword.Text.Trim().Length < 6 || !_Config.isEnglish(txtUserPassword.Text.Trim()))
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Şifrəni  6 simvoldan az olmayaraq , ingilis böyük,kiçik hərflərindən , rəqəm simvollarından istifadə edərək daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }

            if (txtUserPassword.Text != txtUserPasswordRepeat.Text)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Şifrələr eyni deyil!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }

            if (drlUserSex.SelectedValue == "")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Cinsini seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }

            if (drlUserType.SelectedValue == "")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "İstifadəçinin tipini seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }
            string imageName = "";
            if (drlUserSex.SelectedValue == "M") imageName = "/img/man.png";
            if (drlUserSex.SelectedValue == "F") imageName = "/img/woman.png";

            string result = db.AddUser(txtUserName.Text, txtUserSurname.Text, txtUserLogin.Text, txtUserPassword.Text, txtContactNumber.Text,
           (short)u_data.LogonUserID, drlUserSex.SelectedValue, txtPosition.Text, imageName, drlUserType.SelectedValue);


            if (result == "EXISTS_USER")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz istifadəçi adı artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddUserModal();", true);
                return;
            }

            _Config.Rd("/users");

        }

        protected void lnkEditUser_Click(object sender, EventArgs e)
        {
            LinkButton lnkEditUser = (LinkButton)sender;
            hdnEditUser.Value = lnkEditUser.CommandArgument;
            FillUserDataById(Convert.ToInt16(hdnEditUser.Value));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditUserModal();", true);
        }

        private void FillUserDataById(short editUserID)
        {
            DataRow drEditUser = db.GetUserById(editUserID);
            if (drEditUser != null)
            {
                txtEditUserName.Text = Convert.ToString(drEditUser["NAME"]);
                txtEditUserSurname.Text = Convert.ToString(drEditUser["SURNAME"]);
                txtEditContactNumber.Text = Convert.ToString(drEditUser["CONTACTNUMBER"]);
                txtEditUserLogin.Text = Convert.ToString(drEditUser["USER_NAME"]);
                txtEditPosition.Text = Convert.ToString(drEditUser["POSITION"]);
                drlEditUserSex.SelectedValue = Convert.ToString(drEditUser["GENDER"]);
                drlEditUserType.SelectedValue = Convert.ToString(drEditUser["TYPE"]);

            }
        }

        protected void btnUserEdit_Click(object sender, EventArgs e)
        {
            short editUserId = Convert.ToInt16(hdnEditUser.Value);
            if (txtEditUserSurname.Text.Trim().Length < 3)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Soyadı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditUserModal();", true);
                return;
            }


            if (txtEditUserName.Text.Trim().Length < 3)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditUserModal();", true);
                return;
            }


            if (txtEditUserLogin.Text.Trim().Length < 3 || !_Config.isLoginSymbols(txtEditUserLogin.Text.Trim()))
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "İstifadəçi adını 3 simvoldan az olmayaraq , ingilis böyük,kiçik hərflərindən , rəqəm simvollarından istifadə edərək daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditUserModal();", true);
                return;
            }


            if (drlEditUserSex.SelectedValue == "")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Cinsini seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditUserModal();", true);
                return;
            }

            if (drlEditUserType.SelectedValue == "")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "İstifadəçinin tipini seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditUserModal();", true);
                return;
            }
            string imageName = "";
            if (drlEditUserSex.SelectedValue == "M") imageName = "/img/man.png";
            if (drlEditUserSex.SelectedValue == "F") imageName = "/img/woman.png";

            string result = db.UpdateUser(editUserId, txtEditUserName.Text, txtEditUserSurname.Text, txtEditContactNumber.Text, (short)u_data.LogonUserID, drlEditUserSex.SelectedValue, txtEditPosition.Text, imageName, drlEditUserType.SelectedValue);

            if (result != "OK")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditUserModal();", true);
                return;
            }
            _Config.Rd("/users");
        }

        //blok user
        protected void lnkBlockUser_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            int userId = Convert.ToInt32(lnk.CommandArgument);

            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            string currstatus = Convert.ToString(grdUsers.DataKeys[rowIndex].Values["STATUS"]);
            if (currstatus == "ACTIVE")
            {
                ltrAlertModalHeaderLabel.Text = "<i class=\"glyphicon glyphicon-lock\"></i>&nbspBlock User</h4>";
                ltrAlertModalBodyLabel.Text = "İstifadəçi blok olunsun?";
                hdnUserStatus.Value = "DEACTIVE";
            }

            if (currstatus == "DEACTIVE")
            {
                ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-unlock\"></i>&nbspUnblock user</h4>";
                ltrAlertModalBodyLabel.Text = "İstifadəçi blokdan çıxarılsın?";
                hdnUserStatus.Value = "ACTIVE";
            }
            hdnBlockUser.Value = userId.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
        }

        protected void btnUserChangeStatus_Click(object sender, EventArgs e)
        {
            int userId = 0;
            string successStatusMessage = "";
            if (hdnUserStatus.Value == "ACTIVE")
            {
                userId = Convert.ToInt32(hdnBlockUser.Value);
                successStatusMessage = "İstifaəçi blokdan çıxarıldı";
            }
            if (hdnUserStatus.Value == "DEACTIVE")
            {
                userId = Convert.ToInt32(hdnBlockUser.Value);
                successStatusMessage = "İstifadəçi blok edildi";
            }
            if (hdnUserStatus.Value == "DELETED")
            {
                userId = Convert.ToInt32(hdnDeleteUser.Value);
                successStatusMessage = "İstifadəçi silindi";
            }


            string result = db.BlockUser((Int16)userId, (Int16)u_data.LogonUserID, hdnUserStatus.Value);


            if (result != "OK")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
                return;
            }
            _config.AlertMessageNew(this, MessageType.SUCCESS, successStatusMessage);
            _Config.Rd("/users");
           
        }

        //delete user
        protected void lnkDeleteUser_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspİstifadəçinin silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "İstifadəçi silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnDeleteUser.Value = lnk.CommandArgument;
            hdnUserStatus.Value = "DELETED";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
        }
    }
}