﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IDECheckList.Users.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../lib/jquery/jquery.js"></script>
    <script>
        $(document).ready(function () {
            $("#txtUserPasswordShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtUserPasswordShow input').attr("type") == "text") {
                    $('#txtUserPasswordShow input').attr('type', 'password');
                    $('#txtUserPasswordShow i').addClass("fa-eye-slash");
                    $('#txtUserPasswordShow i').removeClass("fa-eye");
                } else if ($('#txtUserPasswordShow input').attr("type") == "password") {
                    $('#txtUserPasswordShow input').attr('type', 'text');
                    $('#txtUserPasswordShow i').removeClass("fa-eye-slash");
                    $('#txtUserPasswordShow i').addClass("fa-eye");
                }
            });
        });

        $(document).ready(function () {
            $("#txtUserPasswordRepeatShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtUserPasswordRepeatShow input').attr("type") == "text") {
                    $('#txtUserPasswordRepeatShow input').attr('type', 'password');
                    $('#txtUserPasswordRepeatShow i').addClass("fa-eye-slash");
                    $('#txtUserPasswordRepeatShow i').removeClass("fa-eye");
                } else if ($('#txtUserPasswordRepeatShow input').attr("type") == "password") {
                    $('#txtUserPasswordRepeatShow input').attr('type', 'text');
                    $('#txtUserPasswordRepeatShow i').removeClass("fa-eye-slash");
                    $('#txtUserPasswordRepeatShow i').addClass("fa-eye");
                }
            });
        });


        $(document).ready(function () {
            $("#txtChangePasswordUserPasswordShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtChangePasswordUserPasswordShow input').attr("type") == "text") {
                    $('#txtChangePasswordUserPasswordShow input').attr('type', 'password');
                    $('#txtChangePasswordUserPasswordShow i').addClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordShow i').removeClass("fa-eye");
                } else if ($('#txtChangePasswordUserPasswordShow input').attr("type") == "password") {
                    $('#txtChangePasswordUserPasswordShow input').attr('type', 'text');
                    $('#txtChangePasswordUserPasswordShow i').removeClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordShow i').addClass("fa-eye");
                }
            });
        });

        $(document).ready(function () {
            $("#txtChangePasswordUserPasswordRepeatShow a").on('click', function (event) {
                event.preventDefault();
                if ($('#txtChangePasswordUserPasswordRepeatShow input').attr("type") == "text") {
                    $('#txtChangePasswordUserPasswordRepeatShow input').attr('type', 'password');
                    $('#txtChangePasswordUserPasswordRepeatShow i').addClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordRepeatShow i').removeClass("fa-eye");
                } else if ($('#txtChangePasswordUserPasswordRepeatShow input').attr("type") == "password") {
                    $('#txtChangePasswordUserPasswordRepeatShow input').attr('type', 'text');
                    $('#txtChangePasswordUserPasswordRepeatShow i').removeClass("fa-eye-slash");
                    $('#txtChangePasswordUserPasswordRepeatShow i').addClass("fa-eye");
                }
            });
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:LinkButton ID="lnkAddNewUserModal" OnClick="lnkAddNewUserModal_Click" CssClass="btn btn-primary" runat="server">
            <i class ="fa fa-user-plus"></i> Yeni istifadəçi</asp:LinkButton>
            <div class="mb40"></div>
            <div class="mb40"></div>

            <div class="table-responsive">
                <asp:GridView ID="grdUsers" class="table table-bordered table-hover table-primary table-striped nomargin" runat="server" AutoGenerateColumns="False" DataKeyNames="RECORD_ID,STATUS" GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="USER_NAME" HeaderText="İSTİFADƏÇİ ADI" />
                        <asp:BoundField DataField="NAME" HeaderText="ADI" />
                        <asp:BoundField DataField="SURNAME" HeaderText="SOYADI" />
                        <asp:BoundField DataField="CONTACTNUMBER" HeaderText="ƏLAQƏ NÖMRƏSİ" />
                        <asp:BoundField DataField="POSITION" HeaderText="VƏZİFƏSİ" />
                        <asp:TemplateField HeaderText="STATUS">
                            <ItemTemplate>
                                <img src='<%#Convert.ToString(Eval("STATUS")) == "ACTIVE" ? "/img/ACCEPT24.png" : "/img/CANCEL24.png" %>' title='<%#Convert.ToString(Eval("STATUS")) == "ACTIVE" ? "Aktiv" : "Blok" %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            <HeaderStyle />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <ul class="table-options">
                                    <li>
                                        <asp:LinkButton ID="lnkEditUser" OnClick ="lnkEditUser_Click"
                                            title="Redaktə et" runat="server"
                                            class="fa fa-pencil" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                        </asp:LinkButton>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="lnkBlockUser" OnClick ="lnkBlockUser_Click"
                                            title='<%#Convert.ToString(Eval("STATUS")) == "ACTIVE" ? "Blok et" : "Blokdan çıxar"%>' runat="server"
                                            class='<%#Convert.ToString(Eval("STATUS")) == "ACTIVE" ? "fa fa-unlock" : "fa fa-lock"%>' CommandArgument='<%#Eval("RECORD_ID") %>'>
                                        </asp:LinkButton>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="lnkDeleteUser" OnClick ="lnkDeleteUser_Click"
                                            title="Sil" runat="server"
                                            class="fa fa-trash" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="100px" />
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>
                <asp:HiddenField ID="hdnDeleteUser" runat="server" />
                <asp:HiddenField ID="hdnBlockUser" runat="server" />
                <asp:HiddenField ID="hdnUserStatus" runat="server" />
                <asp:HiddenField ID="hdnEditUser" runat="server" />
            </div>


            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Modal  AddUser-->
                    <div class="modal bounceIn" id="modalAddUser" data-backdrop="static">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="modalAddUserLabel"><i class="icon ion-person-add"></i>&nbsp&nbspYeni istifadəçi</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                Soyad :
                                                <asp:TextBox ID="txtUserSurname" autocomplete="off" placeholder="Soyad" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                İstifadəçi adı:
                                                <asp:TextBox ID="txtUserLogin" autocomplete="off" MaxLength="51" placeholder="İstifadəçi adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                            Şifrə:
                                            <div class="form-group input-group" id="txtUserPasswordShow">
                                                <asp:TextBox ID="txtUserPassword" autocomplete="off" placeholder="Şifrə" runat="server" MaxLength="50" class="form-control" TextMode="Password"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            Şifrə təkrar:
                                            <div class="form-group input-group" id="txtUserPasswordRepeatShow">
                                                <asp:TextBox ID="txtUserPasswordRepeat" autocomplete="off" placeholder="Şifrə təkrar" runat="server" MaxLength="50" class="form-control" TextMode="Password"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                Ad :
                                                <asp:TextBox ID="txtUserName" autocomplete="off" placeholder="Ad" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                Əlaqə nömrəsi:
                                                <asp:TextBox ID="txtContactNumber" autocomplete="off" type="text" placeholder="Əlaqə nömrəsi" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                Cinsi:
                                                <asp:DropDownList ID="drlUserSex" class="form-control" runat="server">
                                                    <asp:ListItem Value="" Selected="True">Cinsi</asp:ListItem>
                                                    <asp:ListItem Value="M">Kişi</asp:ListItem>
                                                    <asp:ListItem Value="F">Qadın</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                Vəzifə:
                                                <asp:TextBox ID="txtPosition" type="text" placeholder="Vəzifə" runat="server" class="form-control" MaxLength="80"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb30"></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="drlUserType" class="form-control" runat="server">
                                                <asp:ListItem Value="" Selected="True">İstifadəçi tipi</asp:ListItem>
                                                <asp:ListItem Value="admin">admin</asp:ListItem>
                                                <asp:ListItem Value="user">user</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <img id="add_user_loading" style="display: none" src="../img/loader1.gif" />
                                    <asp:Button ID="btnUserAdd" class="btn btn-primary" runat="server" Text="Əlavə et"
                                        OnClick="btnUserAdd_Click"
                                        OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_user_loading').style.display = '';
                                                           document.getElementById('btnUserAddCancel').style.display = 'none';" />
                                    <button id="btnUserAddCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUserAdd" />
                </Triggers>
            </asp:UpdatePanel>



            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Modal  EditUser-->
                    <div class="modal bounceIn" id="modalEditUser" data-backdrop="static">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="modalEditUserLabel"><i class="icon ion-edit"></i>&nbsp&nbspİstifadəçi yenilənməsi</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                Soyad :
                                                <asp:TextBox ID="txtEditUserSurname" autocomplete="off" placeholder="Soyad" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                İstifadəçi adı:
                                                <asp:TextBox ID="txtEditUserLogin" ReadOnly autocomplete="off" MaxLength="51" placeholder="İstifadəçi adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                Cinsi:
                                                <asp:DropDownList ID="drlEditUserSex" class="form-control" runat="server">
                                                    <asp:ListItem Value="" Selected="True">Cinsi</asp:ListItem>
                                                    <asp:ListItem Value="M">Kişi</asp:ListItem>
                                                    <asp:ListItem Value="F">Qadın</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                Ad :
                                                <asp:TextBox ID="txtEditUserName" autocomplete="off" placeholder="Ad" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                Əlaqə nömrəsi:
                                                <asp:TextBox ID="txtEditContactNumber" autocomplete="off" type="text" placeholder="Əlaqə nömrəsi" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                            </div>

                                            <div class="form-group">
                                                Vəzifə:
                                                <asp:TextBox ID="txtEditPosition" type="text" placeholder="Vəzifə" runat="server" class="form-control" MaxLength="80"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb30"></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="drlEditUserType" class="form-control" runat="server">
                                                <asp:ListItem Value="" Selected="True">İstifadəçi tipi</asp:ListItem>
                                                <asp:ListItem Value="admin">admin</asp:ListItem>
                                                <asp:ListItem Value="user">user</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <img id="edit_user_loading" style="display: none" src="../img/loader1.gif" />
                                    <asp:Button ID="btnUserEdit" class="btn btn-primary" runat="server" Text="Yadda saxla"
                                        OnClick ="btnUserEdit_Click"
                                        OnClientClick="this.style.display = 'none';
                                                           document.getElementById('edit_user_loading').style.display = '';
                                                           document.getElementById('btnUserEditCancel').style.display = 'none';" />
                                    <button id="btnUserEditCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUserEdit" />
                </Triggers>
            </asp:UpdatePanel>


            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Modal  Alert-->
                    <div class="modal bounceIn" id="modalAlert" data-backdrop="static">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="modalAlertLabel">
                                        <asp:Literal ID="ltrAlertModalHeaderLabel" runat="server"></asp:Literal>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <asp:Literal ID="ltrAlertModalBodyLabel" runat="server"></asp:Literal>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <img id="change_userstatus_loading" style="display: none" src="../img/loader1.gif" />
                                    <asp:Button ID="btnUserChangeStatus" class="btn btn-primary" runat="server" Text="Bəli" 
                                        OnClick ="btnUserChangeStatus_Click"
                                        OnClientClick="this.style.display = 'none';
                                                           document.getElementById('change_userstatus_loading').style.display = '';
                                                           document.getElementById('btnUserChangeStatusCancel').style.display = 'none';" />
                                    <button id="btnUserChangeStatusCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUserChangeStatus" />
                </Triggers>
            </asp:UpdatePanel>




        </asp:View>
    </asp:MultiView>


    <script type="text/javascript">

        function openAddUserModal() {
            $('#modalAddUser').modal({ show: true });
        }

        function openEditUserModal() {
            $('#modalEditUser').modal({ show: true });
        }

        function openAlertModal() {
            $('#modalAlert').modal({ show: true });
        }

    </script>


</asp:Content>

