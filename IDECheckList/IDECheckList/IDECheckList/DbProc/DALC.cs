﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IDECheckList.DbProc
{
    public class DALC
    {
        public static SqlConnection SqlConn
        {
            get
            {
                try
                {
                    return new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}