﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IDECheckList.DbProc
{
    public partial class DbProcess : DALC
    {

        public DataTable GetSalesAgentList()
        {
            DataTable dt = new DataTable("IDE_CHLIST_SALESAGENTS");
            SqlDataAdapter da = new SqlDataAdapter("select * FROM IDE_CHLIST_SALESAGENTS where status = 'ACTIVE' order by CREATED_DATE desc", SqlConn);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }


        public string AddSalesAgents(string pCode,
                          string pName,
                          string pNote,
                          short pCreatedBy_UserRecId)
        {
            SqlCommand cmd = new SqlCommand("spAddSalesAgents", SqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@pCode", pCode);
            cmd.Parameters.AddWithValue("@pName", pName);
            cmd.Parameters.AddWithValue("@pNote", pNote);
            cmd.Parameters.AddWithValue("@pCreatedBy_UserRecId", pCreatedBy_UserRecId);


            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return pResult;
        }



        public DataRow GetSalesAgentById(short recID)
        {
            DataTable dt = new DataTable("IDE_CHLIST_SALESAGENTS");
            SqlDataAdapter da = new SqlDataAdapter("select * FROM IDE_CHLIST_SALESAGENTS where RECORD_ID = @id", SqlConn);
            try
            {
                da.SelectCommand.Parameters.AddWithValue("@id", recID);
                da.Fill(dt);
            }
            catch
            {
                return null;
            }
            if (dt.Rows.Count == 0) return null;
            return dt.Rows[0];
        }


        public string UpdateSalesAgents(short pRecID,
                          string pCode,
                          string pName,
                          string pNote,
                          short pModifiedBy_UserRecId)
        {
            SqlCommand cmd = new SqlCommand("spUpdateSalesAgents", SqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("pRecID", pRecID);
            cmd.Parameters.AddWithValue("@pCode", pCode);
            cmd.Parameters.AddWithValue("@pName", pName);
            cmd.Parameters.AddWithValue("@pNote", pNote);
            cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);


            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return pResult;
        }



        public string DeleteSalesAgent(short pRecID,
                                     short pModifiedBy_UserRecId)
        {
            SqlCommand cmd = new SqlCommand("spDeleteSalesAgents", SqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@pRecID", pRecID);
            cmd.Parameters.AddWithValue("@pModifiedBy_UserRecId", pModifiedBy_UserRecId);


            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return pResult;
        }

    }
}