﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IDECheckList.SalesChannels.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <asp:LinkButton ID="lnkAddNewUserModal" CssClass="btn btn-success" runat="server"
        OnClick="lnkAddNewUserModal_Click">
            <i class ="fa fa-plus"></i> Yeni Satış kanalı</asp:LinkButton>
    <div class="mb40"></div>
    <div class="mb40"></div>

    <div class="table-responsive">
        <asp:GridView ID="grdChannels" class="table table-bordered table-hover table-success table-striped nomargin" runat="server" AutoGenerateColumns="False" DataKeyNames="RECORD_ID" GridLines="None">
            <Columns>
                <asp:BoundField DataField="CODE" HeaderText="KODU" />
                <asp:BoundField DataField="NAME" HeaderText="ADI" />
                <asp:BoundField DataField="NOTE" HeaderText="QEYD" />


                <asp:TemplateField>
                    <ItemTemplate>
                        <ul class="table-options">
                            <li>
                                <asp:LinkButton ID="lnkEditChannels" OnClick="lnkEditChannels_Click"
                                    title="Redaktə et" runat="server"
                                    class="fa fa-pencil" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                </asp:LinkButton>
                            </li>

                            <li>
                                <asp:LinkButton ID="lnkDeleteChannels" OnClick ="lnkDeleteChannels_Click"
                                    title="Sil" runat="server"
                                    class="fa fa-trash" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                </asp:LinkButton>
                            </li>
                        </ul>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </asp:TemplateField>
            </Columns>

        </asp:GridView>
        <asp:HiddenField ID="hdnDeleteChannels" runat="server" />
        <asp:HiddenField ID="hdnEditChannels" runat="server" />
    </div>




    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  AddChannel-->
            <div class="modal bounceIn" id="modalAddChannel" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalAddUserLabel"><i class="icon ion-person-add"></i>&nbsp&nbspYeni kanal</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        Kodu :
                                        <asp:TextBox ID="txtAddChannelCode" autocomplete="off" placeholder="Kodu" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        Adı:
                                       <asp:TextBox ID="txtAddChannelName" autocomplete="off" MaxLength="150" placeholder="Adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                        Qeyd:
                                       <asp:TextBox ID="txtAddChannelNote" TextMode="MultiLine" autocomplete="off" MaxLength="300" placeholder="Qeyd" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <img id="add_channel_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnChannelAdd" class="btn btn-success" runat="server" Text="Əlavə et"
                                OnClick="btnChannelAdd_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_channel_loading').style.display = '';
                                                           document.getElementById('btnChannelAddCancel').style.display = 'none';" />
                            <button id="btnChannelAddCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnChannelAdd" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  EditChannel-->
            <div class="modal bounceIn" id="modalEditChannel" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalEditUserLabel"><i class="icon ion-edit"></i>&nbsp&nbspKanalın yenilənməsi</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        Kodu :
                                        <asp:TextBox ID="txtEditChannelCode" autocomplete="off" placeholder="Kodu" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        Adı:
                                       <asp:TextBox ID="txtEditChannelName" autocomplete="off" MaxLength="150" placeholder="Adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                        Qeyd:
                                       <asp:TextBox ID="txtEditChannelNote" TextMode="MultiLine" autocomplete="off" MaxLength="300" placeholder="Qeyd" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <img id="Edit_channel_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnChannelEdit" class="btn btn-success" runat="server" Text="Yenilə"
                                OnClick="btnChannelEdit_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('Edit_channel_loading').style.display = '';
                                                           document.getElementById('btnChannelEditCancel').style.display = 'none';" />
                            <button id="btnChannelEditCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnChannelEdit" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  Alert-->
            <div class="modal bounceIn" id="modalAlert" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalAlertLabel">
                                <asp:Literal ID="ltrAlertModalHeaderLabel" runat="server"></asp:Literal>
                        </div>
                        <div class="modal-body">
                            <p>
                                <asp:Literal ID="ltrAlertModalBodyLabel" runat="server"></asp:Literal>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <img id="delete_channel_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnChannelDelete" class="btn btn-success" runat="server" Text="Bəli"
                                OnClick ="btnChannelDelete_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_channel_loading').style.display = '';
                                                           document.getElementById('btnChannelDeleteCancel').style.display = 'none';" />
                            <button id="btnChannelDeleteCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnChannelDelete" />
        </Triggers>
    </asp:UpdatePanel>



    <script type="text/javascript">

        function openAlertModal() {
            $('#modalAlert').modal({ show: true });
        }

        function openAddChannelModal() {
            $('#modalAddChannel').modal({ show: true });
        }

        function openEditChannelModal() {
            $('#modalEditChannel').modal({ show: true });
        }

    </script>


</asp:Content>

