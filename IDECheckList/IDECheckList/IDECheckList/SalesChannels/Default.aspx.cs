﻿using IDECheckList.App_Code;
using IDECheckList.DbProc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IDECheckList.App_Code._Config;

namespace IDECheckList.SalesChannels
{
    public partial class Default : System.Web.UI.Page
    {

        UserData u_data = new UserData();
        DbProcess db = new DbProcess();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserData"] == null) _Config.Rd("/exit");
            u_data = (UserData)Session["UserData"];
            if (!IsPostBack)
            {
                FillChannels();
            }
            if (grdChannels.Rows.Count > 0) grdChannels.HeaderRow.TableSection = TableRowSection.TableHeader;
        }


        void FillChannels()
        {
            grdChannels.DataSource = db.GetChannelList();
            grdChannels.DataBind();
            grdChannels.UseAccessibleHeader = true;
         if (grdChannels.Rows.Count > 0)   grdChannels.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void lnkAddNewUserModal_Click(object sender, EventArgs e)
        {
            txtAddChannelCode.Text = "";
            txtAddChannelName.Text = "";
            txtAddChannelNote.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddChannelModal();", true);
        }

        protected void btnChannelAdd_Click(object sender, EventArgs e)
        {
            if (txtAddChannelCode.Text.Trim().Length < 5)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 5 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddChannelModal();", true);
                return;
            }


            if (txtAddChannelName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddChannelModal();", true);
                return;
            }


            string result = db.AddChannels(txtAddChannelCode.Text.Trim(), txtAddChannelName.Text.Trim(), txtAddChannelNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddChannelModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddChannelModal();", true);
                return;
            }

            _Config.Rd("/saleschannels");


        }


        //edit
        protected void lnkEditChannels_Click(object sender, EventArgs e)
        {
            LinkButton lnkEditChannel = (LinkButton)sender;
            hdnEditChannels.Value = lnkEditChannel.CommandArgument;
            FillChannelDataById(Convert.ToInt16(hdnEditChannels.Value));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditChannelModal();", true);
        }


        private void FillChannelDataById(short editChannelID)
        {
            DataRow drEditChannel = db.GetChannelById(editChannelID);
            if (drEditChannel != null)
            {
                txtEditChannelCode.Text = Convert.ToString(drEditChannel["CODE"]);
                txtEditChannelName.Text = Convert.ToString(drEditChannel["NAME"]);
                txtEditChannelNote.Text = Convert.ToString(drEditChannel["NOTE"]);
            }
        }

        protected void btnChannelEdit_Click(object sender, EventArgs e)
        {
            short editChannelId = Convert.ToInt16(hdnEditChannels.Value);
            if (txtEditChannelCode.Text.Trim().Length < 5)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 5 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditChannelModal();", true);
                return;
            }


            if (txtEditChannelName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditChannelModal();", true);
                return;
            }


            string result = db.UpdateChannels(editChannelId, txtEditChannelCode.Text.Trim(), txtEditChannelName.Text.Trim(), txtEditChannelNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditChannelModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditChannelModal();", true);
                return;
            }

            _Config.Rd("/saleschannels");


        }

        //delete
        protected void lnkDeleteChannels_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspKanalın silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "Kanal silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnDeleteChannels.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
        }

        protected void btnChannelDelete_Click(object sender, EventArgs e)
        {
            string result = db.DeleteChannels(Int16.Parse(hdnDeleteChannels.Value), (Int16)u_data.LogonUserID);

            if (result != "OK")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
                return;
            }
         
            _Config.Rd("/saleschannels");
        }
    }
}