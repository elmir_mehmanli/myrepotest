﻿using System;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace IDECheckList.App_Code
{
    public class _Config
    {
       
        public decimal ToDecimal(object value)
        {
            try
            {
                string _value = Convert.ToString(value);
                decimal _a = 5m / 4m;
                string _splitter = "";
                if (_a.ToString().IndexOf(".") > -1) _splitter = ".";
                else if (_a.ToString().IndexOf(",") > -1) _splitter = ",";
                string value2 = _value;
                value2 = _value.Replace(".", _splitter);
                value2 = _value.Replace(",", _splitter);
                return Convert.ToDecimal(value2);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public static bool isLoginSymbols(String key)
        {
            string bind = "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM.@-_";
            for (int i = 0; i < key.Length; i++)
            {
                if (bind.IndexOf(key[i]) == -1) return false;
            }
            return true;
        }

        public double ToDouble(object value)
        {
            try
            {
                string _value = Convert.ToString(value);
                decimal _a = 5m / 4m;
                string _splitter = "";
                if (_a.ToString().IndexOf(".") > -1) _splitter = ".";
                else if (_a.ToString().IndexOf(",") > -1) _splitter = ",";
                string value2 = _value;
                value2 = _value.Replace(".", _splitter);
                value2 = _value.Replace(",", _splitter);
                return Convert.ToDouble(value2);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  float ToFloat(object Value)
        {
            string _value = Convert.ToString(Value);
            try
            {
                return float.Parse(_value);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        

      

        
        public static DateTime HostingTime
        {
            get
            {
                return DateTime.Now.AddHours(0);
            }
        }
        
        public static void PageSettings()
        {
            HttpContext.Current.Response.Cache.SetNoStore(); //Templəri bağlayırıq...
            HttpContext.Current.Session.Timeout = 10080;
            HttpContext.Current.Server.ScriptTimeout = 9999; //Əgər yüklənmə gecikərsə maksimum gözləmə saniyəsi.
            System.Threading.Thread.Sleep(0);
        }

        //Get WebConfig.config App Key
        public  string GetAppSetting(string KeyName)
        {
            return Convert.ToString(ConfigurationManager.AppSettings[KeyName]);
        }

        //Fileupload-da gələn şəkilin ölçüsünü kəsir (100x??px).
        public static Unit PicturesSizeSplit(string s)
        {
            try
            {
                int i = Convert.ToInt16(s.Substring(0, 3).Trim().Trim('x'));
                if (i < 220)
                    return Unit.Pixel(i);
                else return Unit.Pixel(220);
            }
            catch { return Unit.Pixel(220); }
        }

       

       


        public static string MonthToText(string Mont)
        {
            if (Mont == "01") return "Yanvar";
            if (Mont == "02") return "Fevral";
            if (Mont == "03") return "Mart";
            if (Mont == "04") return "Aprel";
            if (Mont == "05") return "May";
            if (Mont == "06") return "İyun";
            if (Mont == "07") return "İyul";
            if (Mont == "08") return "Avqust";
            if (Mont == "09") return "Sentyabr";
            if (Mont == "10") return "Oktyabr";
            if (Mont == "11") return "Noyabr";
            if (Mont == "12") return "Dekabr";

            return "--";
        }

        public static string DateTimeFormat(DateTime Dt)
        {
            return Dt.ToString("dd") + " " + MonthToText(Dt.ToString("MM")) + " " + Dt.ToString("yyyy");
        }

        public static string ClearInjection(string x)
        {
            x = x.Replace("<", "&lt;");
            x = x.Replace(">", "&gt;");
            x = x.Replace("`", "");
            x = x.Replace("'", "");
            x = x.Replace("\"", "");
            x = x.Replace("?", "");
            x = x.Replace("!", "");
            x = x.Replace("/", "");
            x = x.Replace("\\", "");
            x = x.Replace("&", "");
            x = x.Replace("#", "");
            x = x.Replace("~", "");
            x = x.Replace("%", "");
            x = x.Trim('.');
            x = x.Trim();
            return x;
        }

        
        //Numaric testi.
        public static bool IsNumeric(string s)
        {
            if (s == null) return false;
            if (s.Length < 1) return false;
            for (int i = 0; i < s.Length; i++)
            {
                if ("0123456789".IndexOf(s.Substring(i, 1)) < 0) { return false; }
            }
            return true;
        }

       
        //Sha1  - 
        public static string Sha1(string Password)
        {
            byte[] result;
            System.Security.Cryptography.SHA1 ShaEncrp = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            Password = String.Format("{0}{1}{0}", "CSAASADM", Password);
            byte[] buffer = new byte[Password.Length];
            buffer = System.Text.Encoding.UTF8.GetBytes(Password);
            result = ShaEncrp.ComputeHash(buffer);
            return Convert.ToBase64String(result);
        }

        //Səhifəni yönləndirək:
        public static void Rd(string GetUrl)
        {
            HttpContext.Current.Response.Redirect(GetUrl, true);
            HttpContext.Current.Response.End();
        }


     

        

        

        public  bool isDate(string dateFormat)
        {
            string tarih = dateFormat;
            DateTime date;
            string pattern = "dd.MM.yyyy";
            bool rihtDate = DateTime.TryParseExact(tarih, pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
            return rihtDate;
        }

       
        public static bool isEnglish(String key)
        {
            string bind = "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
            for (int i = 0; i < key.Length; i++)
            {
                if (bind.IndexOf(key[i]) == -1) return false;
            }
            return true;
        }

      
        public static DateTime ToDate(string date)
        {
            DateTime dt;
            string pattern = "dd.MM.yyyy";
            DateTime.TryParseExact(date, pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            return dt;
        }

        public  void AlertMessage(Page control, MessageType _messageType, string message)
        {
            string mType = "", title = ""; ;
            if (_messageType == MessageType.SUCCESS)
            {
                mType = "success";
                title = "İNFORMASİYA";
            }
            if (_messageType == MessageType.ERROR)
            {
                mType = "danger";
                title = "XƏTA";
            }
            if (_messageType == MessageType.WARNING)
            {
                mType = "warning";
                title = "XƏBƏRDARLIQ";
            }
            control.ClientScript.RegisterStartupScript(control.GetType(), "jsAlert", "jsAlert('" + title + "','" + message + "','"+ mType +"');", true);
            
        }



        public void AlertMessageNew(Page control, MessageType _messageType, string message)
        {
             control = (Page)HttpContext.Current.Handler; 
            
            string mType = "", title = ""; ;
            if (_messageType == MessageType.SUCCESS)
            {
                mType = "success";
                title = "İNFORMASİYA";
            }
            if (_messageType == MessageType.ERROR)
            {
                mType = "danger";
                title = "XƏTA";
            }
            if (_messageType == MessageType.WARNING)
            {
                mType = "warning";
                title = "XƏBƏRDARLIQ";
            }
            ScriptManager.RegisterStartupScript(control, typeof(string), "jsAlert" + System.Guid.NewGuid().ToString(), "jsAlert('" + title + "','" + message + "','" + mType + "');", true);

        }


        public  enum MessageType
        {
            SUCCESS,
            ERROR,
            WARNING
        }



        public string ConvertMoneyToWord(string money)
        {
            string result = "";
            decimal _money = Convert.ToDecimal(money);
            string toWord = ConvertToWord(_money);
            string[] split = Regex.Split(toWord, @"(?<!^)(?=[A-Z])");
            result = String.Join(" ", split);
            result = result.Replace("Wyirmi", "İyirmi").Replace("Alli", "Əlli").Replace("Uç", "Üç").Replace("Iki", "İki");
            if (result.Trim().Length > 1)
            {
                result = result.ToLower();
            }

            result = result.Replace("manat", result.IndexOf("qəpik") > -1 ? ",":"").Trim();
            result = result.Replace("qəpik", "").Trim();
            return result;
        }


        public string ConvertMonthToWord(string month)
        {
            string toWord = ConvertMoneyToWord(month); ;
            toWord = toWord.Replace("manat", "").Trim();
            toWord = toWord.Replace("qəpik", "").Trim();
            return toWord;
        }

        private string ConvertToWord(decimal tutar)
        {
            string sTutar = tutar.ToString("F2").Replace('.', ','); // Replace('.',',') ondalık ayracının . olma durumu için            
            string lira = sTutar.Substring(0, sTutar.IndexOf(',')); //tutarın tam kısmı
            string kurus = sTutar.Substring(sTutar.IndexOf(',') + 1, 2);
            string yazi = "";

            string[] birler = { "", "Bir", "Iki", "Uç", "Dörd", "Beş", "Altı", "Yeddi", "Səkkiz", "Doqquz" };
            string[] onlar = { "", "On", "Wyirmi", "Otuz", "Qırx", "Alli", "Altmış", "Yetmiş", "Səksən", "Doxsan" };
            string[] binler = { "Kvadrilyon", "Trilyon", "Milyard", "Milyon", "Min", "" }; //KATRİLYON'un önüne ekleme yapılarak artırabilir.

            int grupSayisi = 6; //sayıdaki 3'lü grup sayısı. katrilyon içi 6. (1.234,00 daki grup sayısı 2'dir.)
                                //KATRİLYON'un başına ekleyeceğiniz her değer için grup sayısını artırınız.

            lira = lira.PadLeft(grupSayisi * 3, '0'); //sayının soluna '0' eklenerek sayı 'grup sayısı x 3' basakmaklı yapılıyor.            

            string grupDegeri;

            for (int i = 0; i < grupSayisi * 3; i += 3) //sayı 3'erli gruplar halinde ele alınıyor.
            {
                grupDegeri = "";

                if (lira.Substring(i, 1) != "0")
                    grupDegeri += birler[Convert.ToInt32(lira.Substring(i, 1))] + "Yüz"; //yüzler                

                if (grupDegeri.Trim() == "BirYüz") //biryüz düzeltiliyor.
                    grupDegeri = "Yüz";

                grupDegeri += onlar[Convert.ToInt32(lira.Substring(i + 1, 1))]; //onlar

                grupDegeri += birler[Convert.ToInt32(lira.Substring(i + 2, 1))]; //birler                

                if (grupDegeri.Trim() != "") //binler
                    grupDegeri += binler[i / 3];

                if (grupDegeri.Trim() == "BirMin") //birbin düzeltiliyor.
                    grupDegeri = "Min";

                yazi += grupDegeri;
            }

            if (yazi != "")
                yazi += " Manat ";

            int yaziUzunlugu = yazi.Length;

            if (kurus.Substring(0, 1) != "0") //kuruş onlar
                yazi += onlar[Convert.ToInt32(kurus.Substring(0, 1))];

            if (kurus.Substring(1, 1) != "0") //kuruş birler
                yazi += birler[Convert.ToInt32(kurus.Substring(1, 1))];

            if (yazi.Length > yaziUzunlugu)
                yazi += "Qəpik";
            else
                yazi += "";

            return yazi.Trim();
        }

        


    }
}