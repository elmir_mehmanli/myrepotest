﻿using IDECheckList.App_Code;
using IDECheckList.DbProc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IDECheckList
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserData u_data = new UserData();
            DbProcess db = new DbProcess();

            if (!IsPostBack)
            {
                if (Session["UserData"] == null) _Config.Rd("/exit");
                u_data = (UserData)Session["UserData"];

                ltrUserHeaderImage.Text = string.Format(@"<img src=""{0}"" alt="""" />", u_data.LoginUserImage.Trim());
                ltrUserHeaderImage_1.Text = string.Format(@" <img src=""{0}"" alt=""""  class=""media-object img-circle"">", u_data.LoginUserImage.Trim());

                ltrUserHeaderName.Text = u_data.LoginUserName;
                ltrUserHeaderName_1.Text = u_data.LoginUserName;
                ltrUserPosition.Text = u_data.LoginUserType;


                //Left Menu Begin
                DataTable dtMenu = db.GetMenu("az", u_data.LoginUserType);
                if (dtMenu == null || dtMenu.Rows.Count == 0) return;

                IEnumerable<DataRow> queryMainMenu =
                from mainMenu in dtMenu.AsEnumerable()
                where mainMenu.Field<Int16>("PARENT_ID") == 0
                orderby mainMenu.Field<Int16>("PRIORITY")
                select mainMenu;
                
                DataTable dtMainMenu = queryMainMenu.CopyToDataTable<DataRow>();

                if (dtMainMenu == null || dtMainMenu.Rows.Count == 0) return;
                ltrLeftMenu.Text = "";

                string activeMain = "", activeSub = "";
                string activeMainNameHeader = "", activeSubNameHeader = "";
                string iconCell = "";

                string req_url = "";

                foreach (DataRow drMainMenu in dtMainMenu.Rows)
                {
                    string nav_parent = "";
                    activeMain = "";

                    DataTable dtSubMenu = new DataTable("dtSubMenu");
                    IEnumerable<DataRow> querySubMenu =
                       from subMenu in dtMenu.AsEnumerable()
                       where subMenu.Field<Int16>("Parent_ID") == Convert.ToInt32(drMainMenu["ID"])
                       orderby subMenu.Field<Int16>("ID")
                       select subMenu;
                    if (querySubMenu.Any())
                    {
                        dtSubMenu = querySubMenu.CopyToDataTable<DataRow>();
                        nav_parent = dtSubMenu.Rows.Count > 0 ? "nav-parent" : "";
                    }



                    /*for active menu*/
                    try { req_url = Request.Url.Segments[1].ToLower().Replace("/", ""); }
                    catch { req_url = ""; }

                    if (req_url != "")
                    {
                        foreach (DataRow drSubMenu1 in dtSubMenu.Rows)
                        {
                            string url_sub = Convert.ToString(drSubMenu1["URL"]).ToLower().Replace("/", "");
                            if (url_sub == req_url)
                            {
                                activeMain = " active";
                                activeMainNameHeader = Convert.ToString(drMainMenu["NAME"]);
                            }
                        }

                        string url_main = Convert.ToString(drMainMenu["URL"]).ToLower().Replace("/", "");
                        if (url_main == req_url)
                        {
                            activeMain = " active";
                            activeMainNameHeader = Convert.ToString(drMainMenu["NAME"]);
                        }

                    }



                    ltrLeftMenu.Text +=
                    @"<li class=""" + nav_parent + "" + activeMain + "\">";

                    iconCell = "";
                    if (Convert.ToString(drMainMenu["ICON"]) != "") iconCell = "<i class = \"" + Convert.ToString(drMainMenu["ICON"]) + "\"></i>";

                    ltrLeftMenu.Text +=
                    "<a href=\"" + Convert.ToString(drMainMenu["URL"]) + "\">" + iconCell + "<span>" + Convert.ToString(drMainMenu["NAME"]) + "</span></a>";
                    if (dtSubMenu.Rows.Count > 0)
                    {
                        activeSub = "";
                        ltrLeftMenu.Text +=
                        "<ul class=\"children\">";
                        foreach (DataRow drSubMenu in dtSubMenu.Rows)
                        {
                            string url_sub = Convert.ToString(drSubMenu["URL"]).ToLower().Replace("/", "");
                            if (url_sub == req_url)
                            {
                                activeSub = " class =  \"active\"";
                                activeSubNameHeader = Convert.ToString(drSubMenu["NAME"]);
                            }
                            else activeSub = "";
                            ltrLeftMenu.Text += "<li" + activeSub + "><a href=\"" + Convert.ToString(drSubMenu["URL"]) + "\">" + Convert.ToString(drSubMenu["NAME"]) + "</a></li>";
                        }
                        ltrLeftMenu.Text += "</ul>";
                    }
                    ltrLeftMenu.Text += "</li>";

                }


                if (activeSubNameHeader == "")
                {
                    ltrContentHeaderMenu1.Text = string.Format("<li class=\"active\">{0}</li>", activeMainNameHeader);
                    ltrContentHeaderMenu2.Text = "";
                }
                else
                {
                    ltrContentHeaderMenu1.Text = string.Format("<li>{0}</li>", activeMainNameHeader);
                    ltrContentHeaderMenu2.Text = string.Format("<li class=\"active\">{0}</li>", activeSubNameHeader); ;
                }

                if (req_url == "index")
                {
                    ltrContentHeaderMenu1.Text = ltrContentHeaderMenu2.Text = "";
                    ltrContentHeaderMenuMain.Text = "<li class = \"active\"><a href=\"/index\"><i class=\"fa fa-home mr5\"></i>IDE CheckList</a></li>";
                }
                if (req_url == "changepassword")
                {
                    ltrContentHeaderMenu1.Text = string.Format("<li class=\"active\">{0}</li>", "Şifrəni dəyiş");
                    ltrContentHeaderMenu2.Text = "";
                }
                else
                {
                    ltrContentHeaderMenuMain.Text = "<li><a href=\"/index\"><i class=\"fa fa-home mr5\"></i>IDE CheckList</a></li>";
                }

               


            }
        }
    }
}