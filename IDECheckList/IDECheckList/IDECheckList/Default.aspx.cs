﻿using IDECheckList.App_Code;
using System;
using static IDECheckList.App_Code._Config;
using IDECheckList.DbProc;

namespace IDECheckList
{
    public partial class Default : System.Web.UI.Page
    {
        _Config _config = new _Config();
        DbProcess db = new DbProcess();
        UserData u_data = new UserData();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserSession"] != null) _Config.Rd("/index");
                ImgSecurity.ImageUrl = "~/Captcha/?" + new Random().Next().ToString();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtLogin.Text.Length < 3)
            {
                _config.AlertMessage(this, MessageType.ERROR, "İstifadəçi adını daxil edin!");
                txtLogin.Focus();
                return;
            }

            if (txtPassword.Text.Length < 5)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Şifrəni daxil edin!");
                txtPassword.Focus();
                return;
            }

            string security_code = txtSecurity.Text;
            txtSecurity.Text = "";
            ImgSecurity.ImageUrl = "~/Captcha/?" + new Random().Next().ToString();

            string Er = "";
            if (security_code.Length < 1) Er = "*";
            if (Convert.ToString(Session["SecurityKod"]).Length < 1) Er = "*";
            if (Convert.ToString(Session["SecurityKod"]) != security_code) Er = "*";

            if (Er == "*")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Təhlükəsizlik kodu düzgün deyil.");
                return;
            }




            string pUserInfo = "";
            string login_result = db.dbLogin(txtLogin.Text, txtPassword.Text, ref pUserInfo);

            if (login_result != "OK")
            {
                _config.AlertMessage(this, MessageType.ERROR, "İstifadəçi adı və ya şifrə düzgün deyil");
                return;
            }

            string[] uInfo = pUserInfo.Split('#');
            try
            {
                if (uInfo.Length != 4)
                {
                   _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                    return;
                }
                u_data.LogonUserID = Convert.ToInt32(uInfo[0].Trim());
                u_data.LoginUserName = Convert.ToString(uInfo[1].Trim());
                u_data.LoginUserImage = Convert.ToString(uInfo[2].Trim());
                u_data.LoginUserType = Convert.ToString(uInfo[3].Trim());
                u_data.lang = Convert.ToString(Session["lang"]);

                Session["UserData"] = u_data;
            }
            catch
            {
                Session.Clear();
                Session.Abandon();
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            _Config.Rd("/index");


        }
    }
}