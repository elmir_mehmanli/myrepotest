﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IDECheckList.Brends.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:LinkButton ID="lnkAddNewBrendModal" CssClass="btn btn-primary" runat="server" OnClick ="lnkAddNewBrendModal_Click">
            <i class ="fa fa-plus"></i> Yeni Brend</asp:LinkButton>
    <div class="mb40"></div>
    <div class="mb40"></div>

    <div class="table-responsive">
        <asp:GridView ID="grdBrends" class="table table-bordered table-hover table-primary table-striped nomargin" runat="server" AutoGenerateColumns="False" DataKeyNames="RECORD_ID" GridLines="None">
            <Columns>
                <asp:BoundField DataField="CODE" HeaderText="KODU" />
                <asp:BoundField DataField="NAME" HeaderText="ADI" />
                <asp:BoundField DataField="NOTE" HeaderText="QEYD" />


                <asp:TemplateField>
                    <ItemTemplate>
                        <ul class="table-options">
                            <li>
                                <asp:LinkButton ID="lnkEditBrends"  OnClick ="lnkEditBrends_Click"
                                    title="Redaktə et" runat="server"
                                    class="fa fa-pencil" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                </asp:LinkButton>
                            </li>

                            <li>
                                <asp:LinkButton ID="lnkDeleteBrends"  OnClick ="lnkDeleteBrends_Click"
                                    title="Sil" runat="server"
                                    class="fa fa-trash" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                </asp:LinkButton>
                            </li>
                        </ul>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </asp:TemplateField>
            </Columns>

        </asp:GridView>
        <asp:HiddenField ID="hdnDeleteBrends" runat="server" />
        <asp:HiddenField ID="hdnEditBrends" runat="server" />
    </div>




    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  AddBrend-->
            <div class="modal bounceIn" id="modalAddBrend" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalAddBrendLabel"><i class="fa fa-plus"></i>&nbsp&nbspYeni Brend</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        Kodu :
                                        <asp:TextBox ID="txtAddBrendCode" autocomplete="off" placeholder="Kodu" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        Adı:
                                       <asp:TextBox ID="txtAddBrendName" autocomplete="off" MaxLength="150" placeholder="Adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                        Qeyd:
                                       <asp:TextBox ID="txtAddBrendNote" TextMode="MultiLine" autocomplete="off" MaxLength="300" placeholder="Qeyd" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <img id="add_Brend_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnBrendAdd" class="btn btn-primary" runat="server" Text="Əlavə et"
                                OnClick ="btnBrendAdd_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_Brend_loading').style.display = '';
                                                           document.getElementById('btnBrendAddCancel').style.display = 'none';" />
                            <button id="btnBrendAddCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnBrendAdd" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  EditBrend-->
            <div class="modal bounceIn" id="modalEditBrend" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalEditBrendLabel"><i class="icon ion-edit"></i>&nbsp&nbspBrendin yenilənməsi</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        Kodu :
                                        <asp:TextBox ID="txtEditBrendCode" autocomplete="off" placeholder="Kodu" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        Adı:
                                       <asp:TextBox ID="txtEditBrendName" autocomplete="off" MaxLength="150" placeholder="Adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                        Qeyd:
                                       <asp:TextBox ID="txtEditBrendNote" TextMode="MultiLine" autocomplete="off" MaxLength="300" placeholder="Qeyd" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <img id="Edit_Brend_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnBrendEdit" class="btn btn-primary" runat="server" Text="Yenilə"
                                OnClick ="btnBrendEdit_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('Edit_Brend_loading').style.display = '';
                                                           document.getElementById('btnBrendEditCancel').style.display = 'none';" />
                            <button id="btnBrendEditCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnBrendEdit" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  Alert-->
            <div class="modal bounceIn" id="modalAlert" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalAlertLabel">
                                <asp:Literal ID="ltrAlertModalHeaderLabel" runat="server"></asp:Literal>
                        </div>
                        <div class="modal-body">
                            <p>
                                <asp:Literal ID="ltrAlertModalBodyLabel" runat="server"></asp:Literal>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <img id="delete_Brend_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnBrendDelete" class="btn btn-primary" runat="server" Text="Bəli"
                                OnClick ="btnBrendDelete_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_Brend_loading').style.display = '';
                                                           document.getElementById('btnBrendDeleteCancel').style.display = 'none';" />
                            <button id="btnBrendDeleteCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnBrendDelete" />
        </Triggers>
    </asp:UpdatePanel>



    <script type="text/javascript">

        function openAlertModal() {
            $('#modalAlert').modal({ show: true });
        }

        function openAddBrendModal() {
            $('#modalAddBrend').modal({ show: true });
        }

        function openEditBrendModal() {
            $('#modalEditBrend').modal({ show: true });
        }

    </script>
</asp:Content>
