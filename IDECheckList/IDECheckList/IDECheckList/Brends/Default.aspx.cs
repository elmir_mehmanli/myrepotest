﻿using IDECheckList.App_Code;
using IDECheckList.DbProc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IDECheckList.App_Code._Config;

namespace IDECheckList.Brends
{
    public partial class Default : System.Web.UI.Page
    {
        UserData u_data = new UserData();
        DbProcess db = new DbProcess();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserData"] == null) _Config.Rd("/exit");
            u_data = (UserData)Session["UserData"];
            if (!IsPostBack)
            {
                FillBrends();
            }
            if (grdBrends.Rows.Count > 0) grdBrends.HeaderRow.TableSection = TableRowSection.TableHeader;
        }


        void FillBrends()
        {
            grdBrends.DataSource = db.GetBrendList();
            grdBrends.DataBind();
            grdBrends.UseAccessibleHeader = true;
            if (grdBrends.Rows.Count > 0) grdBrends.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void lnkAddNewBrendModal_Click(object sender, EventArgs e)
        {
            txtAddBrendCode.Text = "";
            txtAddBrendName.Text = "";
            txtAddBrendNote.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddBrendModal();", true);
        }


        //add brend
        protected void btnBrendAdd_Click(object sender, EventArgs e)
        {
            if (txtAddBrendCode.Text.Trim().Length < 24)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 24 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddBrendModal();", true);
                return;
            }


            if (txtAddBrendName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddBrendModal();", true);
                return;
            }


            string result = db.AddBrends(txtAddBrendCode.Text.Trim(), txtAddBrendName.Text.Trim(), txtAddBrendNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddBrendModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddBrendModal();", true);
                return;
            }

            _Config.Rd("/brends");
        }


        //edit brend modal
        protected void lnkEditBrends_Click(object sender, EventArgs e)
        {
            LinkButton lnkEditBrend = (LinkButton)sender;
            hdnEditBrends.Value = lnkEditBrend.CommandArgument;
            FillBrendDataById(Convert.ToInt16(hdnEditBrends.Value));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditBrendModal();", true);
        }


        private void FillBrendDataById(short editBrendID)
        {
            DataRow drEditMarket = db.GetBrendById(editBrendID);
            if (drEditMarket != null)
            {
                txtEditBrendCode.Text = Convert.ToString(drEditMarket["CODE"]);
                txtEditBrendName.Text = Convert.ToString(drEditMarket["NAME"]);
                txtEditBrendNote.Text = Convert.ToString(drEditMarket["NOTE"]);
            }
        }


        //edit brend
        protected void btnBrendEdit_Click(object sender, EventArgs e)
        {
            short editBrendId = Convert.ToInt16(hdnEditBrends.Value);
            if (txtEditBrendCode.Text.Trim().Length < 24)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 24 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditBrendModal();", true);
                return;
            }


            if (txtEditBrendName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditBrendModal();", true);
                return;
            }


            string result = db.UpdateBrends(editBrendId, txtEditBrendCode.Text.Trim(), txtEditBrendName.Text.Trim(), txtEditBrendNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditBrendModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditBrendModal();", true);
                return;
            }

            _Config.Rd("/brends");
        }


        //delete brend modal
        protected void lnkDeleteBrends_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspBrendin silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "Brend silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnDeleteBrends.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
        }

        protected void btnBrendDelete_Click(object sender, EventArgs e)
        {
            string result = db.DeleteBrends(Int16.Parse(hdnDeleteBrends.Value), (Int16)u_data.LogonUserID);

            if (result != "OK")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
                return;
            }

            _Config.Rd("/brends");
        }
    }
}