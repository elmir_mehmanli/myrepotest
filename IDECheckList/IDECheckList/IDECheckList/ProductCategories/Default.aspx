﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IDECheckList.ProductCategories.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:LinkButton ID="lnkAddNewProductCategoriesModal" CssClass="btn btn-success" runat="server" OnClick ="lnkAddNewProductCategoriesModal_Click">
            <i class ="fa fa-plus"></i> Yeni məhsul kateqoriyası</asp:LinkButton>
    <div class="mb40"></div>
    <div class="mb40"></div>

    <div class="table-responsive">
        <asp:GridView ID="grdProductCategories" class="table table-bordered table-hover table-success table-striped nomargin" runat="server" AutoGenerateColumns="False" DataKeyNames="RECORD_ID" GridLines="None">
            <Columns>
                <asp:BoundField DataField="CODE" HeaderText="KODU" />
                <asp:BoundField DataField="NAME" HeaderText="ADI" />
                <asp:BoundField DataField="NOTE" HeaderText="QEYD" />


                <asp:TemplateField>
                    <ItemTemplate>
                        <ul class="table-options">
                            <li>
                                <asp:LinkButton ID="lnkEditProductCategories"   OnClick ="lnkEditProductCategories_Click"
                                    title="Redaktə et" runat="server"
                                    class="fa fa-pencil" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                </asp:LinkButton>
                            </li>

                            <li>
                                <asp:LinkButton ID="lnkDeleteProductCategories"   OnClick ="lnkDeleteProductCategories_Click"
                                    title="Sil" runat="server"
                                    class="fa fa-trash" CommandArgument='<%#Eval("RECORD_ID") %>'>
                                </asp:LinkButton>
                            </li>
                        </ul>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </asp:TemplateField>
            </Columns>

        </asp:GridView>
        <asp:HiddenField ID="hdnDeleteProductCategories" runat="server" />
        <asp:HiddenField ID="hdnEditProductCategories" runat="server" />
    </div>




    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  AddProductCategories-->
            <div class="modal bounceIn" id="modalAddProductCategories" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalAddProductCategoriesLabel"><i class="fa fa-plus"></i>&nbsp&nbspYeni məhsul kateqoriyası</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        Kodu :
                                        <asp:TextBox ID="txtAddProductCategoriesCode" autocomplete="off" placeholder="Kodu" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        Adı:
                                       <asp:TextBox ID="txtAddProductCategoriesName" autocomplete="off" MaxLength="150" placeholder="Adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                        Qeyd:
                                       <asp:TextBox ID="txtAddProductCategoriesNote" TextMode="MultiLine" autocomplete="off" MaxLength="300" placeholder="Qeyd" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <img id="add_ProductCategories_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnProductCategoriesAdd" class="btn btn-success" runat="server" Text="Əlavə et"
                                OnClick ="btnProductCategoriesAdd_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_ProductCategories_loading').style.display = '';
                                                           document.getElementById('btnProductCategoriesAddCancel').style.display = 'none';" />
                            <button id="btnProductCategoriesAddCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnProductCategoriesAdd" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  EditProductCategories-->
            <div class="modal bounceIn" id="modalEditProductCategories" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalEditMarketLabel"><i class="icon ion-edit"></i>&nbsp&nbspMəhsul kateqoriyasının yenilənməsi</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        Kodu :
                                        <asp:TextBox ID="txtEditProductCategoriesCode" autocomplete="off" placeholder="Kodu" MaxLength="51" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        Adı:
                                       <asp:TextBox ID="txtEditProductCategoriesName" autocomplete="off" MaxLength="150" placeholder="Adı" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                        Qeyd:
                                       <asp:TextBox ID="txtEditProductCategoriesNote" TextMode="MultiLine" autocomplete="off" MaxLength="300" placeholder="Qeyd" runat="server" class="form-control" type="text"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <img id="Edit_ProductCategories_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnProductCategoriesEdit" class="btn btn-success" runat="server" Text="Yenilə"
                                OnClick ="btnProductCategoriesEdit_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('Edit_ProductCategories_loading').style.display = '';
                                                           document.getElementById('btnProductCategoriesEditCancel').style.display = 'none';" />
                            <button id="btnProductCategoriesEditCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnProductCategoriesEdit" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  ProductCategories-->
            <div class="modal bounceIn" id="modalAlert" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalAlertLabel">
                                <asp:Literal ID="ltrAlertModalHeaderLabel" runat="server"></asp:Literal>
                        </div>
                        <div class="modal-body">
                            <p>
                                <asp:Literal ID="ltrAlertModalBodyLabel" runat="server"></asp:Literal>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <img id="delete_ProductCategories_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnProductCategoriesDelete" class="btn btn-success" runat="server" Text="Bəli"
                                OnClick ="btnProductCategoriesDelete_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_ProductCategories_loading').style.display = '';
                                                           document.getElementById('btnProductCategoriesDeleteCancel').style.display = 'none';" />
                            <button id="btnProductCategoriesDeleteCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnProductCategoriesDelete" />
        </Triggers>
    </asp:UpdatePanel>



    <script type="text/javascript">

        function openAlertModal() {
            $('#modalAlert').modal({ show: true });
        }

        function openAddProductCategoriesModal() {
            $('#modalAddProductCategories').modal({ show: true });
        }

        function openEditProductCategoriesModal() {
            $('#modalEditProductCategories').modal({ show: true });
        }

    </script>
</asp:Content>

