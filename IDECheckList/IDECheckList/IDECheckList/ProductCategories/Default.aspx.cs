﻿using IDECheckList.App_Code;
using IDECheckList.DbProc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IDECheckList.App_Code._Config;

namespace IDECheckList.ProductCategories
{
    public partial class Default : System.Web.UI.Page
    {
        UserData u_data = new UserData();
        DbProcess db = new DbProcess();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserData"] == null) _Config.Rd("/exit");
            u_data = (UserData)Session["UserData"];
            if (!IsPostBack)
            {
                FillProductCategories();
            }
            if (grdProductCategories.Rows.Count > 0) grdProductCategories.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        void FillProductCategories()
        {
            grdProductCategories.DataSource = db.GetProductCategoriesList();
            grdProductCategories.DataBind();
            grdProductCategories.UseAccessibleHeader = true;
            if (grdProductCategories.Rows.Count > 0) grdProductCategories.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void lnkAddNewProductCategoriesModal_Click(object sender, EventArgs e)
        {
            txtAddProductCategoriesCode.Text = "";
            txtAddProductCategoriesName.Text = "";
            txtAddProductCategoriesNote.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddProductCategoriesModal();", true);
        }


        //add ProductCategories
        protected void btnProductCategoriesAdd_Click(object sender, EventArgs e)
        {
            if (txtAddProductCategoriesCode.Text.Trim().Length < 24)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 24 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddProductCategoriesModal();", true);
                return;
            }


            if (txtAddProductCategoriesName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddProductCategoriesModal();", true);
                return;
            }


            string result = db.AddProductCategories(txtAddProductCategoriesCode.Text.Trim(), txtAddProductCategoriesName.Text.Trim(), txtAddProductCategoriesNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddProductCategoriesModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAddProductCategoriesModal();", true);
                return;
            }

            _Config.Rd("/ProductCategories");
        }


        //edit ProductCategories modal
        protected void lnkEditProductCategories_Click(object sender, EventArgs e)
        {
            LinkButton lnkEditProductCategories = (LinkButton)sender;
            hdnEditProductCategories.Value = lnkEditProductCategories.CommandArgument;
            FillProductCategoriesDataById(Convert.ToInt16(hdnEditProductCategories.Value));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditProductCategoriesModal();", true);
        }

        private void FillProductCategoriesDataById(short editProductCategoriesID)
        {
            DataRow drEditProductCategories = db.GetProductCategoriesById(editProductCategoriesID);
            if (drEditProductCategories != null)
            {
                txtEditProductCategoriesCode.Text = Convert.ToString(drEditProductCategories["CODE"]);
                txtEditProductCategoriesName.Text = Convert.ToString(drEditProductCategories["NAME"]);
                txtEditProductCategoriesNote.Text = Convert.ToString(drEditProductCategories["NOTE"]);
            }
        }

        //edit ProductCategories
        protected void btnProductCategoriesEdit_Click(object sender, EventArgs e)
        {
            short editProductCategoriesId = Convert.ToInt16(hdnEditProductCategories.Value);
            if (txtEditProductCategoriesCode.Text.Trim().Length < 24)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Kodu 24 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditProductCategoriesModal();", true);
                return;
            }


            if (txtEditProductCategoriesName.Text.Trim().Length == 0)
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Adı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditProductCategoriesModal();", true);
                return;
            }


            string result = db.UpdateProductCategories(editProductCategoriesId, txtEditProductCategoriesCode.Text.Trim(), txtEditProductCategoriesName.Text.Trim(), txtEditProductCategoriesNote.Text.Trim(), (short)u_data.LogonUserID);


            if (result == "EXISTS_CODE")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Daxil etdiyiniz kod artıq sistemdə mövcuddur.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditProductCategoriesModal();", true);
                return;
            }

            if (result == "ERROR")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openEditProductCategoriesModal();", true);
                return;
            }

            _Config.Rd("/ProductCategories");
        }

        //delete ProductCategories modal
        protected void lnkDeleteProductCategories_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspMəhsul kateqoriyasının silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "Məhsul kateqoriyası silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnDeleteProductCategories.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
        }


        //delete ProductCategories
        protected void btnProductCategoriesDelete_Click(object sender, EventArgs e)
        {
            string result = db.DeleteProductCategories(Int16.Parse(hdnDeleteProductCategories.Value), (Int16)u_data.LogonUserID);

            if (result != "OK")
            {
                _config.AlertMessageNew(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop" + System.Guid.NewGuid().ToString(), "openAlertModal();", true);
                return;
            }

            _Config.Rd("/ProductCategories");
        }
    }
}