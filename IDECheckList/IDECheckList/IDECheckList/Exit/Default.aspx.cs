﻿using IDECheckList.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IDECheckList.Exit
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["az"] = null;
            Session["UserData"] = null;
            Session.Clear();
            Session.Abandon();
            _Config.Rd("/");

        }
    }
}